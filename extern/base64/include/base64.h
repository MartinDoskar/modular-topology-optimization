/*
   base64 encoding and decoding with C++.

   Version: 1.02 (modified by Martin Doskar MartinDoskar@gmail.com)
			- Changed back from header-only to header and source
			- Added namespace base64
			- pragma once
			- Added encode_vector_into_base64_string function template

   Copyright (C) 2004-2017 René Nyffenegger

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
	  claim that you wrote the original source code. If you use this source code
	  in a product, an acknowledgment in the product documentation would be
	  appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
	  misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   René Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

#pragma once

#include <iostream>
#include <optional>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <vector>

namespace base64 {


	std::string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);

	
	std::string base64_decode(std::string const& encoded_string);


	template <typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value>>
	std::string encode_vector_into_base64_string(const std::vector<T>& dataVector)
	{
		return base64_encode(reinterpret_cast<unsigned char const*>(dataVector.data()), dataVector.size()*(sizeof(T) / sizeof(unsigned char)));
	}


	template <typename T, typename = std::enable_if_t<std::is_arithmetic<T>::value>>
	std::string encode_vector_into_base64_string_with_length(const std::vector<T>& dataVector)
	{
		// Converts data of the vector into base64 string representation
		// and prepend a header containing the int32 length of the vector (specific for binary XML VTK format)
		auto nBytes = static_cast<int32_t>(sizeof(T) * dataVector.size());
		auto encodedString = base64_encode(reinterpret_cast<unsigned char const*>(&nBytes), sizeof(int32_t) / sizeof(unsigned char));
		encodedString.append(encode_vector_into_base64_string(dataVector));

		return encodedString;
	}

	template <typename T>
	std::vector<T> decode_base64_string_into_vector(T, const std::string& inString, const std::optional<int> inNdata = std::nullopt) 
	{
		// Check/deduce length of data vector from string representation
		auto nChar = static_cast<int>(inString.size());
		if (nChar % 4 != 0)
			throw std::length_error{ "base64::decode_base64_string_into_vector: base64 string is not multiple of 4." };
		auto nBytes = 3 * (nChar / 4);
		if ((inString.size() > 1) && (inString.at(inString.length() - 1) == '='))
			nBytes -= 1;
		if ((inString.size() > 2) && (inString.at(inString.length() - 2) == '='))
			nBytes -= 1;
		auto nData = nBytes / sizeof(T);

		// Check validity of provided number
		if (inNdata.has_value()) {
			if (inNdata.value() != nData)
				throw std::invalid_argument{ "base64::decode_base64_string_into_vector: Provided length of data does not correspond to the identified lenght." };
		}

		// Decode data
		auto dataStr = base64_decode(inString);
		auto dataRaw = reinterpret_cast<T*>(const_cast<char*>(dataStr.data()));
		return std::vector<T>{ dataRaw, dataRaw + nData };
	}

}
