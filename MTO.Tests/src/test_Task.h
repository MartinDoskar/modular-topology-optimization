#include "Task.h"

#include <gtest/gtest.h>

TEST(Task, JSONctorHelmholtz)
{
    auto JSONinput = nlohmann::json{
        {"discretisationType", 0},
        {"domainResolution", {5, 3}},
        {"domainSize", {4, 3}},
        {"relativeVolumeConstraint", 0.3},
        {"filterType", 1},
        {"filterRadius", 0.1},
        {"problems", {
            {
                {"prescribedValues", {{0, 0.0}, {1, 0.0}, {3, 0.0}}}, 
                {"prescribedLoad", {{5, 1.0}}}
            }, 
            {
                {"prescribedValues", { 
                    {{"lowerBound", {0.0,0.0}}, {"upperBound", {0.001,1.0}}, {"component", 0}, {"value", 0.0}}
                }}, 
                {"objectiveVector", {{5, 1.0}}}
            }
        }}
    };

    mto::Task{JSONinput};

    EXPECT_TRUE(true);
}

TEST(Task, JSONctorKernel)
{
    auto JSONinput = nlohmann::json{
        {"discretisationType", 0},
        {"domainResolution", {10, 10}},
        {"domainSize", {4.0, 3.0}},
        {"relativeVolumeConstraint", 0.3},
        {"filterType", 0},
        {"filterRadius", 1.0}};

    mto::Task{JSONinput};

    EXPECT_TRUE(true);
}

