#include "Discretisation.h"

#include <gtest/gtest.h>

TEST(Discretization, RegularGrid)
{
    auto testJSON = nlohmann::json{
        {"discretisationType", 0},
        {"domainResolution", {5, 3}},
        {"domainSize", {4.0, 3.0}},
        {"relativeVolumeConstraint", 0.3}};

    auto d = mto::Discretisation{testJSON};

    EXPECT_TRUE(std::abs(d.get_whole_volume() - 4.0 * 3.0) < 1e-10);
    EXPECT_TRUE(std::abs(d.get_current_volume() - 0.3 * 4.0 * 3.0) < 1e-10);
    EXPECT_TRUE(std::abs(d.get_relative_current_volume() - 0.3) < 1e-10);

    d.update_element_densities(0.5);
    EXPECT_TRUE(std::abs(d.get_current_volume() - 0.5 * 4.0 * 3.0) < 1e-10);
    EXPECT_TRUE(std::abs(d.get_relative_current_volume() - 0.5) < 1e-10);
}

TEST(Discretization, BoundaryBox)
{
    auto testJSON = nlohmann::json{
        {"discretisationType", 0},
        {"domainResolution", {5, 3}},
        {"domainSize", {1.0, 1.0}},
        {"relativeVolumeConstraint", 0.3}};

    const auto d = mto::Discretisation{testJSON};
    const auto b = mto::BoundaryBox{Eigen::Vector2d{0.0,0.0}, Eigen::Vector2d{0.05,0.35}};

    const auto nids = d.get_node_ids_inside_box(b);

    EXPECT_TRUE(nids.size() == 2);
}

TEST(Discretization, Mesh)
{
    auto testJSON = nlohmann::json{
        {"discretisationType", 1},
        {"nodes", {{0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {1.0, 1.0, 0.0}}},
        {"elements", {{{"type", 8}, {"matID", 0}, {"nodes", {0, 1, 2, 3}}}, {{"type", 8}, {"matID", 0}, {"nodes", {0, 1, 2, 3}}}}},
        {"domainSize", {4.0, 3.0}},
        {"relativeVolumeConstraint", 0.3}};

    auto d = mto::Discretisation{testJSON};

    EXPECT_TRUE(d.get_number_of_nodes() == 4);
    EXPECT_TRUE(d.get_number_of_elements() == 2);
}

TEST(Discretization, AdditionalDiscretisation)
{
    auto testJSON = nlohmann::json{
        {"discretisationType", 0},
        {"domainResolution", {5, 3}},
        {"domainSize", {1.0, 1.0}},
        {"relativeVolumeConstraint", 0.3},
        {"additionalDiscretisation", {
            {"domainOrigin", {0.4, 1.0}},
            {"discretisationType", 0},
            {"domainResolution", {5, 2}},
            {"domainSize", {1.0, 0.4}},
            {"relativeVolumeConstraint", 0.3}
        }}
        };

    const auto d = mto::Discretisation{testJSON};

    std::cout << d.get_number_of_nodes() << std::endl;

    EXPECT_TRUE(d.get_number_of_nodes() == 38);
    EXPECT_TRUE(d.get_number_of_elements() == 25);
}