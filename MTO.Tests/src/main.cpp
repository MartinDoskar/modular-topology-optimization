#include "test_Discretisation.h"
#include "test_Optimizer.h"
#include "test_Task.h"

#include <gtest/gtest.h>

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}