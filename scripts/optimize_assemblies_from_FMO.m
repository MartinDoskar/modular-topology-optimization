% OPTIMIZE_ASSEMBLIES_FROM_FMO runs topology optimisation for provided
% assembly plans
%
% Features: * The last two runs correspond to non-modular (DNS) and periodic setup
%
% Version:  0.3.2 (2020-02-09)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

% assemblyFile = 'C:/Users/doskamar/Downloads/assemblies_v2.mat';
assemblyFile = 'P:/WP2/MTO/FMO_v2/martin_random2.mat';

folderTemplate = 'C:/Users/doskamar/Documents/MTO/temp/%i';
fileTemplate = 'input.json';
programFolder = 'C:/Users/doskamar/Documents/MTO/bin/Release';     % Folder where MTO.Application executable is stored
programName = 'MTO.Application.exe';

outputFolder = 'C:/Users/doskamar/Documents/MTO/temp';

setupTemplate = struct( ...
    'originalIndex', 0, ...
    'assemblyPlan', [], ...
    'assemblyResolution', [], ...
    'nModules', NaN, ...
    'moduleSize', [0.5, 0.5], ...
    'moduleResolution', [50, 50], ...
    'prescribedVolFrac', 0.3, ...
    'maxIter', 200, ...
    'diffNormLimit', 1e-3, ...
    'filterRadius', 1e-4, ...
    'outputFolder', '' ...
    );


%% Temporary fix for restoring interrupted calculations
foo = dir('../temp');

doneMask = false(length(foo)-3, 1);
valueVector = nan(length(foo)-3, 1);
for i = 3:length(foo)-1    
   if foo(i).isdir
      bar = dir(fullfile(foo(i).folder, foo(i).name)); 
      
      trueIndex = sscanf(foo(i).name, '%i');
      
      doneFlag = false;
      for j = 1:length(bar)
        if strcmpi(bar(j).name, 'final.vtk')
            doneFlag = true;
            break;
        end
      end
      
      doneMask(trueIndex) = doneFlag;  
      
      if doneFlag
        resFilePath = fullfile(foo(i).folder, foo(i).name, 'log.txt');
        iF = fopen(resFilePath, 'rt');
        while ~feof(iF)
           line = fgetl(iF); 
           value = sscanf(line, 'Final compliance = %f');
           if ~isempty(value)
               valueVector(trueIndex) = value;
           end
        end
        fclose(iF);
      end
      
   end
end


trueValues = valueVector(1:end-2);

skipValues = isnan(trueValues);
trueValues(skipValues) = [];

hist(trueValues)

ylims = get(gca(), 'YLim');
hold on;
plot( valueVector(end)*[1,1], ylims, '--', 'LineWidth', 2 );
plot( valueVector(end-1)*[1,1], ylims, '--', 'LineWidth', 2 );
hold off;


% foo = [rawData.randomTiling.modularCompliance];
foo = [rawData.randomTiling.clusterCost];
% foo = [rawData.randomTiling.clusterCostCity];
% foo = [rawData.randomTiling.clusterCostCorr];
% foo = [rawData.randomTiling.clusterCostCosine];
foo(skipValues) = [];

iF = figure();
scatter( foo, trueValues)
grid on;
box off;
xlabel('FMMO', 'Interpreter', 'latex');
ylabel('MTO', 'Interpreter', 'latex');

fprintf('Correlation coefficient:\n');
foo = [rawData.randomTiling.modularCompliance];
fprintf('\t FMMTO: %e\n', corr( foo(~skipValues)', trueValues ));
foo = [rawData.randomTiling.clusterCost];
fprintf('\t cluster: %e\n', corr( foo(~skipValues)', trueValues ));
foo = [rawData.randomTiling.clusterCostCity];
fprintf('\t cluster_city: %e\n', corr( foo(~skipValues)', trueValues ));
foo = [rawData.randomTiling.clusterCostCorr];
fprintf('\t cluster_corr: %e\n', corr( foo(~skipValues)', trueValues ));
foo = [rawData.randomTiling.clusterCostCosine];
fprintf('\t cluster_cosine: %e\n', corr( foo(~skipValues)', trueValues ));

    
%% Load and pre-process data
globalTimer = tic;
fprintf( '%s: Script has started\n', datestr(now()));

rawData = load(assemblyFile);
nUniqueData = length(rawData.randomTiling);

uniqueData = cell(1,nUniqueData);
for i = 1:nUniqueData
    uniqueData{i} = rawData.randomTiling(i).modularAssembly;    
end

% data = rawData.ass4;  
% nData = length(data);
% 
% byRow = zeros(nData, numel(data{1}));
% for i = 1:nData
%    byRow(i,:) = data{i}(:); 
% end
% 
% [foo, iA, iC] = unique(byRow, 'stable', 'rows');
% nUniqueData = size(foo, 1);
% formerIndex = [iA; -1; -1];     
% 
% uniqueData = cell(1,nUniqueData);
% for i = 1:nUniqueData
%    uniqueData{i} = reshape(foo(i,:), size(data{1})); 
% end
% 
% if nData ~= nUniqueData
%    warning('Duplicit assembly plans were found (%i), considering only the unique ones (%i) (+ uniform and periodic)\n', ...
%        nData - nUniqueData, nUniqueData);
% end

uniqueData{end+1} = uniqueData{1};
uniqueData{end}(:) = 1:numel(uniqueData{end});

uniqueData{end+1} = uniqueData{1};
uniqueData{end}(:) = 1;

nUniqueData = length(uniqueData);

fprintf('%s: Data have been loaded\n', datestr(now()));

%%

global t;
t = Tracker(nUniqueData);

dq = parallel.pool.DataQueue;
afterEach(dq, @(varargin) update_tracker());

optimizedCompliance = nan(nUniqueData, 1);
parfor i = 1:nUniqueData
      
    localTimer = tic;
    if ~doneMask(i)      

        tempPlan = uniqueData{i};    
        [moduleNumbers, map, backmap] = unique(tempPlan(:), 'sorted');    
        consecutivePlan = fliplr(reshape(backmap, size(tempPlan))');

        folderPath = sprintf(folderTemplate, i);    
        if ~exist(folderPath, 'dir')
            mkdir(folderPath);
        end
        fileName = sprintf(fileTemplate, i);

        setup = setupTemplate;
    %     setup.originalIndex = formerIndex(i); 
        setup.assemblyPlan = consecutivePlan(:);
        setup.assemblyResolution = size(consecutivePlan);
        setup.nModules = length(moduleNumbers);
        setup.outputFolder = folderPath; 

        iF = fopen(fullfile(folderPath, fileName), 'wt');
        fprintf(iF, '%s\n', jsonencode(setup));
        fclose(iF);

        systemCmd = sprintf('%s %s', fullfile(programFolder,'MTO.Application.exe'), fullfile(folderPath,fileName));
        [status,cmdout] = system(systemCmd);

        if status == 0
            optimizedCompliance(i) = ...
                sscanf( ...
                    cmdout(strfind(cmdout, 'Final compliance = '):end), ...
                    'Final compliance = %f');

            iFlog = fopen(fullfile(folderPath, 'log.txt'), 'wt');
            fprintf(iFlog, '%s', cmdout);
            fclose(iFlog); 

        else
            warning(cmdout);
        end
    end
    
    send(dq, i);    
    fprintf('\tOptimization of assembly no. %i has finished in %f s\n', i, toc(localTimer));
    
end

%% Save output

fprintf('%s: All has been computed in %f s\n', datestr(now()), toc(globalTimer));

save( fullfile(outputFolder, 'results.mat'), ...
    'assemblyFile', 'setupTemplate', 'uniqueData', 'optimizedCompliance', ...
    '-v7.3');

%%
load('P:\WP2\MTO\FMO\ass5_v1\results.mat');

figure()
hist(optimizedCompliance(1:end-2), 1000)

fprintf('Results of optimisation:\n')
fprintf('FMO assemblies:\n');
fprintf('\tbest = %e\n', min(optimizedCompliance(1:end-2)));
fprintf('\tmean = %e\n', mean(optimizedCompliance(1:end-2)));
fprintf('\tstd  = %e\n', std(optimizedCompliance(1:end-2)));
fprintf('PUC version:\n');
fprintf('\tbest = %e\n', optimizedCompliance(end));
fprintf('Full-scale version:\n');
fprintf('\tbest = %e\n', optimizedCompliance(end-1));


%% Auxiliary function
function update_tracker()
    global t;
    t = add(t);
    t.print();
end
    