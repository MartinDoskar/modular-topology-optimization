%
% A script developed for parsing input *.json file and produced *.vtk files
% in order to produce *.mat inputs for DD solvers
%
% Features: * Test produce stiffness matrix per each module
%           * Store modules as binary bmp for subsequent STL creation
%
% Version:  0.3.0 (2020-11-11)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

% inResultsPath = 'P:\WP2\MTO\_compare_paper\modular_100_022_001\28.vtk';   
% inDefinitionPath = 'C:\Users\Huricane\Documents\MTO\MTO.Tests\data\inputs_paper.json';
inResultsPath = 'P:\WP2\MTO\_compare_paper\modular_100_022_0006\100.vtk';
inDefinitionPath = 'C:\Users\Martin\Documents\MTO\MTO.Tests\data\inputs_paper.json';

storeBMP = true;
outputFolder = '';
binaryImageThreshold = 0.8;

%%

inResultsPath = 'C:\Users\Huricane\Documents\MTO\bin\Release\2.vtk';
inDefinitionPath = 'C:\Users\Huricane\Documents\MTO\MTO.Tests\data\inputs_paper.json';

outputFilePath = './problem_10x10_step500.mat';

info = sprintf('Reduced resolution (10x10 per module) of a simple supproted beam with 16x6 modules\nInput file:  %s\nModule file: %s',...
    inDefinitionPath, inResultsPath);


%% Parse inputs
definition = jsondecode(fileread(inDefinitionPath));
if any(definition.assemblyPlan(:) == 0)
    definition.assemblyPlan = definition.assemblyPlan + 1;
end
definition.assemblyPlan = reshape(definition.assemblyPlan, definition.assemblyResolution');

c = textread(inResultsPath, '%s', 'delimiter', '\n');
anchor1 = find(~cellfun(@isempty, strfind(c, 'SCALARS densityDesign')));
anchor2 = find(~cellfun(@isempty, strfind(c, 'SCALARS densityChange')));

readData = reshape(cellfun(@str2num, {c{anchor1+2:anchor2-1}}), ...
    (definition.assemblyResolution .* definition.moduleResolution)');

% Plot design density data
% figure(100);
% imshow(rot90(readData));


%% Extract individual modules
clear modules
modules(1:definition.nModules) = struct('design', [], 'mesh', []);

blocksPerRow = definition.assemblyResolution(1) * definition.moduleResolution(1);

for iTy = 1:definition.assemblyResolution(2)
    for iTx = 1:definition.assemblyResolution(1)
        
        iModule = definition.assemblyPlan(iTx, iTy);
        
        if isempty(modules(iModule).design)
            localDesign = nan(definition.moduleResolution(1), definition.moduleResolution(2));
            
            for iMy = 1:definition.moduleResolution(2)
                localDesign(:,iMy) = readData(...
                    ((iTy-1)*definition.moduleResolution(2) + (iMy-1)) * blocksPerRow + ...
                    (iTx-1)*definition.moduleResolution(1) + (1:definition.moduleResolution(1)) );
            end
            
            if storeBMP
                iFig = figure(99);
                cla();
                imshow(rot90( localDesign < binaryImageThreshold ));
                
                print(iFig, fullfile(outputFolder, sprintf('module_%i.bmp', iModule)), '-dbmp'); 
            end
            
            modules(iModule).design = localDesign;
            
        end
        
    end
end

% Construct mesh and stiffness for each module
for iM = 1:definition.nModules
    if ~isempty(modules(iModule).design)
        
%         % Visualize individual modules
%         figure(iM);
%         cla();
%         imshow(rot90(modules(iM).design));        

        % Construct mesh and stiffness mtrx
        modules(iM).mesh = create_mesh_from_img(modules(iM).design, definition.moduleSize ./ definition.moduleResolution);
        modules(iM).K = construct_stiffness(modules(iM).mesh, modules(iM).design);
        modules(iM).Fint = zeros(2 * modules(iM).mesh.nNodes, 1);
        
        %% Investigate stiffness matrix
        fprintf('Module no. %i: min = %e, max = %e\n', iM, min(modules(iM).design(:)), max(modules(iM).design(:)));
        
        K = modules(iM).K;
        
        spy(K);
        [eigVecs, eigVals] = eig(full(K));
        
        % Test pseudoinverse
        Q = zeros(modules(iM).mesh.nNodes * 2, 3);
        Q(1:2:end, 1) = 1.0;
        Q(2:2:end, 2) = 1.0;
        Q(1:2:end, 3) = -modules(iM).mesh.nodes(:,2);
        Q(2:2:end, 3) = modules(iM).mesh.nodes(:,1);
        
        Q = Q / sqrtm(Q'*Q);
        
        Iind = [ 2 * modules(iM).mesh.boundary.vertex{1} - 1, ...
                 2 * modules(iM).mesh.boundary.vertex{1}, ...
                 2 *modules(iM).mesh.boundary.vertex{2} ];
        
        rho = 1e-06 * eigVals(4,4);
        
        KrhoI = K;
        KrhoI(Iind,Iind) = KrhoI(Iind, Iind) + rho * (Q(Iind,:) * (Q(Iind,:)' * Q(Iind,:)) * Q(Iind,:)');
        KrhoI = 0.5 * (KrhoI + KrhoI');
             
        KrhoF = K + rho * (Q * ((Q'*Q) \ Q'));
        KrhoF = 0.5 * (KrhoF + KrhoF');
        
%         spy(KrhoFull);
%         spy(KrhoI);
        
        Kpinv = pinv(full(K));
        KinvI = inv(full(KrhoI));
        KinvF = inv(full(KrhoF));
        
        testF = zeros(size(K,1), 1);
        testF( 2*modules(iM).mesh.boundary.edge{2} -1 ) = 1;
        testF( 2*modules(iM).mesh.boundary.edge{1} -1 ) = -1;
        
        uPinv = Kpinv * testF;
        uInvI = KrhoI \ testF;
        uInvF = KrhoF \ testF;
        
        uPinvCleaned = uPinv - Q * (Q' * uPinv);
        uInvICleaned = uInvI - Q * (Q' * uInvI);
        uInvFCleaned = uInvF - Q * (Q' * uInvF);
        
        norm(uPinvCleaned - uInvICleaned) / max(norm(uPinvCleaned), norm(uInvICleaned))
        norm(uPinvCleaned - uInvFCleaned) / max(norm(uPinvCleaned), norm(uInvFCleaned))
        
        
    end
end


%% Construct domain data
clear domains
domains(1:prod(definition.assemblyResolution)) = struct('moduleId', [], 'boundary', []);

% Define elements
nTx = definition.assemblyResolution(1);
nTy = definition.assemblyResolution(2);

nX = definition.moduleResolution(1);
nY = definition.moduleResolution(2);

nodesPerRow = (nTx*nX+1);
nodesPerCol = (nY-1)*nTy;
allNodesInRows = nodesPerRow*(nTy+1);

for iTy = 1:nTy
    for iTx = 1:nTx
        iDomain = (iTy-1)*nTx + iTx;
        
        nodesAB = (iTy-1)*nodesPerRow + (iTx-1)*nX + 1 : (iTy-1)*nodesPerRow + iTx*nX + 1;
        nodesBD = allNodesInRows + iTx*nodesPerCol + (iTy-1)*(nY-1) + 1 : allNodesInRows + iTx*nodesPerCol + iTy*(nY-1);
        nodesDC = iTy*nodesPerRow + iTx*nX + 1 : -1 : iTy*nodesPerRow + (iTx-1)*nX + 1;
        nodesCA = allNodesInRows + (iTx-1)*nodesPerCol + iTy*(nY-1) : -1 : allNodesInRows + (iTx-1)*nodesPerCol + (iTy-1)*(nY-1) + 1;
        
        nodeA = nodesAB(1);
        nodeB = nodesAB(end);
        nodeC = nodesDC(end);
        nodeD = nodesDC(1);
        
        boundary.vertex = {nodeA, nodeB, nodeC, nodeD};
        boundary.edge = {fliplr(nodesCA)', nodesBD', nodesAB(2:end-1)', fliplr(nodesDC(2:end-1))'};
        
        boundary.whole = [ boundary.vertex{1}; boundary.vertex{2}; boundary.vertex{3}; boundary.vertex{4}; ...
                           boundary.edge{1}; boundary.edge{2}; boundary.edge{3}; boundary.edge{4} ];
                       
        domains(iDomain).moduleId = definition.assemblyPlan(iTx, iTy);
        domains(iDomain).boundary = boundary;
    end
end

left = 1;
right = nodesPerRow;
top = nTy*nodesPerRow + ceil(nodesPerRow / 2);

loading = struct('uPres', {[2*left-1, 0.0],[2*left, 0.0],[2*right, 0.0]}, 'Fext', {[2*top, -10]});

problem = struct( ...
    'loading', loading, ...
    'domains', domains, ...
    'modules', modules, ...
    'info', info);

save(outputFilePath, 'problem');


%% Auxiliary functions
function [mesh] = create_mesh_from_img(img, voxelSize)

    [nY, nX] = size(img);

    mesh.nodes = [ kron( ones(1,nY+1), (0:1:nX) )', ...
                   kron( (0:1:nY), ones(1,nX+1) )', ...
                   zeros( (nX+1)*(nY+1), 1) ];
    mesh.elements = zeros( nX*nY, 4 );
    mesh.elemMat = (1:nX*nY)';
    
    mesh.nodes(:,1) = mesh.nodes(:,1) * voxelSize(1);
    mesh.nodes(:,2) = mesh.nodes(:,2) * voxelSize(2);   
    
    mesh.nodes(:,1) = mesh.nodes(:,1) - mean(mesh.nodes(:,1));
    mesh.nodes(:,2) = mesh.nodes(:,2) - mean(mesh.nodes(:,2));
    mesh.nodes(:,3) = mesh.nodes(:,3) - mean(mesh.nodes(:,3));

    for iY = 1:nY
        for iX = 1:nX
            voxelVertices = [ (iY-1)*(nX+1) + iX, (iY-1)*(nX+1) + iX + 1, iY*(nX+1) + iX + 1, iY*(nX+1) + iX  ];
            mesh.elements(((iY-1)*nX + iX), : ) = voxelVertices;
        end
    end

    mesh.nNodes = size(mesh.nodes, 1);
    mesh.nElems = size(mesh.elements, 1);

    mesh.boundary.vertex = {[1], [nX+1], [(nX)*(nY+1)+1], [(nX+1)*(nY+1)]};
    mesh.boundary.edge = { ((nX+2):(nX+1):((nX-1)*(nY+1)+1))', ...
        ((2*nX+2):(nX+1):((nX)*(nY+1)))', ...
        (2:nX)', ...
        (((nX)*(nY+1)+2):((nX+1)*(nY+1)-1))' };
    mesh.boundary.whole = [ mesh.boundary.vertex{1}; mesh.boundary.vertex{2}; mesh.boundary.vertex{3}; mesh.boundary.vertex{4}; ...
                            mesh.boundary.edge{1}; mesh.boundary.edge{2}; mesh.boundary.edge{3}; mesh.boundary.edge{4} ];

end

function [K] = construct_stiffness(mesh, design)

    parameters = struct('Emin', 1.0e-9, 'E0', 1.0, 'nu', 0.3, 'penalisation', 3);
    nDOFsElem = 8;
    
    oneOverSqrtThree = 0.577350269189626;
    quadrature(1) = struct('coords', [-oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
    quadrature(2) = struct('coords', [+oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
    quadrature(3) = struct('coords', [-oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );
    quadrature(4) = struct('coords', [+oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );
    
    D0 = get_default_material_stiffness(parameters.E0, parameters.nu);
    X = mesh.nodes(mesh.elements(1,:), 1:2);
    K0 = zeros(nDOFsElem);
    for iG = 1:length(quadrature)        
        Bparam = get_B_mtrx(quadrature(iG).coords(1), quadrature(iG).coords(2));
        J = Bparam * X;
        Bcoord = J \ Bparam;
        
        B = [ Bcoord(1,1), 0.0, Bcoord(1,2), 0.0, Bcoord(1,3), 0.0, Bcoord(1,4), 0.0; ...
             0.0, Bcoord(2,1), 0.0, Bcoord(2,2), 0.0, Bcoord(2,3), 0.0, Bcoord(2,4); ...
             Bcoord(2,1), Bcoord(1,1), Bcoord(2,2), Bcoord(1,2), Bcoord(2,3), Bcoord(1,3), Bcoord(2,4), Bcoord(1,4) ];
        
        K0 = K0 + B'*(D0*B) * quadrature(iG).weight * det(J);
    end
    
    r = zeros(mesh.nElems * nDOFsElem^2, 1);
    c = zeros(mesh.nElems * nDOFsElem^2, 1);
    v = zeros(mesh.nElems * nDOFsElem^2, 1);
    for iE = 1:mesh.nElems
        iNodes = mesh.elements(iE,:);
        iDOFs = [2*iNodes - 1; 2*iNodes];
        iDOFs = iDOFs(:);
        
        coeff = (parameters.Emin + design(iE)^(parameters.penalisation) * (parameters.E0 - parameters.Emin));
        Ke = coeff * K0;
        
        r((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(iDOFs, ones(nDOFsElem,1));
        c((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(ones(nDOFsElem,1), iDOFs);
        v((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = Ke(:); 
    end
    K = sparse(r, c, v, 2*mesh.nNodes, 2*mesh.nNodes);
    K = 0.5 * (K + K');

end

function [D] = get_default_material_stiffness(E, nu)
    numerator = (1.0 - nu * nu);
    G = E / (2.0 * (1.0 + nu));

    D = [ E / numerator, nu * E / numerator, 0.0; ...
          nu * E / numerator, E / numerator, 0.0; ...
          0.0, 0.0, G ];
end

function [B] = get_B_mtrx(eta, ksi)
    B  = [ -0.25 * (1.0 - eta), +0.25 * (1.0 - eta), +0.25 * (1.0 + eta), -0.25 * (1.0 + eta); ...
           -0.25 * (1.0 - ksi), -0.25 * (1.0 + ksi), +0.25 * (1.0 + ksi), +0.25 * (1.0 - ksi) ];
end
   
