% A script developed for parsing input *.json file and produced *.vtk files
% in order to produce *.mat inputs for DD solvers
%
% Features: * Test produce stiffness matrix per each module
%           * Added geometrical shift for subsequent plotting
%           * Enable saving filtering problem data
%
% Version:  0.4.0 (2021-02-16)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');
        
resolutions = [10, 20, 30, 40, 50];
steps = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];%[1, 3, 5, 10, 30, 50, 100];

stiffnessParameters = struct('Emin', 1.0e-6, 'E0', 1.0, 'nu', 0.3, 'penalisation', 3);
filteringParameters = struct('r', 0.05);
returnFiltering = false;    


for iR = 1:length(resolutions)
    for iS = 1:length(steps)
        
        resolution = resolutions(iR);
        step = steps(iS);
        
        % inResultsPath = 'P:\WP2\MTO\_compare_paper\modular_100_022_001\100.vtk';
        % inDefinitionPath = 'C:\Users\Huricane\Documents\MTO\MTO.Tests\data\inputs_paper.json';
        inResultsPath = sprintf('C:/Users/Huricane/Documents/MTO/bin/RelWithDebInfo/%i_2_e6/%i.vtk', resolution, step);
        inDefinitionPath = sprintf('C:/Users/Huricane/Documents/MTO/bin/RelWithDebInfo/%i_2_e6/inputs_paper.json', resolution);
                
        if returnFiltering
            problemStr = 'Filtering';
        else
            problemStr = 'Mechanics';
        end
        
        outputFilePath = sprintf('./problem_%ix%i_step%02i_%s.mat', resolution, resolution, step, problemStr);
        
        info = sprintf('Reduced resolution (100x100 per module) of a simple supproted beam with 16x6 modules\nInput file:  %s\nModule file: %s\nSaved problem: %s',...
            inDefinitionPath, inResultsPath, problemStr);
        
        
        
        % Parse inputs
        definition = parse_topopt_input_JSON(inDefinitionPath);
        modules = load_modular_geometry_from_VTK(inResultsPath, definition);        
        
        % Construct mesh and stiffness for each module
        for iM = 1:definition.nModules
            if ~isempty(modules(iM).design)
                
                %         % Visualize individual modules
                %         figure(iM);
                %         cla();
                %         imshow(rot90(modules(iM).design));
                
                
                % Construct mesh and stiffness mtrx
                modules(iM).mesh = create_mesh_from_img(modules(iM).design, definition.moduleSize ./ definition.moduleResolution);
                if returnFiltering
                    [Kf, Ff, Pn2e] = construct_filtering_mtrx(modules(iM).mesh, modules(iM).design, filteringParameters);
                    modules(iM).K = Kf;
                    modules(iM).Fint = Ff;
                else
                    modules(iM).K = construct_stiffness(modules(iM).mesh, modules(iM).design, stiffnessParameters);
                    modules(iM).Fint = zeros(2 * modules(iM).mesh.nNodes, 1);
                end
                
                
                % Test results of averaging
                %         patch( struct('Vertices', modules(iM).mesh.nodes(:,1:2), ...
                %                       'Faces', modules(iM).mesh.elements, ...
                %                       'FaceVertexCData', modules(iM).design(:)), ...
                %                'EdgeColor', 'none', 'FaceColor', 'flat' );
                %         patch( struct('Vertices', modules(iM).mesh.nodes(:,1:2), ...
                %                       'Faces', modules(iM).mesh.elements, ...
                %                       'FaceVertexCData', Kf \ Ff), ...
                %                'EdgeColor', 'none', 'FaceColor', 'interp' );
                %         patch( struct('Vertices', modules(iM).mesh.nodes(:,1:2), ...
                %                       'Faces', modules(iM).mesh.elements, ...
                %                       'FaceVertexCData', Pn2e * (Kf \ Ff)), ...
                %                'EdgeColor', 'none', 'FaceColor', 'flat' );
                %         axis equal;
                
                
                % Investigate stiffness matrix
                %         fprintf('Module no. %i: min = %e, max = %e\n', iM, min(modules(iM).design(:)), max(modules(iM).design(:)));
                %
                %         K = modules(iM).K;
                %
                %         spy(K);
                %         [eigVecs, eigVals] = eig(full(K));
                %
                %         % Test pseudoinverse
                %         Q = zeros(modules(iM).mesh.nNodes * 2, 3);
                %         Q(1:2:end, 1) = 1.0;
                %         Q(2:2:end, 2) = 1.0;
                %         Q(1:2:end, 3) = -modules(iM).mesh.nodes(:,2);
                %         Q(2:2:end, 3) = modules(iM).mesh.nodes(:,1);
                %
                %         Q = Q / sqrtm(Q'*Q);
                %
                %         Iind = [ 2 * modules(iM).mesh.boundary.vertex{1} - 1, ...
                %                  2 * modules(iM).mesh.boundary.vertex{1}, ...
                %                  2 *modules(iM).mesh.boundary.vertex{2} ];
                %
                %         rho = 1e-06 * eigVals(4,4);
                %
                %         KrhoI = K;
                %         KrhoI(Iind,Iind) = KrhoI(Iind, Iind) + rho * (Q(Iind,:) * (Q(Iind,:)' * Q(Iind,:)) * Q(Iind,:)');
                %         KrhoI = 0.5 * (KrhoI + KrhoI');
                %
                %         KrhoF = K + rho * (Q * ((Q'*Q) \ Q'));
                %         KrhoF = 0.5 * (KrhoF + KrhoF');
                %
                % %         spy(KrhoFull);
                % %         spy(KrhoI);
                %
                %         Kpinv = pinv(full(K));
                %         KinvI = inv(full(KrhoI));
                %         KinvF = inv(full(KrhoF));
                %
                %         testF = zeros(size(K,1), 1);
                %         testF( 2*modules(iM).mesh.boundary.edge{2} -1 ) = 1;
                %         testF( 2*modules(iM).mesh.boundary.edge{1} -1 ) = -1;
                %
                %         uPinv = Kpinv * testF;
                %         uInvI = KrhoI \ testF;
                %         uInvF = KrhoF \ testF;
                %
                %         uPinvCleaned = uPinv - Q * (Q' * uPinv);
                %         uInvICleaned = uInvI - Q * (Q' * uInvI);
                %         uInvFCleaned = uInvF - Q * (Q' * uInvF);
                %
                %         norm(uPinvCleaned - uInvICleaned) / max(norm(uPinvCleaned), norm(uInvICleaned))
                %         norm(uPinvCleaned - uInvFCleaned) / max(norm(uPinvCleaned), norm(uInvFCleaned))
                
                
            end
        end
        
        
        % Construct domain data
        clear domains
        domains(1:prod(definition.assemblyResolution)) = struct('moduleId', [], 'boundary', [], 'shift', []);
        
        % Define elements
        nTx = definition.assemblyResolution(1);
        nTy = definition.assemblyResolution(2);
        
        nX = definition.moduleResolution(1);
        nY = definition.moduleResolution(2);
        
        nodesPerRow = (nTx*nX+1);
        nodesPerCol = (nY-1)*nTy;
        allNodesInRows = nodesPerRow*(nTy+1);
        
        for iTy = 1:nTy
            for iTx = 1:nTx
                iDomain = (iTy-1)*nTx + iTx;
                
                nodesAB = (iTy-1)*nodesPerRow + (iTx-1)*nX + 1 : (iTy-1)*nodesPerRow + iTx*nX + 1;
                nodesBD = allNodesInRows + iTx*nodesPerCol + (iTy-1)*(nY-1) + 1 : allNodesInRows + iTx*nodesPerCol + iTy*(nY-1);
                nodesDC = iTy*nodesPerRow + iTx*nX + 1 : -1 : iTy*nodesPerRow + (iTx-1)*nX + 1;
                nodesCA = allNodesInRows + (iTx-1)*nodesPerCol + iTy*(nY-1) : -1 : allNodesInRows + (iTx-1)*nodesPerCol + (iTy-1)*(nY-1) + 1;
                
                nodeA = nodesAB(1);
                nodeB = nodesAB(end);
                nodeC = nodesDC(end);
                nodeD = nodesDC(1);
                
                boundary.vertex = {nodeA, nodeB, nodeC, nodeD};
                boundary.edge = {fliplr(nodesCA)', nodesBD', nodesAB(2:end-1)', fliplr(nodesDC(2:end-1))'};
                
                boundary.whole = [ boundary.vertex{1}; boundary.vertex{2}; boundary.vertex{3}; boundary.vertex{4}; ...
                    boundary.edge{1}; boundary.edge{2}; boundary.edge{3}; boundary.edge{4} ];
                
                domains(iDomain).moduleId = definition.assemblyPlan(iTx, iTy);
                domains(iDomain).boundary = boundary;
                
                domains(iDomain).shift = [(iTx-1) * definition.moduleSize(1); (iTy-1) * definition.moduleSize(2)];
            end
        end
        
        left = 1;
        right = nodesPerRow;
        top = nTy*nodesPerRow + ceil(nodesPerRow / 2);
        
        if returnFiltering
            loading = struct('uPres', {}, 'Fext', {});
        else
            loading = struct('uPres', {[2*left-1, 0.0],[2*left, 0.0],[2*right, 0.0]}, 'Fext', {[2*top, -10]});
        end
        
        problem = struct( ...
            'loading', loading, ...
            'domains', domains, ...
            'modules', modules, ...
            'info', info);
        
        save(outputFilePath, 'problem');

    end
end
