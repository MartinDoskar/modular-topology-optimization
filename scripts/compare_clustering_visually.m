
clearvars;
addpath('./fun');

refData = load('P:/WP2/MTO/FMO_v2/nonuniFMO.mat');
cluData = load('P:/WP2/MTO/FMO_v2/martin_random2.mat');

selectedTiling = 1;

cluMap = cluData.randomTiling(selectedTiling).modularAssembly;
refMap = reshape(refData.nonuniformFMO.modularAssembly, size(cluMap));

uniqueMapping = unique(cluMap);
uniqueMapping(uniqueMapping == -1) = [];
uniqueMap = cluMap;
for i = 1:length(uniqueMapping)
   uniqueMap( uniqueMap == uniqueMapping(i) ) = i; 
end

% Visualize all optimized stiffness tensors (with cluster colouring)
cluColours = lines(length(uniqueMapping));
figure();
for i = 1:numel(refMap)
    if refMap(i) ~= - 1
%         D = cluData.randomTiling(selectedTiling).elasticTensors{ uniqueMapping(cluMap(i)) };
        
        D = refData.nonuniformFMO.elasticTensors{refMap(i)};
        visualize_stiffness_tensor(D, ...
            struct('faceColor', cluColours(uniqueMap(i),:), 'faceAlpha', 0.25) );
        
    end    
end
view(3);
grid on;

%% Visualize all optimized cluster stiffness tensors
cluColours = lines(length(uniqueMapping));
figure();
for i = 1:length(uniqueMapping)
    D = cluData.randomTiling(selectedTiling).elasticTensors{ uniqueMapping(i) };
    visualize_stiffness_tensor(D, ...
        struct('edgeColor', cluColours(uniqueMap(i),:), 'faceColor', cluColours(uniqueMap(i),:), 'faceAlpha', 0.25) );        
end
view(3);
grid on;
%%
% Visualize selected cluster
selectedCluster = 3;
figure();
for i = 1:numel(refMap)
    if uniqueMap(i) == selectedCluster        
        D = refData.nonuniformFMO.elasticTensors{refMap(i)};
        visualize_stiffness_tensor(D, ...
            struct('faceColor', cluColours(uniqueMap(i),:), 'faceAlpha', 0.25) );        
    elseif uniqueMap(i) ~= -1
        D = refData.nonuniformFMO.elasticTensors{refMap(i)};
        visualize_stiffness_tensor(D, ...
            struct('faceColor', 0.6*[1,1,1], 'faceAlpha', 0.10) );           
    end    
end
D = cluData.randomTiling(selectedTiling).elasticTensors{ uniqueMapping(selectedCluster) };
visualize_stiffness_tensor(D, ...
    struct('edgeColor', cluColours(selectedCluster,:), 'faceColor', cluColours(selectedCluster,:), 'faceAlpha', 0.50) ); 
view(3);
grid on;


%% Symbolic values for isotropic material
syms K G
assume(K, 'real');
assume(G, 'real');
assume(K > 0);
assume(G > 0);

% K = 100;
% G = 50;

% nu = (3*K - 2*G)/(2*(3*K + G));

D = [K + 4/3*G, K - 2/3*G, 0; K-2/3*G, K+4/3*G, 0; 0, 0, G];


[evecs, evals] = eig(D);
[X0, Y0, Z0] = sphere(20);

temp = D * [ X0(:)'; Y0(:)'; Z0(:)' ];

X = reshape(temp(1,:), size(X0));
Y = reshape(temp(2,:), size(Y0));
Z = reshape(temp(3,:), size(Z0));

surf(X,Y,Z, 'EdgeColor', [0.8500 0.3250 0.0980], 'FaceColor', [0.8500 0.3250 0.0980], 'FaceAlpha', 0.24 )

axis equal;