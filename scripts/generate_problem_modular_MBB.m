% GENERATE_PROBLEM_MODULAR_MBB creates an input JSON file for MTO.Application that
% reflects given assembly plan for MBB compliance problem
%
% Features: * Optionally, module-wise constant initial guess can be provided by an external file
%             (not used if inititialGuessFile variable is empty)
%           * Supports and loads distributed along 1/4 of tile
%
% Version:  0.4.2 (2022-01-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

totalSize = [1.0, 0.375];
moduleRes = 100;
filterType = 2;         % 0 - kernel-based density, 1 - PDE-based density, 2 - kernel-based sensitivity

nColours = 0;
problemName = 'mbb';

relVolumeConstraintInFMO = 0.4;     % Volume constraint used in FMO simulation
fixedEmptyModules = 0;              % Fixed empty modules (not considered in updating volume constraint)


%% Generate automatically input and output paths and files

[outputFolderJSON, outputFolderMTO, outputFileName, inputFileTiling, inititialGuessFile] = ... 
    set_output_paths(problemName, nColours, filterType, moduleRes);


%% Load assembly data
addpath('./fun');

[rawPlan, data] = load_til_file(inputFileTiling);
plan = rawPlan(:);

if (nColours == 1)
    % Artificially switch to PUC
    plan(plan ~= 0) = 1;
elseif (nColours == 0)
    % Artificially switch to fully-resolved geometry
    plan(plan ~= 0) = 1:nnz(plan ~= 0);
end


%% Impose BC and objectives

tileSize = totalSize ./ size(rawPlan);
elemSize = totalSize ./ (size(rawPlan)*moduleRes);
charElemSize = min(totalSize ./ (size(rawPlan)*moduleRes));

prescribedVals{1} = struct( "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], "upperBound", [0.0 + tileSize(1)/4.0 + charElemSize/10, 0.0 + charElemSize/10], "component", 0, "value", 0.0 );
prescribedVals{2} = struct( "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], "upperBound", [0.0 + tileSize(1)/4.0 + charElemSize/10, 0.0 + charElemSize/10], "component", 1, "value", 0.0 );
prescribedVals{3} = struct( "lowerBound", [totalSize(1) - tileSize(1)/4.0 - charElemSize/10, 0.0 - charElemSize/10], "upperBound", [totalSize(1) + charElemSize/10, 0.0 + charElemSize/10], "component", 1, "value", 0.0 );

loadIntensity = 1 / (tileSize(1) / 2.0);

prescribedLoads{1} = struct( "lowerBound", [totalSize(1)/2.0 - tileSize(1)/4.0 - charElemSize/10, totalSize(2) - charElemSize/10], "upperBound", [totalSize(1)/2.0 - tileSize(1)/4.0 + charElemSize/10, totalSize(2) + charElemSize/10], "component", 1, "value", - loadIntensity * elemSize(1) / 2.0  );
prescribedLoads{2} = struct( "lowerBound", [totalSize(1)/2.0 - tileSize(1)/4.0 + charElemSize/10, totalSize(2) - charElemSize/10], "upperBound", [totalSize(1)/2.0 + tileSize(1)/4.0 - charElemSize/10, totalSize(2) + charElemSize/10], "component", 1, "value", - loadIntensity * elemSize(1) );
prescribedLoads{3} = struct( "lowerBound", [totalSize(1)/2.0 + tileSize(1)/4.0 - charElemSize/10, totalSize(2) - charElemSize/10], "upperBound", [totalSize(1)/2.0 + tileSize(1)/4.0 + charElemSize/10, totalSize(2) + charElemSize/10], "component", 1, "value", - loadIntensity * elemSize(1) / 2.0 );


%% Create JSON

output.E0 = 1.0;
output.Emin = 1e-9;
output.nu = 0.3;

output.qMaxOC = 2.0;
output.qMultiplierOC = 1.01;
output.qAfterIterOC = 20;

output.limIter = 150;
output.limDesignChange = 1e-2;
output.limObjectiveChange = 1e-12;

output.penalisation = 3.0;
% output.penalisationInit = 1.0;
% output.penalisationIncrement = 0.1;
% output.penalisationStep = 5;
% output.penalisationLimit = 5.0;
% output.saveEachNthStep = 50;

output.saveEachNthStepVTK = 1000;
output.saveEachNthStepSVG = 1000;
output.saveModulesEachNthStepSVG = 1000;

output.outputFolder = outputFolderMTO;

output.discretisationType = 2;
output.assemblyResolution = size(rawPlan);
output.assemblyPlan = plan;

output.moduleSize = [totalSize(1)/output.assemblyResolution(1), totalSize(2)/output.assemblyResolution(2)];
output.moduleResolution = [moduleRes, moduleRes];

output.relativeVolumeConstraint = relVolumeConstraintInFMO * (numel(plan)-fixedEmptyModules) / (numel(plan) - nnz(plan == 0));

if exist('inititialGuessFile','var') && ~isempty(inititialGuessFile)
    initGuess = load(inititialGuessFile);
    output.modulewiseConstantInitialGuess = initGuess.x0;
    
    assert(length(initGuess.x0) == length(unique(plan(plan~=0))), ...
        'The number of initial guess values does not correspond to the number of distinct modules');
    
    if any(output.modulewiseConstantInitialGuess <= 0.0)
        warning('Provided initial guess should be positive (not satisfied in %i modules with max difference %e)', ...
            nnz(output.modulewiseConstantInitialGuess <= 0.0), ...
            max(abs(output.modulewiseConstantInitialGuess(output.modulewiseConstantInitialGuess <= 0.0) - 0.0)) );
    end
    if any(output.modulewiseConstantInitialGuess > 1.0)
        warning('Provided initial guess should not exceed 1.0 (not satisfied in %i modules with max difference %e)', ...
            nnz(output.modulewiseConstantInitialGuess > 1.0), ...
            max(abs(output.modulewiseConstantInitialGuess(output.modulewiseConstantInitialGuess > 1.0) - 1.0)) );
    end
    if abs(output.relativeVolumeConstraint - mean(output.modulewiseConstantInitialGuess(plan(plan>0)))) > 1e-6*max(output.relativeVolumeConstraint, mean(output.modulewiseConstantInitialGuess(plan(plan>0))))
        warning(...
            'Provided initial guess does not satisfy volume constraint (volume difference %e', ...
            abs(output.relativeVolumeConstraint - mean(output.modulewiseConstantInitialGuess(plan(plan>0)))));
    end
end

output.optimizerType = "OC";
output.stepOC = 0.2;

output.filterType = filterType;
output.filterRadius = 3.5*charElemSize;

output.problems{1} = struct( ...
    'prescribedValues', {prescribedVals}, ...
    'prescribedLoad', {prescribedLoads} ...
    );

%% Save JSON file
if ~isempty(outputFolderJSON)
    if ~exist(outputFolderJSON, 'dir')
        mkdir(outputFolderJSON);
    end
end

iF = fopen(fullfile(outputFolderJSON, outputFileName), 'wt');
fprintf(iF, '%s\n', jsonencode(output));
fclose(iF);
