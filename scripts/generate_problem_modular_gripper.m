% GENERATE_PROBLEM_MODULAR_GRIPPER creates an input JSON file for MTO.Application that
% reflects given assembly plan for gripper problem with additional layers
% of fixed elements
%
% Features: * Height of the fixed part is governed by relativeFixedPartHeight
%             (discarded with zero height)
%           * Optionally, module-wise constant initial guess can be provided in an external file
%             (not used if inititialGuessFile variable is empty)
%           * Supports distributed along relativeSupportHeight * tileSize(2) of tile
%
% Version:  0.4.1 (2022-01-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

totalSize = [1.0, 1.0];
moduleRes = 100;
filterType = 2;         % 0 - kernel-based density, 1 - PDE-based density, 2 - kernel-based sensitivity

nColours = 4;
problemName = 'grip';

relVolumeConstraintInFMO = 0.4;     % Volume constraint used in FMO simulation
fixedEmptyModules = 8;              % Fixed empty modules (not considered in updating volume constraint)

relativeFixedPartHeight = 0.25; % Height of the output block in units relative to module size (set 0 to turn off)
relativeSupportHeight = 0.25;   % Height of the support in units relative to module size


%% Generate automatically input and output paths and files

[outputFolderJSON, outputFolderMTO, outputFileName, inputFileTiling, inititialGuessFile] = ... 
    set_output_paths(problemName, nColours, filterType, moduleRes);


%% Load assembly data
addpath('./fun');

if mod(relativeFixedPartHeight * moduleRes, 1.0) > 0.0
    warning('Additional fixed part''s height cannot be accurate due to round off.')
end

[rawPlan, data] = load_til_file(inputFileTiling);
plan = rawPlan(:);

if (nColours == 1)
    % Artificially switch to PUC
    plan(plan ~= 0) = 1;
elseif (nColours == 0)
    % Artificially switch to fully-resolved geometry
    plan(plan ~= 0) = 1:nnz(plan ~= 0);
end


%% Impose BC and objectives

moduleSize = totalSize ./ size(rawPlan);
charElemSize = min(totalSize ./ (size(rawPlan)*moduleRes));

fixedPartVerticalElems = ceil(relativeFixedPartHeight * moduleRes);
fixedPartHeight = fixedPartVerticalElems * (totalSize(2)./(size(rawPlan,2)*moduleRes));

prescribedVals{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, 0.0 + (moduleSize(2) * relativeSupportHeight) + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{2} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, 0.0 + (moduleSize(2) * relativeSupportHeight) + charElemSize/10], ...
    "component", 1, "value", 0.0 );
prescribedVals{3} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, totalSize(2) - (moduleSize(2) * relativeSupportHeight) - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, totalSize(2) + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{4} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, totalSize(2) - (moduleSize(2) * relativeSupportHeight) - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, totalSize(2) + charElemSize/10], ...
    "component", 1, "value", 0.0 );

% Artificial restriction of vertical displacements on the horizontal axis of symmetry
% prescribedVals{5} = struct( ...
%     "lowerBound", [0.0 - charElemSize/10, totalSize(2)/2.0 - charElemSize/10], ...
%     "upperBound", [totalSize(1) + charElemSize/10, totalSize(2)/2.0 + charElemSize/10], ...
%     "component", 1, "value", 0.0 );

prescribedLoads{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, totalSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, totalSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 );

prescribedObjective{1} = struct( ...
    "lowerBound", [totalSize(1) - charElemSize/10, 3/10*totalSize(2) + fixedPartHeight - charElemSize/10], ...
    "upperBound", [totalSize(1) + charElemSize/10, 3/10*totalSize(2) + fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", -1.0 );
prescribedObjective{2} = struct( ...
    "lowerBound", [totalSize(1) - charElemSize/10, 7/10*totalSize(2) - fixedPartHeight - charElemSize/10], ...
    "upperBound", [totalSize(1) + charElemSize/10, 7/10*totalSize(2) - fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", 1.0 );

additionalStiffness{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, totalSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, totalSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 );
additionalStiffness{2} = struct(  ...
    "lowerBound", [totalSize(1) - charElemSize/10, 3/10*totalSize(2) + fixedPartHeight - charElemSize/10], ...
    "upperBound", [totalSize(1) + charElemSize/10, 3/10*totalSize(2) + fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", 0.025 );
additionalStiffness{3} = struct( ...
    "lowerBound", [totalSize(1) - charElemSize/10, 7/10*totalSize(2) - fixedPartHeight - charElemSize/10], ...
    "upperBound", [totalSize(1) + charElemSize/10, 7/10*totalSize(2) - fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", 0.025 );


%% Prepare additional discretisation
if relativeFixedPartHeight > 0.0
    additionalDomainLower = struct( ...
        'discretisationType', 0, ...
        'constantInitialGuess', 1.0, ...
        'domainResolution', [2*moduleRes, fixedPartVerticalElems], ...
        'domainOrigin', [4/5*totalSize(1), 3/10*totalSize(2)], ...
        'domainSize', [1/5*totalSize(1), fixedPartHeight ], ...
        'alwaysFilledElems', (1:(2*moduleRes * fixedPartVerticalElems))-1 );
    
    additionalDomainUpperAndLower = struct( ...
        'discretisationType', 0, ...
        'constantInitialGuess', 1.0, ...
        'domainResolution', [2*moduleRes, fixedPartVerticalElems], ...
        'domainOrigin', [4/5*totalSize(1), 7/10*totalSize(2) - fixedPartHeight ], ...
        'domainSize', [1/5*totalSize(1), fixedPartHeight ], ...
        'alwaysFilledElems', (1:(2*moduleRes * fixedPartVerticalElems))-1, ...
        'additionalDiscretisation', additionalDomainLower );
end


%% Create JSON

output.E0 = 1.0;
output.Emin = 1e-9;
output.nu = 0.3;

output.qMaxOC = 2.0;
output.qMultiplierOC = 1.01;
output.qAfterIterOC = 20;

output.limIter = 150;
output.limDesignChange = 1e-2;
output.limObjectiveChange = 1e-12;

output.penalisation = 3.0;
% output.penalisationInit = 1.0;
% output.penalisationIncrement = 0.1;
% output.penalisationStep = 5;
% output.penalisationLimit = 5.0;
% output.saveEachNthStep = 50;

output.saveEachNthStepVTK = 1000;
output.saveEachNthStepSVG = 1000;
output.saveModulesEachNthStepSVG = 1000;

output.outputFolder = outputFolderMTO;

output.discretisationType = 2;
output.assemblyResolution = size(rawPlan);
output.assemblyPlan = plan;
if relativeFixedPartHeight > 0.0
    output.additionalDiscretisation = additionalDomainUpperAndLower;
end

output.moduleSize = [totalSize(1)/output.assemblyResolution(1), totalSize(2)/output.assemblyResolution(2)];
output.moduleResolution = [moduleRes, moduleRes];

output.relativeVolumeConstraint = relVolumeConstraintInFMO * (numel(plan)-fixedEmptyModules) / (numel(plan) - nnz(plan == 0));

if exist('inititialGuessFile','var') && ~isempty(inititialGuessFile)
    initGuess = load(inititialGuessFile);
    output.modulewiseConstantInitialGuess = initGuess.x0;
    
    assert(length(initGuess.x0) == length(unique(plan(plan~=0))), ...
        'The number of initial guess values does not correspond to the number of distinct modules');
    
    if any(output.modulewiseConstantInitialGuess <= 0.0)
        warning('Provided initial guess should be positive (not satisfied in %i modules with max difference %e)', ...
            nnz(output.modulewiseConstantInitialGuess <= 0.0), ...
            max(abs(output.modulewiseConstantInitialGuess(output.modulewiseConstantInitialGuess <= 0.0) - 0.0)) );
    end
    if any(output.modulewiseConstantInitialGuess > 1.0)
        warning('Provided initial guess should not exceed 1.0 (not satisfied in %i modules with max difference %e)', ...
            nnz(output.modulewiseConstantInitialGuess > 1.0), ...
            max(abs(output.modulewiseConstantInitialGuess(output.modulewiseConstantInitialGuess > 1.0) - 1.0)) );
    end
    if abs(output.relativeVolumeConstraint - mean(output.modulewiseConstantInitialGuess(plan(plan>0)))) > 1e-6*max(output.relativeVolumeConstraint, mean(output.modulewiseConstantInitialGuess(plan(plan>0))))
        warning(...
            'Provided initial guess does not satisfy volume constraint (volume difference %e', ...
            abs(output.relativeVolumeConstraint - mean(output.modulewiseConstantInitialGuess(plan(plan>0)))));
    end
end

output.optimizerType = "OC";

output.filterType = filterType;
output.filterRadius = 3.5*charElemSize;

output.problems{1} = struct( ...
    'prescribedValues', {prescribedVals}, ...
    'prescribedLoad', {prescribedLoads}, ...
    'objectiveVector', {prescribedObjective}, ...
    'additionalStiffness', {additionalStiffness} ...
    );


%% Save JSON file
if ~isempty(outputFolderJSON)
    if ~exist(outputFolderJSON, 'dir')
        mkdir(outputFolderJSON);
    end
end

iF = fopen(fullfile(outputFolderJSON, outputFileName), 'wt');
fprintf(iF, '%s\n', jsonencode(output));
fclose(iF);
