
tiles = [...
    1 2 2 2; ...
    1 2 3 3; ...
    1 1 1 2; ...
    3 3 2 4; ...
    3 3 4 3; ...
    2 1 3 1; ...
    4 1 3 2; ...
    4 4 2 4; ...
    2 2 4 3; ...
    1 4 2 2; ...
    2 4 3 3 ];
tiling = [7, 2, 9, 9, 11, 7, 2, 9, 9, 9, 6, 2, 11, 7, 2, 9, 9, 9, 6, 2, 11; ...
          1, 11, 7, 2, 6, 1, 11, 7, 2, 6, 3, 2, 6, 1, 11, 7 2, 6, 3, 2, 6; ...
          10, 7, 10, 7, 3, 10, 7, 10, 7, 3, 10, 7, 3, 10, 7, 10, 7, 3, 10, 7, 3; ...
          8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8; ...
          5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5; ...
          11, 7, 2, 6, 2, 11, 7, 2, 6, 2, 11, 7, 2, 11, 7, 2, 6, 2, 11, 7, 2; ...
          7, 10, 7, 3, 2, 6, 10, 7, 3, 2, 6, 1, 11, 7, 10, 7, 3, 2, 6, 1, 11; ...
          10, 8, 8, 8, 7, 3, 10, 8, 8, 7, 3, 10, 7, 10, 8, 8, 8, 7, 3, 10, 7 ]';

%%

tileStencil = "T%i";
tileSize = [1, 1];

nTiles = size(tiles, 1);
sTiling = size(tiling);

edgeColourFill = {'#5aa02c', '#d40000', '#3771c8', '#ff6600'};
edgeColourStroke = {'#529028', '#c70000', '#3264b3', '#f06000'};

tilePositions = { [-10, 0], [-8.5, 0], [-7, 0], [-5.5, 0], [-10, 1.5], [-8.5, 1.5], [-7, 1.5], [-5.5, 1.5], [-9.25, 3.0], [-7.75, 3.0], [-6.25, 3.0] };

%%

iF = fopen('test.svg', 'wt+');

fprintf(iF, '<svg\n\twidth="210mm"\n\theight="297mm"\n\tviewBox="0 0 210 297"\n\tversion="1.1"\n\tid="til01"\n\txmlns="http://www.w3.org/2000/svg"\n\txmlns:svg="http://www.w3.org/2000/svg"\n\txmlns:xlink="http://www.w3.org/1999/xlink">\n');

fprintf(iF, '<defs>\n');
for iT = 1:nTiles    
    fprintf(iF, '\t<g id="%s">\n', sprintf(tileStencil, iT));
    
    fprintf(iF, '\t<path\n');
    fprintf(iF, '\t\tstyle="fill:%s;stroke:%s;stroke-width:0.01;stroke-linecap:square;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"\n', edgeColourFill{tiles(iT,1)}, edgeColourStroke{tiles(iT,1)} );
    fprintf(iF, '\t\td="m 0,296.15 c 0.2,0 0.2,0.2 0.2,0.2 0,0 0,0.10001 0,0.30001 0,0.2 -0.2,0.2 -0.2,0.2 V 296.15"\n');
    fprintf(iF, '\t\tid="%s" />\n', sprintf("edge%02i%02i", iT, 1));
    
    fprintf(iF, '\t<path\n');
    fprintf(iF, '\t\tstyle="fill:%s;stroke:%s;stroke-width:0.01;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"\n', edgeColourFill{tiles(iT,2)}, edgeColourStroke{tiles(iT,2)} );
    fprintf(iF, '\t\td="m 1,296.15 c -0.2,0 -0.2,0.2 -0.2,0.2 0,0 0,0.10001 0,0.30001 0,0.2 0.2,0.2 0.2,0.2 V 296.15"\n');
    fprintf(iF, '\t\tid="%s" />\n', sprintf("edge%02i%02i", iT, 2));
    
    fprintf(iF, '\t<path\n');
    fprintf(iF, '\t\tstyle="fill:%s;stroke:%s;stroke-width:0.01;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"\n', edgeColourFill{tiles(iT,3)}, edgeColourStroke{tiles(iT,3)} );
    fprintf(iF, '\t\td="m 0.15,297.00001 c 0,-0.2 0.2,-0.2 0.2,-0.2 0,0 0.10001,0 0.30001,0 0.2,0 0.2,0.2 0.2,0.2 H 0.15"\n');
    fprintf(iF, '\t\tid="%s" />\n', sprintf("edge%02i%02i", iT, 3));
    
    fprintf(iF, '\t<path\n');
    fprintf(iF, '\t\tstyle="fill:%s;stroke:%s;stroke-width:0.01;stroke-linecap:square;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"\n', edgeColourFill{tiles(iT,4)}, edgeColourStroke{tiles(iT,4)} );
    fprintf(iF, '\t\td="m 0.15,296.00001 c 0,0.2 0.2,0.2 0.2,0.2 0,0 0.10001,0 0.30001,0 0.2,0 0.2,-0.2 0.2,-0.2 H 0.15"\n');
    fprintf(iF, '\t\tid="%s" />\n', sprintf("edge%02i%02i", iT, 4));
    
    fprintf(iF, '\t<path\n');
    fprintf(iF, '\t\tstyle="fill:none;stroke:#666666;stroke-width:0.015;stroke-linecap:square;stroke-linejoin:miter;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0"\n');
    fprintf(iF, '\t\td="m 0,297 h 1 v -1 H 0 Z"\n');
    fprintf(iF, '\t\tid="%s" />\n', sprintf("border%02i", iT));
    
    fprintf(iF, '\t</g>\n');    
end
fprintf(iF, '</defs>\n');

for iY = 1:sTiling(2)
    for iX = 1:sTiling(1)
        id = tiling(iX, iY);
        fprintf(iF, '<use x="%e" y="%e" xlink:href="#%s"/>\n', (iX-1)*tileSize(1), -(iY-1)*tileSize(2), sprintf(tileStencil, id) );
    end
end

for iT = 1:nTiles
    fprintf(iF, '<use x="%e" y="%e" xlink:href="#%s"/>\n', tilePositions{iT}, sprintf(tileStencil, iT) );
end
        
fprintf(iF, '</svg>\n');

fclose(iF);