% Testing script for extracting state of a given module throughout topology
% optimization and check connectivity via Laplacian
%
% Version:  0.2.0 (2020-12-07)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun/');

resultsFileStencil = 'P:\\WP2\\MTO\\_compare_paper\\modular_100_022_001\\%i.vtk';
definitionFile = 'C:\Users\Huricane\Documents\MTO\MTO.Tests\data\inputs_paper.json';

stencilSteps = 0:1:100;
% chosenModuleId = 8;


%%

nStencilSteps = length(stencilSteps);
definition = parse_topopt_input_JSON(definitionFile);

modules = load_modular_geometry_from_VTK(...
    sprintf(resultsFileStencil, 1),...
    definition);
mesh = create_mesh_from_img(...
    modules(1).design, ...
    definition.moduleSize ./ definition.moduleResolution);

minEigVals = NaN(nStencilSteps, definition.nModules);
maxEigVals = NaN(nStencilSteps, definition.nModules);
for iS = 1:nStencilSteps 
    
   step = stencilSteps(iS);   
   resultsFile = sprintf(resultsFileStencil, step);
   
   modules = load_modular_geometry_from_VTK(resultsFile, definition);
   
   for iM = 1:definition.nModules
       if ~isempty(modules(iM).design)
           
           L = construct_Laplacian(mesh, modules(iM).design);
           
           L = full(L);
           evals = eig(L);
           sortedEvals = sort(evals, 'ascend');
           
           minEigVals(iS, iM) = sortedEvals(2);
           maxEigVals(iS, iM) = sortedEvals(end);
           
%            smallestEigvals = svds(L, 5, 'smallest');
%            largestEigvals = svds(L, 5, 'largest');
%            largestEigvals = largestEigvals(~isnan(largestEigvals));

%            minEigVals(iS, iM) = smallestEigvals(end-1);
%            if ~isempty(largestEigvals)
%                maxEigVals(iS, iM) = max(largestEigvals);
%            end
           
       end
   end
   
end

% save('computedData.mat', '-v7.3');
% load('computedData.mat');


%% Plot results for different modules

nModules = length(modules);

iFig = figure();
lineColours = lines(nModules);
lineHandles = zeros(nModules, 1);
lineLabels = cell(nModules, 1);
hold on;
for iM = 1:nModules
lineHandles(iM) = plot(stencilSteps, minEigVals(:,iM), '-', 'Color', lineColours(iM,:));
lineLabels{iM} = sprintf('Module %i', iM);
plot(stencilSteps, maxEigVals(:,iM), '-.', 'Color', lineColours(iM,:));
end
hold off;

grid on;
xl = xlabel('Iteration no.', 'Interpreter', 'latex');
yl = ylabel('Eigenvalue', 'Interpreter', 'latex');
set(gca(), 'YScale', 'log', 'TickLabelInterpreter', 'latex');
ll = legend(lineHandles, lineLabels, 'Interpreter', 'latex', 'EdgeColor', 0.999*[1,1,1]);


%% Plot relation between volume fraction and eigenvalue
volFracs = zeros(nModules, 1);
for iM = 1:nModules
   volFracs(iM) = mean(modules(iM).design(:));
end

iFig = figure();
scatter(volFracs, maxEigVals(end,:))

xl = xlabel('Volume fraction', 'Interpreter', 'latex');
yl = ylabel('Largest eigenvalue', 'Interpreter', 'latex');
set(gca(), 'XScale', 'lin', 'YScale', 'lin', 'TickLabelInterpreter', 'latex');
% set(gca(), 'XScale', 'log', 'YScale', 'log', 'TickLabelInterpreter', 'latex');


%% Test individual module
iSelectedModule = 8;

figure();
imshow(modules(iSelectedModule).design > 0.5);

figure();
plot(stencilSteps, minEigVals(:,iSelectedModule), '-.');
hold on;
plot(stencilSteps, maxEigVals(:,iSelectedModule), '--');
hold off;
set(gca(), 'YScale', 'log');

shift_value = 1e-3;
shift_data = @(data) max(min(data, 1.0), 0.0) .* (1 - shift_value) + shift_value;

L = full(construct_Laplacian(mesh, shift_data(modules(iSelectedModule).design)) );
L = 0.5*(L+L');
[eves, evas] = eig(L);
[sortedEvas, sortedIndex] = sort(abs(diag(evas)));

figure();
surf(reshape(eves(:,sortedIndex(6)), [101,101]))
% surf(modules(iSelectedModule).design)


%% Test different approaches to avoid singularity
iSelectedModule = 8;

L = construct_Laplacian(mesh, modules(8).design);
tic

Lfull = full(L);
[evecs, evals] = eig(Lfull);
fprintf('Ver1: min %e, max %e\n', evals(2,2), max(diag(evals)));


Lfull = full(L);
Lfull = Lfull + 1e-2 * diag(ones(size(Lfull,1),1));
[evecs, evals] = eig(Lfull);
fprintf('Ver2: min %e, max %e\n', min(diag(evals)), max(diag(evals)));

Lfull = full(L);
evec0 = ones(size(Lfull,1),1);
evec0 = evec0 ./ (evec0'*evec0);
Lfull = Lfull + 1e-2 * (evec0*evec0');
[evecs, evals] = eig(Lfull);
fprintf('Ver2: min %e, max %e\n', min(diag(evals)), max(diag(evals)));

Lfull = full(L);
Lfull(1,1) = Lfull(1,1) + 1e-2;
[evecs, evals] = eig(Lfull);
fprintf('Ver3: min %e, max %e\n', min(diag(evals)), max(diag(evals)));

Lfull = full(L);
[evecs, evals] = eig(Lfull(2:end,2:end));
fprintf('Ver4: min %e, max %e\n', min(diag(evals)), max(diag(evals)));

toc