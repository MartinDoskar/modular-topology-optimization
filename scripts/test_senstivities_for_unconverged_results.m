clearvars;
addpath('.\fun');

input = load('C:\Users\marti\Downloads\problem_10x10_step3_v2.mat');
results = load('C:\Users\marti\Downloads\problem_10x10_step3.mat');

p = 3;
E0 = 1.0;
Emin = 1.0e-9;
nu = 0.3;

selectedIteration = 4;

%%
nDOFsElem = 8;

oneOverSqrtThree = 0.577350269189626;
quadrature(1) = struct('coords', [-oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(2) = struct('coords', [+oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(3) = struct('coords', [-oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );
quadrature(4) = struct('coords', [+oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );

D0 = get_default_material_stiffness(E0, nu);
X = input.problem.modules(1).mesh.nodes(input.problem.modules(1).mesh.elements(1,:), 1:2);
K0 = zeros(nDOFsElem);
for iG = 1:length(quadrature)        
    Bparam = get_B_mtrx(quadrature(iG).coords(1), quadrature(iG).coords(2));
    J = Bparam * X;
    Bcoord = J \ Bparam;

    B = [ Bcoord(1,1), 0.0, Bcoord(1,2), 0.0, Bcoord(1,3), 0.0, Bcoord(1,4), 0.0; ...
         0.0, Bcoord(2,1), 0.0, Bcoord(2,2), 0.0, Bcoord(2,3), 0.0, Bcoord(2,4); ...
         Bcoord(2,1), Bcoord(1,1), Bcoord(2,2), Bcoord(1,2), Bcoord(2,3), Bcoord(1,3), Bcoord(2,4), Bcoord(1,4) ];

    K0 = K0 + B'*(D0*B) * quadrature(iG).weight * det(J);
end

%%


nDomains = length(input.problem.domains);
trueSensitivity = cell(nDomains, 1);
interimSensitivity = cell(nDomains, 1);

for iD = 1:nDomains
    
    
   domSolIter = results.problem_10x10_step3.ui_pcgm{selectedIteration}{iD};
   domSolFinal = results.problem_10x10_step3.ui{iD};
   
   iM = input.problem.domains(iD).moduleId;
   
   trueSensitivity{iD} = nan(input.problem.modules(iM).mesh.nElems, 1);
   interimSensitivity{iD} = nan(input.problem.modules(iM).mesh.nElems, 1);
   
   for iE = 1:input.problem.modules(iM).mesh.nElems
       density = input.problem.modules(iM).design(iE);
       iNodes = input.problem.modules(1).mesh.elements(iE,:);
       iDOFs = reshape( [2*iNodes-1; 2*iNodes], [], 1);
       
       Kloc = p*density^(p-1)*(E0 - Emin) * K0;
       
       uLocIter = domSolIter(iDOFs);       
       interimSensitivity{iD}(iE) = -uLocIter'* Kloc * uLocIter;
       
       uLocFinal = domSolFinal(iDOFs);       
       trueSensitivity{iD}(iE) = -uLocFinal'* Kloc * uLocFinal;
        
   end
   
end

%%
relError = cell(size(trueSensitivity));
for iD = 1:nDomains
   relError{iD} = abs(trueSensitivity{iD} - interimSensitivity{iD}) ./ abs(trueSensitivity{iD}); 
end

plot_decomposed_solution(results.problem_10x10_step3.ui_pcgm{selectedIteration}, input.problem, 0.0001, trueSensitivity );

plot_decomposed_solution(results.problem_10x10_step3.ui_pcgm{selectedIteration}, input.problem, 0.0001, interimSensitivity );

plot_decomposed_solution(results.problem_10x10_step3.ui_pcgm{selectedIteration}, input.problem, 0.0001, relError );
colorbar();
set(gca,'colorscale','log');