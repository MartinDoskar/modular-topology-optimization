% GENERATE_PROBLEM_MODULAR_INVERTERANDGRIPPER creates an input JSON file
% for MTO.Application that reflects the given combined assembly plan for
% the gripper and the inverter problem (similarly to standalone ones)
%
% Features: * Two problems are assumed to be separated by a column of empty
%             modules; gripper first followed by inverter
%           * Generator supports two rigid areas:
%               - Height of the fixed part in gripper is governed by relativeFixedGripperPartHeight
%                 (discarded with zero height)
%               - Rigid block at inverter output is parametrized via relativeFixedInverterPart{Height,Width}
%           * Optionally, initial guess can be provided by an external file
%             (not used if inititialGuessFile variable is empty) with two
%             options:
%               - module-wise constant field is used as default
%               - spatially varying field can be provided in initTemplate variable
%           * Modify weights of individual problems
%           * Supports distributed along relativeSupportHeight * tileSize(2) of tile
%
% Version:  0.4.0 (2022-01-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

oneProblemSize = [1.0, 1.0];
moduleRes = 100;
filterType = 2;         % 0 - kernel-based density, 1 - PDE-based density, 2 - kernel-based sensitivity

nColours = 4;
problemName = 'invgrip';

relVolumeConstraintInFMO = 0.4;     % Volume constraint used in FMO simulation
fixedEmptyModules = 18;             % Fixed empty modules (not considered in updating volume constraint)

relativeFixedGripperPartHeight = 0.25;       % Height of the output gripper block in units relative to module size (set 0 to turn off)
relativeFixedInverterPartHeight = 0.0;       % Heigth of the output block in units relative to module size (set 0 to turn off)
relativeFixedInverterPartWidth = 0.0;        % Width of the output block in units relative to module size (set 0 to turn off)
relativeSupportHeight = 0.25;                % Height of the support in units relative to module size

gripperWeight = 0.60;
inverterWeight = 1.0;


%% Generate automatically input and output paths and files

[outputFolderJSON, outputFolderMTO, outputFileName, inputFileTiling, inititialGuessFile] = ... 
    set_output_paths(problemName, nColours, filterType, moduleRes);

% Define initTemplate to modify module-wise constant intial guess
% [Y, X] = meshgrid( ((1:moduleRes) - 0.5)./moduleRes, ((1:moduleRes) - 0.5)./moduleRes);
% X = X(:);
% Y = Y(:);
% initTemplate = ones(size(X));


%% Load assembly data
addpath('./fun');

[rawPlan, data] = load_til_file(inputFileTiling);
plan = rawPlan(:);

if (nColours == 1)
    % Artificially switch to PUC
    plan(plan ~= 0) = 1;
elseif (nColours == 0)
    % Artificially switch to fully-resolved geometry
    plan(plan ~= 0) = 1:nnz(plan ~= 0);
end

oneProblemModularisation = (size(rawPlan,1) - 1)/2;
assert(oneProblemModularisation == size(rawPlan,2), 'The script is designed for two equisized and equimodularized problems.');

offsetSize = oneProblemSize(1)/oneProblemModularisation;
inverterOffset = oneProblemSize(1) + offsetSize;
totalSize = [size(rawPlan,1)/oneProblemModularisation * oneProblemSize(1), oneProblemSize(2)];


%% Impose BC and objectives
moduleSize = totalSize ./ size(rawPlan);
charElemSize = min(oneProblemSize ./ (oneProblemModularisation*moduleRes));

fixedPartVerticalElems = ceil(relativeFixedGripperPartHeight * moduleRes);
fixedPartHeight = fixedPartVerticalElems * (oneProblemSize(2)./(oneProblemModularisation*moduleRes));

% Part 1: Gripper
prescribedVals{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, 0.0 + (moduleSize(2) * relativeSupportHeight) + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{end+1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, 0.0 + (moduleSize(2) * relativeSupportHeight) + charElemSize/10], ...
    "component", 1, "value", 0.0 );
prescribedVals{end+1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, totalSize(2) - (moduleSize(2) * relativeSupportHeight) - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, totalSize(2) + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{end+1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, totalSize(2) - (moduleSize(2) * relativeSupportHeight) - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, totalSize(2) + charElemSize/10], ...
    "component", 1, "value", 0.0 );

% Artificial restriction of vertical displacements on the horizontal axis of symmetry
% prescribedVals{end+1} = struct( ...
%     "lowerBound", [0.0 - charElemSize/10, totalSize(2)/2.0 - charElemSize/10], ...
%     "upperBound", [totalSize(1) + charElemSize/10, totalSize(2)/2.0 + charElemSize/10], ...
%     "component", 1, "value", 0.0 );

prescribedLoads{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, oneProblemSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, oneProblemSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 );

prescribedObjective{1} = struct( ...
    "lowerBound", [oneProblemSize(1) - charElemSize/10, 3/10*oneProblemSize(2) + fixedPartHeight - charElemSize/10], ...
    "upperBound", [oneProblemSize(1) + charElemSize/10, 3/10*oneProblemSize(2) + fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", -1.0 * gripperWeight );
prescribedObjective{end+1} = struct( ...
    "lowerBound", [oneProblemSize(1) - charElemSize/10, 7/10*oneProblemSize(2) - fixedPartHeight - charElemSize/10], ...
    "upperBound", [oneProblemSize(1) + charElemSize/10, 7/10*oneProblemSize(2) - fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", 1.0 * gripperWeight );

additionalStiffness{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, oneProblemSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, oneProblemSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 );
additionalStiffness{end+1} = struct(  ...
    "lowerBound", [oneProblemSize(1) - charElemSize/10, 3/10*oneProblemSize(2) + fixedPartHeight - charElemSize/10], ...
    "upperBound", [oneProblemSize(1) + charElemSize/10, 3/10*oneProblemSize(2) + fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", 0.025 );
additionalStiffness{end+1} = struct( ...
    "lowerBound", [oneProblemSize(1) - charElemSize/10, 7/10*oneProblemSize(2) - fixedPartHeight - charElemSize/10], ...
    "upperBound", [oneProblemSize(1) + charElemSize/10, 7/10*oneProblemSize(2) - fixedPartHeight + charElemSize/10], ...
    "component", 1, "value", 0.025 );

% Part 2: Inverter
prescribedVals{end+1} = struct( ...
    "lowerBound", [inverterOffset - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [inverterOffset + charElemSize/10, 0.0 + (moduleSize(2) * relativeSupportHeight) + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{end+1} = struct( ...
    "lowerBound", [inverterOffset - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [inverterOffset + charElemSize/10, 0.0 + (moduleSize(2) * relativeSupportHeight) + charElemSize/10], ...
    "component", 1, "value", 0.0 );
prescribedVals{end+1} = struct( ...
    "lowerBound", [inverterOffset - charElemSize/10, totalSize(2) - (moduleSize(2) * relativeSupportHeight) - charElemSize/10], ...
    "upperBound", [inverterOffset + charElemSize/10, oneProblemSize(2) + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{end+1} = struct( ...
    "lowerBound", [inverterOffset - charElemSize/10, totalSize(2) - (moduleSize(2) * relativeSupportHeight) - charElemSize/10], ...
    "upperBound", [inverterOffset + charElemSize/10, oneProblemSize(2) + charElemSize/10], ...
    "component", 1, "value", 0.0 );

prescribedLoads{end+1} = struct( ...
    "lowerBound", [inverterOffset - charElemSize/10, oneProblemSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [inverterOffset + charElemSize/10, oneProblemSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 );

prescribedObjective{end+1} = struct( ...
    "lowerBound", [totalSize(1) - charElemSize/10, oneProblemSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [totalSize(1) + charElemSize/10, oneProblemSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 * inverterWeight );

additionalStiffness{end+1} = struct( ...
    "lowerBound", [inverterOffset - charElemSize/10, oneProblemSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [inverterOffset + charElemSize/10, oneProblemSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 1.0 );
additionalStiffness{end+1} = struct( ...
    "lowerBound", [totalSize(1) - charElemSize/10, oneProblemSize(2)/2.0 - charElemSize/10], ...
    "upperBound", [totalSize(1) + charElemSize/10, oneProblemSize(2)/2.0 + charElemSize/10], ...
    "component", 0, "value", 0.05 );


%% Prepare additional discretisation
if relativeFixedGripperPartHeight > 0.0
    additionalGripperDomainLower = struct( ...
        'discretisationType', 0, ...
        'constantInitialGuess', 1.0, ...
        'domainResolution', [2*moduleRes, fixedPartVerticalElems], ...
        'domainOrigin', [4/5*oneProblemSize(1), 3/10*oneProblemSize(2)], ...
        'domainSize', [1/5*oneProblemSize(1), fixedPartHeight ], ...
        'alwaysFilledElems', (1:(2*moduleRes * fixedPartVerticalElems))-1 );
    
    additionalGripperDomainUpperAndLower = struct( ...
        'discretisationType', 0, ...
        'constantInitialGuess', 1.0, ...
        'domainResolution', [2*moduleRes, fixedPartVerticalElems], ...
        'domainOrigin', [4/5*oneProblemSize(1), 7/10*oneProblemSize(2) - fixedPartHeight ], ...
        'domainSize', [1/5*oneProblemSize(1), fixedPartHeight ], ...
        'alwaysFilledElems', (1:(2*moduleRes * fixedPartVerticalElems))-1, ...
        'additionalDiscretisation', additionalGripperDomainLower );
    
    additionalDomain = additionalGripperDomainUpperAndLower;
end

if relativeFixedInverterPartHeight > 0.0 && relativeFixedInverterPartWidth > 0.0
    
    fixedInverterPartHorizontalElems = ceil(relativeFixedInverterPartWidth*moduleRes);
    fixedInverterPartVerticalElems = ceil(relativeFixedInverterPartHeight*moduleRes);
    fixedInverterPartWidth = fixedInverterPartHorizontalElems * (totalSize(1)./(size(rawPlan,1)*moduleRes));
    fixedInverterPartHeight = fixedInverterPartVerticalElems * (totalSize(2)./(size(rawPlan,2)*moduleRes));
    
    additionalInverterDomain = struct( ...
        'discretisationType', 0, ...
        'constantInitialGuess', 1.0, ...
        'domainResolution', [fixedInverterPartHorizontalElems, fixedInverterPartVerticalElems], ...
        'domainOrigin', [totalSize(1), 0.5*totalSize(2)-0.5*fixedInverterPartHeight], ...
        'domainSize', [fixedInverterPartWidth, fixedInverterPartHeight], ...
        'alwaysFilledElems', 0:(fixedInverterPartHorizontalElems*fixedInverterPartVerticalElems-1) );
    
    if exist('additionalDomain', 'var')
        additionalInverterDomain.additionalDiscretisation = additionalDomain;
        additionalDomain = additionalInverterDomain;        
    else
        additionalDomain = additionalInverterDomain;
    end
end

%% Create JSON

output.E0 = 1.0;
output.Emin = 1e-9;
output.nu = 0.3;

output.qMaxOC = 2.0;
output.qMultiplierOC = 1.01;
output.qAfterIterOC = 20;

output.limIter = 150;
output.limDesignChange = 1e-2;
output.limObjectiveChange = 1e-12;

output.penalisation = 3.0;
% output.penalisationInit = 3.0;
% output.penalisationIncrement = 0.25;
% output.penalisationStep = 25;
% output.penalisationLimit = 5.0;

output.saveEachNthStepVTK = 1000;
output.saveEachNthStepSVG = 1000;
output.saveModulesEachNthStepSVG = 1000;

output.outputFolder = outputFolderMTO;

output.discretisationType = 2;
output.assemblyResolution = size(rawPlan);
output.assemblyPlan = plan;
if exist('additionalDomain', 'var')
    output.additionalDiscretisation = additionalDomain;
end

output.moduleSize = oneProblemSize ./ oneProblemModularisation;
output.moduleResolution = [moduleRes, moduleRes];

output.relativeVolumeConstraint = relVolumeConstraintInFMO * (numel(plan)-fixedEmptyModules) / (numel(plan) - nnz(plan == 0));

if exist('inititialGuessFile','var') && ~isempty(inititialGuessFile)
    initGuess = load(inititialGuessFile);
    
    if exist('initTemplate', 'var') && ~isempty(initTemplate)
        initGuesses = cell(length(initGuess.x0), 1);
        for iM = 1:length(initGuess.x0)
            %            r = sqrt( (1 - initGuess.x0(iM)) / pi );
            %            initCurrent = initTemplate;
            %            initCurrent( ((X-0.5).^2 + (Y-0.5).^2).^0.5 <= r ) = 0.0;
            %            initGuesses{iM} = initCurrent;
            
            initCurrent = initTemplate;
            initCurrent( ((X-0.5).^2 + (Y-0.5).^2).^0.5 <= 0.15 ) = 0.0;
            phi = sum(initCurrent)/numel(initCurrent);
            
            minRho = 0.1;
            maxRho = 1/phi*(initGuess.x0(iM) - (1-phi)*minRho);
            maxRho = min(1.0, maxRho);
            
            initGuesses{iM} = maxRho*initCurrent + minRho*(1-initCurrent);
            fprintf('Module no. %i: in density %f vs out density %f\n', iM, initGuess.x0(iM), mean(initGuesses{iM}));
        end
        output.modulewiseConstantInitialGuess = initGuesses;
    else
        output.modulewiseConstantInitialGuess = initGuess.x0;
        
        assert(length(initGuess.x0) == length(unique(plan(plan~=0))), ...
            'The number of initial guess values does not correspond to the number of distinct modules');

        if any(output.modulewiseConstantInitialGuess <= 0.0)
            warning('Provided initial guess should be positive (not satisfied in %i modules with max difference %e)', ...
                nnz(output.modulewiseConstantInitialGuess <= 0.0), ...
                max(abs(output.modulewiseConstantInitialGuess(output.modulewiseConstantInitialGuess <= 0.0) - 0.0)) );
        end
        if any(output.modulewiseConstantInitialGuess > 1.0)
            warning('Provided initial guess should not exceed 1.0 (not satisfied in %i modules with max difference %e)', ...
                nnz(output.modulewiseConstantInitialGuess > 1.0), ...
                max(abs(output.modulewiseConstantInitialGuess(output.modulewiseConstantInitialGuess > 1.0) - 1.0)) );
        end
        if abs(output.relativeVolumeConstraint - mean(output.modulewiseConstantInitialGuess(plan(plan>0)))) > 1e-6*max(output.relativeVolumeConstraint, mean(output.modulewiseConstantInitialGuess(plan(plan>0))))
            warning(...
                'Provided initial guess does not satisfy volume constraint (volume difference %e', ...
                abs(output.relativeVolumeConstraint - mean(output.modulewiseConstantInitialGuess(plan(plan>0)))));
        end
    end
end

output.optimizerType = "OC";

output.filterType = filterType;
output.filterRadius = 3.5*charElemSize;

output.problems{1} = struct( ...
    'prescribedValues', {prescribedVals}, ...
    'prescribedLoad', {prescribedLoads}, ...
    'objectiveVector', {prescribedObjective}, ...
    'additionalStiffness', {additionalStiffness} ...
    );

%% Save JSON file
if ~isempty(outputFolderJSON)
    if ~exist(outputFolderJSON, 'dir')
        mkdir(outputFolderJSON);
    end
end

iF = fopen(fullfile(outputFolderJSON, outputFileName), 'wt');
fprintf(iF, '%s\n', jsonencode(output));
fclose(iF);
