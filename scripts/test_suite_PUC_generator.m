% TEST_SUITE_PUC_GENERATOR creates a FE discretization of repeating
% domains from Gosselet et al. (2015) Simultaneous FETI and
% block FETI: Robust domain decomposition with multiple search directions.
%
% Version:  0.1.2 (2021-05-10)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

problemType = 'beam';        % Pick one of 'beam', 'gridLayered', 'gridSquare';
domainSize = 1.0;            % Geometrical size of a PUC subdomain

outputFolder = './data/';


%%

% Generate T3D input
if strcmpi(problemType, 'beam')
    outputFileName = 'PUC_7layers';
    nLayers = 7;
    relElemSize = 0.1;          % Default size of the FE discretization (relative to size)
    produce_T3D_input_layered( fullfile(outputFolder, [outputFileName, '.in']), nLayers, domainSize );
    
elseif strcmpi(problemType, 'gridLayered')
    outputFileName = 'PUC_6layers';
    nLayers = 6;
    relElemSize = 0.1;          % Default size of the FE discretization (relative to size)
    produce_T3D_input_layered( fullfile(outputFolder, [outputFileName, '.in']), nLayers, domainSize );
    
elseif strcmpi(problemType, 'gridSquare')
    outputFileName = 'PUC_square';
    sqareLengthRatio = 0.8;
    relElemSize = 0.05;          % Default size of the FE discretization (relative to size)
    produce_T3D_input_square_in_the_middle( fullfile(outputFolder, [outputFileName, '.in']), sqareLengthRatio, domainSize );
    
else
    error('Unsupported type variant');
    
end

% Process T3D input
[cmdStatus, cmdOutput] = system('bash -c "t3d"');
if cmdStatus == 0
    
    [cmdStatus, cmdOutput] = system( ...
        sprintf('bash -c "t3d -i %s -o %s -d %f"', ...
            strrep(fullfile(outputFolder, [outputFileName, '.in']), '\', '/'), ...
            strrep(fullfile(outputFolder, [outputFileName, '.out']), '\', '/'), ...
            relElemSize * domainSize ));
        
    if cmdStatus ~= 0
        disp(cmdOutput);
        error('Unable to run T3D command');
    end
else
    % Require manual interaction
    fprintf('Run T3D externally\n');
end

% Visualize produced mesh
mesh = load_T3D_mesh(fullfile(outputFolder, [outputFileName, '.out']), true);
plot_initial_and_final_configuration(mesh);



%% Auxiliary functions

function produce_T3D_input_layered( filePath, nLayers, domainSize )

    iF = fopen(filePath, 'wt');
    for iL = 0:nLayers
       fprintf(iF, 'vertex %i xyz %.16f %.16f 0.0 size def\n', 10*(iL+1)+1, 0.0, iL*(domainSize/nLayers));  
       fprintf(iF, 'vertex %i xyz %.16f %.16f 0.0 size def\n', 10*(iL+1)+2, domainSize, iL*(domainSize/nLayers));  
    end

    fprintf(iF, '\n');
    for iL = 1:nLayers
        fprintf(iF, 'curve %i vertex %i %i order 2 size def\n', 10*iL, 10*iL+1, 10*iL+2);  

        fprintf(iF, 'curve %i vertex %i %i order 2 size def\n', 10*iL + 1, 10*iL+1, 10*(iL+1)+1);
        fprintf(iF, 'curve %i vertex %i %i order 2 mirror %i size def\n', 10*iL + 2, 10*iL+2, 10*(iL+1)+2, 10*iL + 1);  
    end
    fprintf(iF, 'curve %i vertex %i %i order 2 mirror %i size def\n', 10*(nLayers+1), 10*(nLayers+1)+1, 10*(nLayers+1)+2, 10*1);  

    fprintf(iF, '\n');
    for iL = 1:nLayers
        fprintf(iF, ...
            'patch %i normal 0 0 1 boundary curve %i %i %i %i property %i size def\n', ...
            iL, 10*iL, 10*iL+2, -(10*(iL+1)), -(10*iL+1), 2-mod(iL,2));    
    end

    fclose(iF);
end

function produce_T3D_input_square_in_the_middle( filePath, sqareLengthRatio, domainSize )

    iF = fopen(filePath, 'wt');
    fprintf(iF, 'vertex 1 xyz %.16f %.16f 0.0 size def\n', -domainSize/2, -domainSize/2);  
    fprintf(iF, 'vertex 2 xyz %.16f %.16f 0.0 size def\n', +domainSize/2, -domainSize/2);  
    fprintf(iF, 'vertex 3 xyz %.16f %.16f 0.0 size def\n', -domainSize/2, +domainSize/2); 
    fprintf(iF, 'vertex 4 xyz %.16f %.16f 0.0 size def\n', +domainSize/2, +domainSize/2); 
    fprintf(iF, 'vertex 11 xyz %.16f %.16f 0.0 size def\n', -(sqareLengthRatio*domainSize)/2, -(sqareLengthRatio*domainSize)/2);  
    fprintf(iF, 'vertex 12 xyz %.16f %.16f 0.0 size def\n', +(sqareLengthRatio*domainSize)/2, -(sqareLengthRatio*domainSize)/2);  
    fprintf(iF, 'vertex 13 xyz %.16f %.16f 0.0 size def\n', -(sqareLengthRatio*domainSize)/2, +(sqareLengthRatio*domainSize)/2); 
    fprintf(iF, 'vertex 14 xyz %.16f %.16f 0.0 size def\n', +(sqareLengthRatio*domainSize)/2, +(sqareLengthRatio*domainSize)/2); 

    fprintf(iF, 'curve 1 vertex 1 2 order 2 size def\n');  
    fprintf(iF, 'curve 2 vertex 3 4 order 2 mirror 1 size def\n');  
    fprintf(iF, 'curve 3 vertex 1 3 order 2 size def\n');  
    fprintf(iF, 'curve 4 vertex 2 4 order 2 mirror 3 size def\n');  
    fprintf(iF, 'curve 11 vertex 11 12 order 2 size def\n');  
    fprintf(iF, 'curve 12 vertex 13 14 order 2 size def\n');  
    fprintf(iF, 'curve 13 vertex 11 13 order 2 size def\n');  
    fprintf(iF, 'curve 14 vertex 12 14 order 2 size def\n');  

    fprintf(iF, 'patch 1 normal 0 0 1 boundary curve 1 4 -2 -3 -11 -14 12 13 property 1 size def\n');
    fprintf(iF, 'patch 11 normal 0 0 1 boundary curve 11 14 -12 -13 property 2 size def\n');

    fclose(iF);
    
end