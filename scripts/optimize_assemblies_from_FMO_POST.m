% UNIQUELY_REMAP_ASSEMBLY_PLAN renumbers given matrix in a unique manner
% (using positions of the first code occurence as the ordering index)
%
% Version:  0.1.0 (2020-12-14)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

addpath('./fun');

inputs{1} = 'P:\WP2\MTO\FMO\ass2_v2\results.mat'; labels{1} = '2 codes, v2';
inputs{2} = 'P:\WP2\MTO\FMO\ass3_v2\results.mat'; labels{2} = '3 codes, v2'; 
inputs{3} = 'P:\WP2\MTO\FMO\ass4_v2\results.mat'; labels{3} = '4 codes, v2'; 
inputs{4} = 'P:\WP2\MTO\FMO\ass4_v1\results.mat'; labels{4} = '4 codes, v1'; 
inputs{5} = 'P:\WP2\MTO\FMO\ass5_v1\results.mat'; labels{5} = '5 codes, v1'; 


%%
lineColours = lines(length(inputs));
plotHandles = nan(length(inputs), 1);

iFig1 = figure();
iFig2 = figure();

for iI = 1:length(inputs)
    
    data = load(inputs{iI});
    
    figure(iFig2)
    hold on;
    hist(data.optimizedCompliance, 1000);
    h = findobj(gca,'Type','patch');
    h(1).FaceColor = lineColours(iI,:);
    h(1).EdgeColor = lineColours(iI,:);
    hold off;
    
    figure(iFig1);
    nData = (length(data.uniqueData)-2);
    byRow = zeros(nData, numel(data.uniqueData{1}));    
    for iD = 1:nData
        uniquePlan = uniquely_remap_assembly_plan(data.uniqueData{iD});
        nModules = length(unique(uniquePlan));
        
        byRow(iD,:) = uniquePlan(:);
        
        hold on;
        plh = scatter(nModules, data.optimizedCompliance(iD), 20, lineColours(iI,:), 'o', 'filled');
        if iD == 1
            plotHandles(iI) = plh;
        end
        hold off;
    end
    
    nUniqueData = size(unique(byRow, 'stable', 'rows'), 1);
    
    fprintf('Input no. %i: %i -> %i\n', iI, nData, nUniqueData);
    
end

figure(iFig1);
grid on;
box off;
set(gca(), 'TickLabelInterpreter', 'latex', 'FontSize', 13);
legend(plotHandles, labels, 'EdgeColor', 0.99*[1,1,1], 'Interpreter', 'latex', 'FontSize', 13);
ylabel('Objective', 'Interpreter', 'latex', 'FontSize', 13)
xlabel('No. modules', 'Interpreter', 'latex', 'FontSize', 13)

figure(iFig2);
grid on;
box off;
set(gca(), 'TickLabelInterpreter', 'latex', 'FontSize', 13);
legend(labels, 'EdgeColor', 0.99*[1,1,1], 'Interpreter', 'latex', 'FontSize', 13);
ylabel('Count', 'Interpreter', 'latex', 'FontSize', 13)
xlabel('Objective', 'Interpreter', 'latex', 'FontSize', 13)