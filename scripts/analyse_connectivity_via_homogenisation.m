

mesh = create_mesh_from_img( zeros(4,4), 0.1 );
data = 0.0 * ones(4*4,1);  

L = construct_Laplacian(mesh, data);