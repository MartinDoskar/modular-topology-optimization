function [mesh] = mesh_tile(moduleRes, moduleSize, originOffset)
% MESH_TILE creates a FE discretization of a Wang tile comprising voxel-like elements
% and triangular linear elements along diagonals such that each element is
% attributed to one of tile edges (stored in matId)
%
% Features: * Mesh can be shifted via originOffset of the bottom left corner
%
% Version:  0.1.0 (2021-09-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

if nargin < 2
    moduleSize = 1.0;
end
if nargin <= 2
   originOffset = [0; 0; 0]; 
end

temp = 0.0:moduleSize/moduleRes:moduleSize;
nodes = [ kron(ones(length(temp),1), temp') + originOffset(1), ...
    kron(temp', ones(length(temp),1)) + originOffset(2), ...
    zeros(length(temp)^2, 1) + originOffset(3) ];

elements(1:(moduleRes^2+2*moduleRes)) = struct('type', 0, 'nodeIds', [], 'matId', []);
counter = 0;
for iY = 1:moduleRes
    for iX = 1:moduleRes
        
        A = (iY - 1) * (moduleRes + 1) + iX;
        B = (iY - 1) * (moduleRes + 1) + (iX + 1);
        C = iY * (moduleRes + 1) + iX;
        D = iY * (moduleRes + 1) + (iX + 1);
        
        if (iX == iY) && ((moduleRes-iX+1) == iY)
            nodes = [ nodes; moduleSize/0.5, moduleSize/0.5, 0.0 ];
            E = size(nodes, 1);
            
            elements(counter+1) = struct('type', 5, 'nodeIds', [A, B, E], 'matId', 3);
            elements(counter+2) = struct('type', 5, 'nodeIds', [B, D, E], 'matId', 2);
            elements(counter+2) = struct('type', 5, 'nodeIds', [D, C, E], 'matId', 4);
            elements(counter+2) = struct('type', 5, 'nodeIds', [C, A, E], 'matId', 1);
            counter = counter + 4;
        elseif (iX == iY)
            elements(counter+1) = struct('type', 5, 'nodeIds', [A, B, D], 'matId', (iX <= moduleRes/2) * 3 + (iX > moduleRes/2) * 2 );
            elements(counter+2) = struct('type', 5, 'nodeIds', [A, D, C], 'matId', (iX <= moduleRes/2) * 1 + (iX > moduleRes/2) * 4 );
            counter = counter + 2;
        elseif ((moduleRes-iX+1) == iY)
            elements(counter+1) = struct('type', 5, 'nodeIds', [A, B, C], 'matId', (iX <= moduleRes/2) * 1 + (iX > moduleRes/2) * 3);
            elements(counter+2) = struct('type', 5, 'nodeIds', [B, D, C], 'matId', (iX <= moduleRes/2) * 4 + (iX > moduleRes/2) * 2 );
            counter = counter + 2;            
        else
            elements(counter+1) = struct('type', 8, 'nodeIds', [A, B, C, D], 'matId', ...
                ((iX < iY) && (iY < (moduleRes-iX+1))) * 1 + ...
                ((iX > iY) && (iY > (moduleRes-iX+1))) * 2 + ...
                ((iX > iY) && (iY < (moduleRes-iX+1))) * 3 + ...
                ((iX < iY) && (iY > (moduleRes-iX+1))) * 4 );
            counter = counter + 1;
        end
        
    end
end
elements = elements(1:counter);

mesh.nodes = nodes;
mesh.elements = elements;

mesh.nNodes = size(nodes, 1);
mesh.nElems = length(elements);

mesh.boundary.vertex{1} = 1; 
mesh.boundary.vertex{2} = moduleRes + 1;
mesh.boundary.vertex{3} = moduleRes*(moduleRes + 1) + 1;
mesh.boundary.vertex{4} = (moduleRes + 1)*(moduleRes + 1);

mesh.boundary.edge{1} = ((moduleRes+2):(moduleRes+1):((moduleRes-1)*(moduleRes+1)+1))';
mesh.boundary.edge{2} = ((2*(moduleRes+1)):(moduleRes+1):(moduleRes*(moduleRes+1)))';
mesh.boundary.edge{3} = (2:moduleRes)';
mesh.boundary.edge{4} = ((moduleRes*(moduleRes+1)+2):((moduleRes+1)*(moduleRes+1)-1))';

mesh.boundary.whole = [ mesh.boundary.vertex{1}; mesh.boundary.vertex{2}; mesh.boundary.vertex{3}; mesh.boundary.vertex{4}; ...
                        mesh.boundary.edge{1}; mesh.boundary.edge{2}; mesh.boundary.edge{3}; mesh.boundary.edge{4} ];

end