classdef Tracker
    properties
        done
        total
    end
    
    methods
        function obj = Tracker(nTotal)
            obj.total = nTotal;
            obj.done = 0;
        end
        function obj = add(obj)
           obj.done = obj.done + 1; 
        end
        function ratio = print(obj)
            ratio = obj.done / obj.total;
            fprintf('%s: %.2f %% done\n', datestr(now()), ratio * 100);
        end
    end
end