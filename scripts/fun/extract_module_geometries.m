function [ modules ] = extract_module_geometries( VTKfile, input2, moduleResolution )
% EXTRACT_MODULE_GEOMETRIES reads legacy VTK file for entries
% densityDesign and densityProjected and parses images of individual
% modules in the assembly plan
%
%   [ modules ] = extract_module_geometries( VTKfile, assemblyPlan, moduleResolution )
%   [ modules ] = extract_module_geometries( VTKfile, JSONfile )
%
% Features: * Support loading assemblyPlan and moduleResolution from JSON
%             input to MTO.Application
%
% Version:  0.1.0 (2021-06-15)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

if nargin == 2 && ischar(input2) 
    JSONfile = input2;
    problemDefinition = jsondecode(fileread(JSONfile));
    
    assemblyPlan = reshape( problemDefinition.assemblyPlan , ...
        problemDefinition.assemblyResolution(1), problemDefinition.assemblyResolution(2) );
    moduleResolution = [ problemDefinition.moduleResolution(1), problemDefinition.moduleResolution(2) ];
    
elseif nargin == 3
    assemblyPlan = input2;
else
   error('Function extract_module_geometries requires either two or three parameters');
end

% Load input file
c = textread(VTKfile, '%s', 'delimiter', '\n');

anchor1 = find(contains(c, 'SCALARS densityDesign'));
anchor2 = find(contains(c, 'SCALARS densityProjected'));

assert(length(anchor1) == 1, 'Required field densityDesign not found in a file.');
assert(length(anchor2) == 1, 'Required field densityProjected not found in a file.');

readDensityDesign = cellfun(@str2num, {c{ anchor1+2:anchor2-1 }});
readDensityProjected = cellfun(@str2num, {c{ anchor2+2:(anchor2+anchor2-anchor1-1) }});

% Parse densities and assign them to modules
nElemsPerModule = prod(moduleResolution);

assert( mod(length(readDensityDesign), nElemsPerModule) == 0, 'The length of read density vector does not correspond to provided module resolution');
assert( all(assemblyPlan(:) >= 0), 'Assembly plan should contain only non-negative values (with 0 reserved for an empty spot)');

counter = 0;
[sTx, sTy] = size(assemblyPlan);
modules(1:max(assemblyPlan(:))) = struct('used', false, 'densityDesign', [], 'densityProjected', []);
for iTy = 1:sTy
    for iTx = 1:sTx
        moduleId = assemblyPlan(iTx,iTy);
        if (moduleId > 0)
            if ~modules(moduleId).used
                modules(moduleId).densityDesign = reshape( readDensityDesign(counter + (1:nElemsPerModule)), moduleResolution);
                modules(moduleId).densityProjected = reshape( readDensityProjected(counter + (1:nElemsPerModule)), moduleResolution);
                
                modules(moduleId).used = true;
            end
            counter = counter + nElemsPerModule;
        end
    end
end

end