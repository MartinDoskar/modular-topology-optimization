function iFig = plot_decomposed_solution(solution, problem, inMagnificationFactor, faceVertexData)
% PLOT_DECOMPOSED_SOLUTION plots domain-wise solutions over the whole 
% problem domain. Solution on each domain is given as a cell entry in
% `solution` input, decomposition and meshes of individual modules are
% provided in `problem` structure. For better visualisation, artificial
% magnification can be imposed by specifying inMagnificationFactor (1 by
% default).
%
%   iFig = plot_decomposed_solution(solution, problem, inMagnificationFactor)
%
% Version:  0.1.0 (2020-11-15)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

% Default magnification
if nargin > 2
    magnificationFactor = inMagnificationFactor;
else
    magnificationFactor = 1;
end

useCData = (nargin > 3);

nDomains = length(problem.domains);
domainColours = lines(nDomains);

assert(length(solution) == nDomains, 'Solution must be provided as a cell array containing solution vectors for each domain.');

iFig = figure();
for iD = 1:nDomains
   
    mId = problem.domains(iD).moduleId;  
    
    nodeCoords = problem.modules(mId).mesh.nodes(:,1:2);
    nodeCoords(:,1) = nodeCoords(:,1) + problem.domains(iD).shift(1);
    nodeCoords(:,2) = nodeCoords(:,2) + problem.domains(iD).shift(2);
    
    deformedCoords = nodeCoords + magnificationFactor * (reshape(solution{iD}, 2, [])');
        
    hold on;
    if useCData
        patch('Vertices', deformedCoords, 'Faces', problem.modules(mId).mesh.elements, 'FaceVertexCData', faceVertexData{mId}, ...
            'EdgeColor', 'none', 'FaceColor', 'flat');
    else
        patch('Vertices', deformedCoords, 'Faces', problem.modules(mId).mesh.elements, ...
            'EdgeColor', domainColours(iD,:), 'FaceColor', 'none');
    end
    hold off;    
end

set( gca(), 'TickLabelInterpreter', 'latex', 'FontSize', 12 );
set( gca(), 'TickDir', 'in' );
axis equal;
grid on;

end
