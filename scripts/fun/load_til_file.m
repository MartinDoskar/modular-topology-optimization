function [plan, data] = load_til_file(fileName)
% LOAD_TIL_FILE loads and parses assembly plan data from *.til file
% produced as an output of Marek Tyburec's procedures
%
% Version:  0.1.0 (2021-06-13)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

iF = fopen(fileName, 'rt');

fgetl(iF);
fgetl(iF);
data.filename = fgetl(iF);

fgetl(iF);
fgetl(iF);
auxResolution = sscanf(fgetl(iF), '%i')';

fgetl(iF);
fgetl(iF);

aux = zeros(auxResolution);
for iR = 1:auxResolution(1)
   aux(iR,:) = sscanf(fgetl(iF), '%i');
end
aux = aux + 1;
aux(aux == -1) = 0;

plan = rot90(aux,-1);

data.assemblyResolution = fliplr(auxResolution);
data.assemblyPlan = reshape(plan, [], 1);

fclose(iF);

end