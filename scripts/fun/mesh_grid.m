function [mesh] = mesh_grid(gridRes, gridSize, originOffset)
% MESH_GRID creates a FE discretization comprising voxel-like elements
% arranged in a regular grid with resolution gridRes and total size
% gridSize
%
% Features: * Mesh can be shifted via originOffset of the bottom left corner
%
% Version:  0.1.0 (2021-09-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

if nargin <= 2
   originOffset = [0; 0; 0]; 
end

x = (0.0:gridSize(1)/gridRes(1):gridSize(1)) + originOffset(1);
y = (0.0:gridSize(2)/gridRes(2):gridSize(2)) + originOffset(2);

nodes = [ kron(ones(length(y),1), x'), ...
    kron(y', ones(length(x),1)), ...
    zeros(length(x)*length(y), 1) + originOffset(3) ];

elements(1:prod(gridRes)) = struct('type', 0, 'nodeIds', [], 'matId', []);
counter = 0;
for iY = 1:gridRes(2)
    for iX = 1:gridRes(1)        
        A = (iY - 1) * (gridRes(1) + 1) + iX;
        B = (iY - 1) * (gridRes(1) + 1) + (iX + 1);
        C = iY * (gridRes(1) + 1) + iX;
        D = iY * (gridRes(1) + 1) + (iX + 1);
        
        elements(counter+1) = struct('type', 8, 'nodeIds', [A, B, C, D], 'matId',0 );
        counter = counter + 1;        
    end
end
elements = elements(1:counter);

mesh.nodes = nodes;
mesh.elements = elements;

mesh.nNodes = size(nodes, 1);
mesh.nElems = length(elements);

mesh.boundary.vertex{1} = 1; 
mesh.boundary.vertex{2} = gridRes(1) + 1;
mesh.boundary.vertex{3} = gridRes(2)*(gridRes(1) + 1) + 1;
mesh.boundary.vertex{4} = (gridRes(2) + 1)*(gridRes(1) + 1);

mesh.boundary.edge{1} = ((gridRes(1)+2):(gridRes(1)+1):((gridRes(2)-1)*(gridRes(1)+1)+1))';
mesh.boundary.edge{2} = ((2*(gridRes(1)+1)):(gridRes(1)+1):(gridRes(2)*(gridRes(1)+1)))';
mesh.boundary.edge{3} = (2:gridRes(1))';
mesh.boundary.edge{4} = ((gridRes(2)*(gridRes(1)+1)+2):((gridRes(2)+1)*(gridRes(1)+1)-1))';

mesh.boundary.whole = [ mesh.boundary.vertex{1}; mesh.boundary.vertex{2}; mesh.boundary.vertex{3}; mesh.boundary.vertex{4}; ...
                        mesh.boundary.edge{1}; mesh.boundary.edge{2}; mesh.boundary.edge{3}; mesh.boundary.edge{4} ];


end