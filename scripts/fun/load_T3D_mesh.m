function [ geometry ] = load_T3D_mesh( inFile, extractBoundary )
% LOAD_T3D_MESH processes T3D output file format and return geometry structure
%
% Features: * Script should handle both triangles and tetrahedra
%           * Besides the standard fields nNodes, nElem, nodes, elems and
%             elemMats, the output structure also contains type and id of 
%             parent entity for both nodes and elements
%           * Enables extracting boundary from bounding box when required
%
% Version:  0.5 (2020-06-08)
% Author:   Martin Do�k�� (MartinDoskar@gmail.com)

GEOM_TOL = 1e-12;

dim = 0;    % Define dimension of the problem
deg = 0;    % Define approximation degree 
nDOFs = 0;  % Number of DOFs per element

if nargin == 1
    extractBoundary = false;
else
    assert(isa(extractBoundary, 'logical'), 'Second argument must be a bool value');
end

rawData = dlmread( inFile );

assert( rawData(1,1) == 7, 'Script is developed for ''mixed'' type of output mesh of T3D.' );
assert( all(rawData(2,[4,6,7,8]) == 0), 'Script supports only triangular (for 2D) and tetrahedral (for 3D) elements.');

% Handle additional edge elements
nElemOffset = rawData(2,2);

deg = rawData(1,2);
nNodes = rawData(2,1);
if (rawData(2,3) ~= 0) && (rawData(2,5) == 0)
    nElems = rawData(2,3);
    dim = 2;
    nDOFs = (deg==1)*3 + (deg==2)*6;
elseif (rawData(2,3) == 0) && (rawData(2,5) ~= 0)
    nElems = rawData(2,5);
    dim = 3;
    nDOFs = (deg==1)*4 + (deg==2)*10;
else
    error('Input script either mixes 2D and 3D elements or contains no element at all.');
end

geometry = struct( 'nNodes', nNodes, 'nElems', nElems, 'nodes', zeros(nNodes,dim), 'elements', zeros(nElems,nDOFs), ...
                   'elemMats', zeros(nElems,1), 'nodeParentType', zeros(nNodes,1), 'nodeParentID', zeros(nNodes,1), ...
                   'elemParentType', zeros(nElems,1), 'elemParentID', zeros(nElems,1) );

geometry.nodes = rawData(3:nNodes+2,2:dim+1);
if dim == 2
   geometry.nodes = [ geometry.nodes, zeros(size(geometry.nodes,1),1) ]; 
end
geometry.nodeParentType = rawData(3:nNodes+2,5);
geometry.nodeParentID   = rawData(3:nNodes+2,6);

geometry.elements       = rawData(nNodes+3+nElemOffset:nNodes+2+nElemOffset+nElems,2:nDOFs+1);
geometry.elemParentType = rawData(nNodes+3+nElemOffset:nNodes+2+nElemOffset+nElems,nDOFs+2);
geometry.elemParentID   = rawData(nNodes+3+nElemOffset:nNodes+2+nElemOffset+nElems,nDOFs+3);
geometry.elemMats       = rawData(nNodes+3+nElemOffset:nNodes+2+nElemOffset+nElems,nDOFs+4);

if extractBoundary
   minBound = min(geometry.nodes);
   maxBound = max(geometry.nodes);
   
   leftNodesMap = abs(geometry.nodes(:,1) - minBound(1)) < GEOM_TOL;
   rightNodesMap = abs(geometry.nodes(:,1) - maxBound(1)) < GEOM_TOL;
   bottomNodesMap = abs(geometry.nodes(:,2) - minBound(2)) < GEOM_TOL;
   topNodesMap = abs(geometry.nodes(:,2) - maxBound(2)) < GEOM_TOL;
   
   geometry.boundary.vertex = {[],[],[],[],[],[],[],[]};
   geometry.boundary.edge = {[],[],[],[],[],[],[],[],[],[],[],[]};
   geometry.boundary.face = {[],[],[],[],[],[],[],[]};
   
   geometry.boundary.vertex{1} = intersect( find(leftNodesMap), find(bottomNodesMap) );
   geometry.boundary.vertex{2} = intersect( find(rightNodesMap), find(bottomNodesMap) );
   geometry.boundary.vertex{3} = intersect( find(leftNodesMap), find(topNodesMap) );
   geometry.boundary.vertex{4} = intersect( find(rightNodesMap), find(topNodesMap) );
   
   foo = setdiff(find(bottomNodesMap), union(geometry.boundary.vertex{1},geometry.boundary.vertex{2}));
   [~,ind] = sort( geometry.nodes(foo,1), 'ascend' );  
   geometry.boundary.edge{1} = foo(ind);
   
   foo = setdiff(find(topNodesMap), union(geometry.boundary.vertex{3},geometry.boundary.vertex{4}));
   [~,ind] = sort( geometry.nodes(foo,1), 'ascend' );  
   geometry.boundary.edge{2} = foo(ind);
   
   foo = setdiff(find(leftNodesMap), union(geometry.boundary.vertex{1},geometry.boundary.vertex{3}));
   [~,ind] = sort( geometry.nodes(foo,2), 'ascend' );  
   geometry.boundary.edge{5} = foo(ind);
   
   foo = setdiff(find(rightNodesMap), union(geometry.boundary.vertex{2},geometry.boundary.vertex{4}));
   [~,ind] = sort( geometry.nodes(foo,2), 'ascend' );   
   geometry.boundary.edge{6} = foo(ind);
   
end

end