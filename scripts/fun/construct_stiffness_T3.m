function [ K ] = construct_stiffness_T3( mesh, materials )
% CONSTRUCT_STIFFNESS_T3 returns the stiffness matrix of a provided mesh
% and materials data
%
%   [ K ] = construct_stiffness_T3( mesh, materials )
%
% Version:  0.1.0 (2021-05-02)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

nDOFsElem = 6;

r = zeros(mesh.nElems * nDOFsElem^2, 1);
c = zeros(mesh.nElems * nDOFsElem^2, 1);
v = zeros(mesh.nElems * nDOFsElem^2, 1);
for iE = 1:mesh.nElems
    iNodes = mesh.elements(iE,:);
    iDOFs = [2*iNodes - 1; 2*iNodes];
    iDOFs = iDOFs(:);

   	x1 = mesh.nodes(iNodes(1),1);
    y1 = mesh.nodes(iNodes(1),2);
	x2 = mesh.nodes(iNodes(2),1);
    y2 = mesh.nodes(iNodes(2),2);
	x3 = mesh.nodes(iNodes(3),1);
    y3 = mesh.nodes(iNodes(3),2);
    
	
	V = 1/2 * ( (x1 - x2)*(y1 - y3) - (x1 - x3)*(y1 - y2) );
    Bcoord = 1/(2*V) * [ y2 - y3, y3 - y1, y1 - y2; x3 - x2, x1 - x3, x2 - x1 ];  
    
    B = zeros(3, 6);
    B(1,1:2:6) = Bcoord(1,:);
    B(2,2:2:6) = Bcoord(2,:);
    B(3,1:2:6) = Bcoord(2,:);
    B(3,2:2:6) = Bcoord(1,:);
    
    Ke = (B' * (materials(mesh.elemMats(iE)).D * B)) * V;

    r((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(iDOFs, ones(nDOFsElem,1));
    c((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(ones(nDOFsElem,1), iDOFs);
    v((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = Ke(:); 
end

K = sparse(r, c, v, 2*mesh.nNodes, 2*mesh.nNodes);
K = 0.5 * (K + K');


end