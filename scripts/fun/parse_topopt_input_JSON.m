function [ problemDefinition ] = parse_topopt_input_JSON( inputFile )
% PARSE_TOPOPT_INPUT_JSON reads a json input for C++ topology
% optimalization
%
%   [ problemDefinition ] = parse_topopt_input_JSON( inputFile )
%
% Version:  0.1.0 (2020-12-01)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

problemDefinition = jsondecode(fileread(inputFile));
if any(problemDefinition.assemblyPlan(:) == 0)
    problemDefinition.assemblyPlan = problemDefinition.assemblyPlan + 1;
end
problemDefinition.assemblyPlan = reshape(problemDefinition.assemblyPlan, problemDefinition.assemblyResolution');

end