function [ modules ] = load_modular_geometry_from_VTK( inputFile, problemDefinition )
% LOAD_MODULAR_GEOMETRY_FROM_VTK reads a VTK file and extract modular data
% based on parameters provided in problemParams
%
% Features: * Loaded designs are locked to be in between 0.0 and 1.0
%
% Version:  0.1.1 (2021-04-15)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

% Load input file
c = textread(inputFile, '%s', 'delimiter', '\n');

% anchor1 = find(contains(c, 'SCALARS densityDesign'));
% anchor2 = find(contains(c, 'SCALARS densityChange'));
anchor1 = find(~cellfun(@isempty, strfind(c, 'SCALARS densityDesign')));
anchor2 = find(~cellfun(@isempty, strfind(c, 'SCALARS densityChange')));


assert(length(anchor1) == 1, 'Required field densityDesign not found in a file.');
assert(length(anchor2) == 1, 'Required field densityChange not found in a file.');

readData = reshape(cellfun(@str2num, {c{anchor1+2:anchor2-1}}), ...
    (problemDefinition.assemblyResolution .* problemDefinition.moduleResolution)');

% Parse read data and extract modular info
blocksPerRow = problemDefinition.assemblyResolution(1) * problemDefinition.moduleResolution(1);

modules(1:problemDefinition.nModules) = struct('design', [], 'mesh', []);
for iTy = 1:problemDefinition.assemblyResolution(2)
    for iTx = 1:problemDefinition.assemblyResolution(1)  
        
        iModule = problemDefinition.assemblyPlan(iTx, iTy);
        
        if isempty(modules(iModule).design)
            localDesign = nan(problemDefinition.moduleResolution(1), problemDefinition.moduleResolution(2));
            
            for iMy = 1:problemDefinition.moduleResolution(2)
                localDesign(:,iMy) = readData(...
                    ((iTy-1)*problemDefinition.moduleResolution(2) + (iMy-1)) * blocksPerRow + ...
                    (iTx-1)*problemDefinition.moduleResolution(1) + (1:problemDefinition.moduleResolution(1)) );
            end
            
%             figure(99);
%             imshow(rot90(tempBar));

            modules(iModule).design = max(0.0, min(1.0, localDesign)); 
            
            % Safety test that all designs are above 0.0 and below 1.0
            if any(modules(iModule).design(:)<0.0) || any(modules(iModule).design(:)>1.0)
               error('Design variable must be above 0.0 and below 1'); 
            end
        end        
    end
end

end