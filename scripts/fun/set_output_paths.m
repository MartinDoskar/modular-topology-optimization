function [outputFolderJSON, outputFolderMTO, outputFileName, inputFileTiling, inputFileGuess] = ... 
    set_output_paths(problemName, nColours, filterType, moduleRes)
% SET_OUTPUT_PATHS sets correct output paths for both Windows and Linux
% platform
%
% Note: * These paths are user specific and was written to our convenience;
%         your output folders may (and probably will) differ
%       * Note that nColours = 0 takes inputs for nColours = 1
%
% Version:  0.1.2 (2022-01-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

% parentFolderName = 'examples';
parentFolderName = 'correction';

fileNameStud = sprintf('%s%i', problemName, nColours);

if ispc
    pathPrefix = 'P:\';
else
    pathPrefix = '/media/ctu_projects';
end

tempFolder = fullfile(pathPrefix, 'WP2', 'MTO', parentFolderName, problemName, ...
    sprintf('%icolor', nColours));
outputFolderJSON = fullfile(tempFolder, ...
    sprintf('MTO_%i_%s', moduleRes, convert_filter_type_into_string_label(filterType)));
if ispc
    outputFolderMTO = outputFolderJSON;
else
    outputFolderMTO = fullfile('.', fileNameStud, ...
        sprintf('MTO_%i_%s', moduleRes, convert_filter_type_into_string_label(filterType)));
end

outputFileName = sprintf('input_modular_%s_%s_%i.json', ...
    problemName, ...
    convert_number_of_colours_into_string_label(nColours), ...
    moduleRes);

inputFolder = fullfile(pathPrefix, 'WP2', 'MTO', parentFolderName, problemName, ...
    sprintf('%icolor', max(nColours,1)));

inputFileTiling = fullfile(inputFolder, [sprintf('%s%i', problemName, max(nColours,1)), '.til']);
if nColours > 1
    inputFileGuess = fullfile(inputFolder, [sprintf('%s%i', problemName, max(nColours,1)), 'guess.mat']);
else
    inputFileGuess = [];
end

end