function [ B ] = get_B_mtrx( eta, ksi )
% GET_B_MTRX returns B mtrx in parametrized coordinates
%
%   [ B ] = get_B_mtrx( eta, ksi )
%
% Version:  0.1.0 (2020-12-02)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

B  = [ -0.25 * (1.0 - eta), +0.25 * (1.0 - eta), +0.25 * (1.0 + eta), -0.25 * (1.0 + eta); ...
       -0.25 * (1.0 - ksi), -0.25 * (1.0 + ksi), +0.25 * (1.0 + ksi), +0.25 * (1.0 - ksi) ];
   
end