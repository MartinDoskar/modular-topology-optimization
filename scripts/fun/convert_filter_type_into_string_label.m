function [ label ] = convert_filter_type_into_string_label(filterType)    
% CONVERT_FILTER_TYPE_INTO_STRING_LABEL returns a string label
% corresponding to an integer filterType coding
%
% Version:  0.1.0 (2021-09-08)
% Author:   Martin Doskar (MartinDoskar@gmail.com)


switch filterType
    case 0
        label = 'kernelDensity'; 
    case 1
        label = 'PDEDensity';
    case 2
        label = 'kernelSensitivity';
    otherwise
        error('Unsupported filter type');
end

end