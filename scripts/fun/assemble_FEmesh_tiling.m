function [ geometry ] = assemble_FEmesh_tiling( tiling, tD, checkCoincidence )
% ASSEMBLE_FEMESH_TILING construct a FE mesh for a given tiling from provided data
%
% [ domain ] = assemble_FEmesh_tiling( tiling, tD )
%
% Notes:    * Zero in the tiling map indicates empty position
%           * Node renumbering option governed by renumType (either 'symrcm' or 'symamd')
%           * Adapts the latest vertex and edge numbering
%           * Adds book-keeping of original tile and node indices for each node and element (**RENAMED IN 1.4**)
%           * Enables expensive coincidence checks (if requested)
%
% Version:  1.5 (2021-09-27)
% Authors:  Martin Doskar (MartinDoskar@gmail.com), Marek Tyburec

    function check_surroundings( checkDir, fromList, toList )
    % CHECK_SURROUNDINGS allows to check a surrounding tile specified by
    % checkDir and copy/control structure fields enlisted in fromList and toList
        
        % Check whether this direction is not outside the tiling and the tiling is defined in that direction   
        proceed = true;
        if checkDir(1)==-1 && proceed
            proceed = proceed && (iTx~=1);
        elseif checkDir(1)==1 && proceed
            proceed = proceed && (iTx~=nTx);
        end
        if checkDir(2)==-1 && proceed
            proceed = proceed && (iTy~=1);
        elseif checkDir(2)==1 && proceed
            proceed = proceed && (iTy~=nTy);
        end
        if proceed
            proceed = proceed && tiling(iTx+checkDir(1),iTy+checkDir(2))~=0 ;
        end

        % Loop over provided fields and update current position fields (and control for consistency)
        if proceed
            for locI = 1:length(fromList)
                if ~isempty( tBound(iTx+checkDir(1),iTy+checkDir(2)).(fromList{locI}) )
                    if isempty( tBound(iTx,iTy).(toList{locI}) )
                        tBound(iTx,iTy).(toList{locI}) = tBound(iTx+checkDir(1),iTy+checkDir(2)).(fromList{locI});
                    else
                        if ~all( tBound(iTx,iTy).(toList{locI}) == tBound(iTx+checkDir(1),iTy+checkDir(2)).(fromList{locI}) )
                            fprintf('Error: Final node number are not consistent!\n');
                        end
                    end
                end
            end
        end
        
    end

GEOMEPS = 1e-10;     % Define geometric tolerance for identification of coinciding tiles 
if nargin < 4
    checkCoincidence = false;
end

% Define auxiliary variables
[ nTx, nTy ] = size( tiling );
sTx = max(tD.tiles(1).mesh.nodes(:,1)) - min(tD.tiles(1).mesh.nodes(:,1));
sTy = max(tD.tiles(1).mesh.nodes(:,2)) - min(tD.tiles(1).mesh.nodes(:,2));

% Count aproximate number of nodes and elements
nNodes = 0;
nElems  = 0;
for iTx = 1:nTx
    for iTy = 1:nTy
        iT = tiling( iTx, iTy );
        if iT ~= 0
            nElems   = nElems + tD.tiles(iT).mesh.nElems;
            nNodes  = nNodes + tD.tiles(iT).mesh.nNodes;
        end
    end
end

% Pre-allocate final variables
nodes    = zeros( nNodes, 3);
elements(1:nElems) = struct('type', 0, 'nodeIds', [], 'matId', []);
% elements = zeros( nElems, nNodesPerElement);
% elements = cell(nTx*nTy,1);
elemMat  = zeros( nElems, 1);
tileID   = zeros( nNodes, 1);
nodeID   = zeros( nNodes, 1);
positionID = zeros( nNodes, 4 );    % Maximally four tiles can share a node 
elemPositionID = zeros( nElems, 1);
elemTileID = zeros( nElems, 1);

% Loop over tiling and assembly final discretization
cN = 0;     % Count nodes
cE = 0;     % Count elements
tBound(1:nTx,1:nTy) = struct( 'v1', [], 'v2', [], 'v3', [], 'v4', [], 'e1', [], 'e2', [], 'e3', [], 'e4', [] );

elemCounter = 0;
for iTy = 1:nTy
    for iTx = 1:nTx        
        iT = tiling( iTx, iTy );    % Tile type
        iT1D = (iTy-1)*nTx+iTx;     % One-dimensional index to the tiling matrix 
        
        if iT~=0
            % Check surroundings (edge neighbours first, followed by vertex neighbours) 
            check_surroundings( [-1;0], {'e2','v2','v4'}, {'e1','v1','v3'} );
            check_surroundings( [+1;0], {'e1','v1','v3'}, {'e2','v2','v4'} );
            check_surroundings( [0;-1], {'e4','v3','v4'}, {'e3','v1','v2'} );
            check_surroundings( [0;+1], {'e3','v3','v4'}, {'e4','v1','v2'} );
            check_surroundings( [-1;-1], {'v4'}, {'v1'} );
            check_surroundings( [+1;-1], {'v3'}, {'v2'} );
            check_surroundings( [-1;+1], {'v2'}, {'v3'} );
            check_surroundings( [+1;+1], {'v1'}, {'v4'} );

            % Define new nodes mask and mark already existing nodes with 0
            newNodes = true( tD.tiles(iT).mesh.nNodes, 1 );
            for i = 1:4  
                if ~isempty( tBound(iTx,iTy).(sprintf('v%i',i)) )
                    newNodes( tD.tiles(iT).mesh.boundary.vertex{i} ) = 0; 
                end          
                if ~isempty( tBound(iTx,iTy).(sprintf('e%i',i)) )
                    newNodes( tD.tiles(iT).mesh.boundary.edge{i} ) = 0; 
                end
            end
            nNewNodes  = nnz(newNodes);
            
            % Perform optional check for node coincidence
            if checkCoincidence
                for i = 1:4 
                if ~isempty( tBound(iTx,iTy).(sprintf('v%i',i)) )
                    auxCoords = tD.tiles(iT).mesh.nodes(tD.tiles(iT).mesh.boundary.vertex{i},:);
                    auxCoords(:,1) = auxCoords(:,1) + (iTx-1)*sTx;
                    auxCoords(:,2) = auxCoords(:,2) + (iTy-1)*sTy;
                    
                    diffCoords = nodes( tBound(iTx,iTy).(sprintf('v%i',i)), : ) - auxCoords;
                    assert( all( abs(diag( diffCoords'*diffCoords )) < GEOMEPS ), 'Nodes identified as identical do not coincide geometrically' ); 
                end
                if ~isempty( tBound(iTx,iTy).(sprintf('e%i',i)) )
                    auxCoords = tD.tiles(iT).mesh.nodes(tD.tiles(iT).mesh.boundary.edge{i},:);
                    auxCoords(:,1) = auxCoords(:,1) + (iTx-1)*sTx;
                    auxCoords(:,2) = auxCoords(:,2) + (iTy-1)*sTy;
                    
                    diffCoords = nodes( tBound(iTx,iTy).(sprintf('e%i',i)), : ) - auxCoords;
                    assert( all( abs(diag( diffCoords'*diffCoords )) < GEOMEPS ), 'Nodes identified as identical do not coincide geometrically' ); 
                end                      
                end
            end

            % Define node renumbering vector
            renumNodes = cumsum(newNodes);
            renumNodes = renumNodes + cN;
            for i = 1:4            
                if ~isempty( tBound(iTx,iTy).(sprintf('v%i',i)) )
                    renumNodes( tD.tiles(iT).mesh.boundary.vertex{i} ) = tBound(iTx,iTy).(sprintf('v%i',i));
                end
                if ~isempty( tBound(iTx,iTy).(sprintf('e%i',i)) )
                    renumNodes( tD.tiles(iT).mesh.boundary.edge{i} ) = tBound(iTx,iTy).(sprintf('e%i',i));
                end
            end

            % Add new nodes (translated), mark their origin, and renumbered elements 
            nodes( cN+1:cN+nNewNodes, 1 ) = tD.tiles(iT).mesh.nodes(newNodes,1) + (iTx-1)*sTx;
            nodes( cN+1:cN+nNewNodes, 2 ) = tD.tiles(iT).mesh.nodes(newNodes,2) + (iTy-1)*sTy;
            
            tileID( cN+1:cN+nNewNodes ) = iT;
            nodeID( cN+1:cN+nNewNodes ) = find(newNodes);
            
            
            aux = tD.tiles(iT).mesh.elements;
            
            % this is the slowest part right now
            for iiE = 1:length(aux)
               aux(iiE).nodeIds = renumNodes( aux(iiE).nodeIds );
            end
            elements(elemCounter + (1:length(aux))) = aux;
            elemCounter = elemCounter + length(aux);
%             elements{iTy + (iTx-1)*nTy} = aux;
            
            elemPositionID( cE+1:cE+tD.tiles(iT).mesh.nElems ) = iT1D;
            elemTileID( cE+1:cE+tD.tiles(iT).mesh.nElems ) = iT;

            % Update tBound structure for current tile
            for i = 1:4
                tBound(iTx,iTy).(sprintf('v%i',i)) = renumNodes( tD.tiles(iT).mesh.boundary.vertex{i} );
                tBound(iTx,iTy).(sprintf('e%i',i)) = renumNodes( tD.tiles(iT).mesh.boundary.edge{i} );
            end
            
            % Update positionID for current tile
            for iU = renumNodes'
                positionID( iU, nnz(positionID(iU,:))+1 ) = iT1D;
            end

            % Update counters
            cN = cN + nNewNodes;
            cE = cE + tD.tiles(iT).mesh.nElems;
        
        end       
    end
end

% Trim unused space
nodes      = nodes(1:cN,:);
elemMat    = elemMat(1:cE);
tileID     = tileID(1:cN);
nodeID     = nodeID(1:cN);
positionID = positionID(1:cN,:);
elemPositionID = elemPositionID(1:cE);
elemTileID = elemTileID(1:cE);

% Store data into output structure
geometry.nNodes         = cN;
geometry.nElems          = cE;
geometry.nodes          = nodes;
geometry.nodeTileID     = tileID;
geometry.nodeNodeID     = nodeID;
geometry.nodePositionID = positionID;
geometry.elements       = elements;
geometry.elemMat        = elemMat;
geometry.elemPositionID = elemPositionID;
geometry.elemTileID     = elemTileID;
    
end