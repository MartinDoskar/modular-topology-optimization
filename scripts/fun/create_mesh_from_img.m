function [ mesh ] = create_mesh_from_img( img, voxelSize )
% GET_DEFAULT_MATERIAL_STIFFNESS returns a 2D stiffness matrix of a
% linearly isotropic material
%
%   [ mesh ] = create_mesh_from_img( img, voxelSize )
%
% Version:  0.1.0 (2020-12-02)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

[nY, nX] = size(img);

mesh.nodes = [ kron( ones(1,nY+1), (0:1:nX) )', ...
               kron( (0:1:nY), ones(1,nX+1) )', ...
               zeros( (nX+1)*(nY+1), 1) ];
mesh.elements = zeros( nX*nY, 4 );
mesh.elemMat = (1:nX*nY)';

mesh.nodes(:,1) = mesh.nodes(:,1) * voxelSize(1);
mesh.nodes(:,2) = mesh.nodes(:,2) * voxelSize(2);   

mesh.nodes(:,1) = mesh.nodes(:,1) - mean(mesh.nodes(:,1));
mesh.nodes(:,2) = mesh.nodes(:,2) - mean(mesh.nodes(:,2));
mesh.nodes(:,3) = mesh.nodes(:,3) - mean(mesh.nodes(:,3));

for iY = 1:nY
    for iX = 1:nX
        voxelVertices = [ (iY-1)*(nX+1) + iX, (iY-1)*(nX+1) + iX + 1, iY*(nX+1) + iX + 1, iY*(nX+1) + iX  ];
        mesh.elements(((iY-1)*nX + iX), : ) = voxelVertices;
    end
end

mesh.nNodes = size(mesh.nodes, 1);
mesh.nElems = size(mesh.elements, 1);

mesh.boundary.vertex = {[1], [nX+1], [(nX)*(nY+1)+1], [(nX+1)*(nY+1)]};
mesh.boundary.edge = { ((nX+2):(nX+1):((nX-1)*(nY+1)+1))', ...
    ((2*nX+2):(nX+1):((nX)*(nY+1)))', ...
    (2:nX)', ...
    (((nX)*(nY+1)+2):((nX+1)*(nY+1)-1))' };
mesh.boundary.whole = [ mesh.boundary.vertex{1}; mesh.boundary.vertex{2}; mesh.boundary.vertex{3}; mesh.boundary.vertex{4}; ...
                        mesh.boundary.edge{1}; mesh.boundary.edge{2}; mesh.boundary.edge{3}; mesh.boundary.edge{4} ];

end