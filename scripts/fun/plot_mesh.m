function [iFig] = plot_mesh(mesh)
% PLOT_MESH visualises the input FE mesh in struct format
%
% Features: * So far only triangles and pixels are supported
%
% Version:  0.1.0 (2021-09-26)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

iFig = figure();

patch( 'Vertices', mesh.nodes, ...
    'Faces', [mesh.elements([mesh.elements.type] == 5).nodeIds]', ...
    'FaceVertexCData', [mesh.elements([mesh.elements.type] == 5).matId]', ...
    'FaceColor', 'flat', 'EdgeColor', 'black');

temp = [mesh.elements([mesh.elements.type] == 8).nodeIds]';
patch( 'Vertices', mesh.nodes, ...
    'Faces', temp(:,[1,2,4,3]), ...
    'FaceVertexCData', [mesh.elements([mesh.elements.type] == 8).matId]', ...
    'FaceColor', 'flat', 'EdgeColor', 'black');

axis equal;

end