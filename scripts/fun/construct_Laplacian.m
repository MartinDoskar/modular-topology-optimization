    function [ L ] = construct_Laplacian( mesh, design )
% CONSTRUCT_LAPLACIAN returns Laplacian matrix of a provided mesh and
% element-wise design densities arising in topology optimization
%
%   [ L ] = construct_Laplacian( mesh, design )
%
% Version:  0.1.0 (2020-12-02)
% Author:   Martin Doskar (MartinDoskar@gmail.com)


nDOFsElem = 4;

oneOverSqrtThree = 0.577350269189626;
quadrature(1) = struct('coords', [-oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(2) = struct('coords', [+oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(3) = struct('coords', [-oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );
quadrature(4) = struct('coords', [+oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );

X = mesh.nodes(mesh.elements(1,:), 1:2);
L0 = zeros(nDOFsElem);
for iG = 1:length(quadrature)        
    Bparam = get_B_mtrx(quadrature(iG).coords(1), quadrature(iG).coords(2));
    J = Bparam * X;
    Bcoord = J \ Bparam;

    L0 = L0 + (Bcoord'*Bcoord) * quadrature(iG).weight * det(J);
end

r = zeros(mesh.nElems * nDOFsElem^2, 1);
c = zeros(mesh.nElems * nDOFsElem^2, 1);
v = zeros(mesh.nElems * nDOFsElem^2, 1);
for iE = 1:mesh.nElems
    iNodes = mesh.elements(iE,:);
    iDOFs = iNodes(:);

    Le = design(iE) * L0;

    r((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(iDOFs, ones(nDOFsElem,1));
    c((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(ones(nDOFsElem,1), iDOFs);
    v((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = Le(:); 
end
L = sparse(r, c, v, mesh.nNodes, mesh.nNodes);
L = 0.5 * (L + L');

end