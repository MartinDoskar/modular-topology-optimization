function [uniquePlan] = uniquely_remap_assembly_plan(assemblyPlan)
% UNIQUELY_REMAP_ASSEMBLY_PLAN renumbers given matrix in a unique manner
% (using positions of the first code occurence as the ordering index)
%
% Version:  0.1.0 (2020-12-14)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

uniquePlan = NaN(size(assemblyPlan));

counter = 0;
for i = 1:numel(assemblyPlan)
   if isnan(uniquePlan(i))
       counter = counter + 1;
       uniquePlan(assemblyPlan == assemblyPlan(i)) = counter;
   end
end

end