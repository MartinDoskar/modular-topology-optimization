function [label] = convert_number_of_colours_into_string_label(nColours)
% CONVERT_NUMBER_OF_COLOURS_INTO_STRING_LABEL returns a string label
% corresponding to a provided number of colours, converting 0 to
% fullyResolved and 1 to PUC
%
% Version:  0.1.0 (2021-09-01)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

assert(nColours >= 0, 'Provided number of colours must be non-negative.');

switch(nColours)
    case 0
        label = 'fullyResolved';
        return;
    case 1
        label = 'PUC';
        return;
    otherwise
        label = sprintf('%icolours', nColours);
        return;
end

end