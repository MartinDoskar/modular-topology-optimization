function [ K ] = construct_stiffness( mesh, design, parameters )
% CONSTRUCT_STIFFNESS returns stiffness matrix of a provided mesh and
% element-wise design densities arising in topology optimization
%
%   [ K ] = construct_stiffness( mesh, design, parameters )
%
% Version:  0.1.0 (2020-12-02)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

nDOFsElem = 8;

oneOverSqrtThree = 0.577350269189626;
quadrature(1) = struct('coords', [-oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(2) = struct('coords', [+oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(3) = struct('coords', [-oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );
quadrature(4) = struct('coords', [+oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );

D0 = get_default_material_stiffness(parameters.E0, parameters.nu);
X = mesh.nodes(mesh.elements(1,:), 1:2);
K0 = zeros(nDOFsElem);
for iG = 1:length(quadrature)        
    Bparam = get_B_mtrx(quadrature(iG).coords(1), quadrature(iG).coords(2));
    J = Bparam * X;
    Bcoord = J \ Bparam;

    B = [ Bcoord(1,1), 0.0, Bcoord(1,2), 0.0, Bcoord(1,3), 0.0, Bcoord(1,4), 0.0; ...
         0.0, Bcoord(2,1), 0.0, Bcoord(2,2), 0.0, Bcoord(2,3), 0.0, Bcoord(2,4); ...
         Bcoord(2,1), Bcoord(1,1), Bcoord(2,2), Bcoord(1,2), Bcoord(2,3), Bcoord(1,3), Bcoord(2,4), Bcoord(1,4) ];

    K0 = K0 + B'*(D0*B) * quadrature(iG).weight * det(J);
end

r = zeros(mesh.nElems * nDOFsElem^2, 1);
c = zeros(mesh.nElems * nDOFsElem^2, 1);
v = zeros(mesh.nElems * nDOFsElem^2, 1);
for iE = 1:mesh.nElems
    iNodes = mesh.elements(iE,:);
    iDOFs = [2*iNodes - 1; 2*iNodes];
    iDOFs = iDOFs(:);

    coeff = (parameters.Emin + design(iE)^(parameters.penalisation) * (parameters.E0 - parameters.Emin));
    Ke = coeff * K0;

    r((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(iDOFs, ones(nDOFsElem,1));
    c((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(ones(nDOFsElem,1), iDOFs);
    v((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = Ke(:); 
end
K = sparse(r, c, v, 2*mesh.nNodes, 2*mesh.nNodes);
K = 0.5 * (K + K');

end