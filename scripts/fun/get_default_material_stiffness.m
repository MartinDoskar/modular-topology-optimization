function [ D ] = get_default_material_stiffness( E, nu )
% GET_DEFAULT_MATERIAL_STIFFNESS returns a 2D stiffness matrix of a
% linearly isotropic material
%
%   [ D ] = get_default_material_stiffness( E, nu )
%
% Version:  0.1.0 (2020-12-02)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

numerator = (1.0 - nu * nu);
G = E / (2.0 * (1.0 + nu));

D = [ E / numerator, nu * E / numerator, 0.0; ...
      nu * E / numerator, E / numerator, 0.0; ...
      0.0, 0.0, G ];

end