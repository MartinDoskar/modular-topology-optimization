function [ K, F, Pn2e ] = construct_filtering_mtrx( mesh, design, parameters )
% CONSTRUCT_FILTERING_MTRX returns system matrix and RHS vector for modified
% Helmholtz-like filtering of design densities
%
%   [ K, F ] = construct_filtering_mtrx( mesh, design, parameters )
%
% Version:  0.2.1 (2021-02-16)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

nDOFsElem = 4;

oneOverSqrtThree = 0.577350269189626;
quadrature(1) = struct('coords', [-oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(2) = struct('coords', [+oneOverSqrtThree, -oneOverSqrtThree], 'weight', 1.0 );
quadrature(3) = struct('coords', [-oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );
quadrature(4) = struct('coords', [+oneOverSqrtThree, +oneOverSqrtThree], 'weight', 1.0 );

D0 = parameters.r^2 * eye(2);

X = mesh.nodes(mesh.elements(1,:), 1:2);
K0 = zeros(nDOFsElem);
Ve = 0;
for iG = 1:length(quadrature)        
    Bparam = get_B_mtrx(quadrature(iG).coords(1), quadrature(iG).coords(2));
    J = Bparam * X;
    Bcoord = J \ Bparam;

    N = get_N_mtrx(quadrature(iG).coords(1), quadrature(iG).coords(2));
    B = Bcoord;

    Ve = Ve + quadrature(iG).weight * det(J);
    K0 = K0 + (N'*N + B'*(D0*B)) * quadrature(iG).weight * det(J);
end

F = zeros(mesh.nNodes, 1);
rP = zeros(mesh.nElems * nDOFsElem, 1);
cP = zeros(mesh.nElems * nDOFsElem, 1);
vP = zeros(mesh.nElems * nDOFsElem, 1);
r = zeros(mesh.nElems * nDOFsElem^2, 1);
c = zeros(mesh.nElems * nDOFsElem^2, 1);
v = zeros(mesh.nElems * nDOFsElem^2, 1);
for iE = 1:mesh.nElems
    iNodes = mesh.elements(iE,:);
    iDOFs = iNodes(:);

    r((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(iDOFs, ones(nDOFsElem,1));
    c((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = kron(ones(nDOFsElem,1), iDOFs);
    v((iE-1)*nDOFsElem^2 + (1:nDOFsElem^2)) = K0(:); 
    
    rP((iE-1)*nDOFsElem + (1:nDOFsElem)) = iE;
    cP((iE-1)*nDOFsElem + (1:nDOFsElem)) = iDOFs(:);
    vP((iE-1)*nDOFsElem + (1:nDOFsElem)) = 1/nDOFsElem;
    
    F(iNodes) = F(iNodes) + 0.25*Ve*[1;1;1;1]*design(iE);
end
K = sparse(r, c, v, mesh.nNodes, mesh.nNodes);
K = 0.5 * (K + K');

Pn2e = sparse(rP, cP, vP, mesh.nElems, mesh.nNodes);

end