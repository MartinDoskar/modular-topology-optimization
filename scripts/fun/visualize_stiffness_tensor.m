function visualize_stiffness_tensor(D, options)
% VISUALIZE_STIFFNESS_TENSOR plots 3D visual representation of a 2D
% stiffness tensor (provided in a matrix form) with colouring options.
%
%   visualize_stiffness_tensor(D, options)
%
% Version:  0.1.0 (2021-02-16)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

defaultOptions = struct( ...
    'figHandle', [], ...
    'nFacets', 50, ...
    'edgeColor', 'none', ...
    'faceColor', 'blue', ...
    'faceAlpha', 0.1 ...  
);
if nargin < 2
    options = struct();
end
for f = fieldnames(defaultOptions)'
  if ~isfield(options, f{1})
      options.(f{1}) = defaultOptions.(f{1});
  end
end

[X0, Y0, Z0] = sphere(options.nFacets);

% [evecs, evals] = eig(D);
% temp = evals * evecs * [ X0(:)'; Y0(:)'; Z0(:)' ];

temp = D * [ X0(:)'; Y0(:)'; Z0(:)' ];

X = reshape(temp(1,:), size(X0));
Y = reshape(temp(2,:), size(Y0));
Z = reshape(temp(3,:), size(Z0));

if ~isempty(options.figHandle)
    figure(options.figHandle);
end
hold on;
surf(X,Y,Z, 'EdgeColor', options.edgeColor, 'FaceColor', options.faceColor, 'FaceAlpha', options.faceAlpha);
hold off;

end