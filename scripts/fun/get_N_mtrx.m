function [ N ] = get_N_mtrx( eta, ksi )
% GET_N_MTRX returns N mtrx in parametrized coordinates
%
%   [ N ] = get_N_mtrx( eta, ksi )
%
% Version:  0.1.0 (2021-02-15)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

N  = [ 0.25 * (1.0 - ksi) * (1.0 - eta), ...
       0.25 * (1.0 + ksi) * (1.0 - eta), ...
       0.25 * (1.0 + ksi) * (1.0 + eta), ...
       0.25 * (1.0 - ksi) * (1.0 + eta) ];
    
end