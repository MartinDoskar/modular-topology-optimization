addpath('./fun');

vtkFile = 'step_301.vtk';
jsonFile = 'input_modular_MBB_4colours_100.json';
outputFolder = 'P:\WP2\MTO\final\mbb4bb\';


%%
modules = extract_module_geometries( fullfile(outputFolder, vtkFile), fullfile(outputFolder, jsonFile) );

for iM = 1:length(modules)
    if modules(iM).used
        imwrite( ...
            rot90(1 - modules(iM).densityDesign), ...
            fullfile(outputFolder, sprintf('module_design_%i.png', iM-1)) );
        
        imwrite( ...
            rot90(1 - modules(iM).densityProjected), ...
            fullfile(outputFolder, sprintf('module_projected_%i.png', iM-1)) );
    end
end