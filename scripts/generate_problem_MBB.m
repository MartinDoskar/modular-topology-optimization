% GENERATE_PROBLEM_MBB creates an input JSON file for MTO.Application for
% the standard MBB problem
%
% Version:  0.2.0 (2021-08-06)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

res = 100;
L = 1.0;

outputFolder = '';
outputFile = 'input_MBB.json';


%% Impose BC and objectives
nX = 3*res;
nY = res;
sX = 3*L;
sY = L;
charElemSize = L/res;

prescribedVals{1} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, 0.0 + charElemSize/10], ...
    "component", 0, "value", 0.0 );
prescribedVals{2} = struct( ...
    "lowerBound", [0.0 - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [0.0 + charElemSize/10, 0.0 + charElemSize/10], ...
    "component", 1, "value", 0.0 );
prescribedVals{3} = struct( ...
    "lowerBound", [sX - charElemSize/10, 0.0 - charElemSize/10], ...
    "upperBound", [sX + charElemSize/10, 0.0 + charElemSize/10], ...
    "component", 1, "value", 0.0 );

prescribedLoads{1} = struct( ...
    "lowerBound", [sX/2.0 - charElemSize/10, sY - charElemSize/10], ...
    "upperBound", [sX/2.0 + charElemSize/10, sY + charElemSize/10], ...
    "component", 1, "value", -1.0 );


%% Create JSON
output.discretisationType = 0;
output.domainResolution = [nX, nY];
output.domainSize = [sX, sY];

output.limIter = 50;
output.limDesignChange = 1e-6;
output.limObjectiveChange = 1e-6;

output.penalisation = 3.0;

output.relativeVolumeConstraint = 0.3;
output.filterType = 0;
output.filterRadius = 1.5*charElemSize;

output.optimizerType = "OC";
output.versionOC = 0;

output.problems{1} = struct( ...
    'prescribedValues', {prescribedVals}, ...
    'prescribedLoad', {prescribedLoads} ...
);  


%% Save JSON file
if ~isempty(outputFolder)
    if ~exist(outputFolder, 'dir')
        mkdir(outputFolder); 
    end
end

iF = fopen(fullfile(outputFolder, outputFile), 'wt');
fprintf(iF, '%s\n', jsonencode(output));
fclose(iF);