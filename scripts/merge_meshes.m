function [outMesh] = merge_meshes(inMeshA, inMeshB, relGeomTol)
% MERGE_MESHES identifies coinciding nodes (givene by relGeomTol) and merges
% the input meshes together
%
%   [outMesh] = merge_meshes(inMeshA, inMeshB, relGeomTol)
%
% Features: * Geometrical tolerance for the identification of coinciding nodes
%             is controled with relGeomTol (default to 1e-06)
%           * Some features/data entries (such as nodeTileID etc.) arising in assemble_FEmesh_tiling
%             are clonned from inMeshA (never from inMeshB)
%
% Version:  0.2.0 (2021-09-27)
% Authors:  Martin Doskar (MartinDoskar@gmail.com), Marek Tyburec

useStructElementDefinition = ~isfield(inMeshA, 'elemMats') && isstruct(inMeshA.elements);

if nargin < 3
    relGeomTol = 1e-5;
end
geomTol = relGeomTol * max( ...
    max(max(inMeshA.nodes) - min(inMeshA.nodes)), ...
    max(max(inMeshB.nodes) - min(inMeshB.nodes)) );

% Identify coinciding nodes
[~,coincidingNodes] = ismembertol(inMeshB.nodes ,inMeshA.nodes, geomTol, 'ByRows', true);
if numel(coincidingNodes(coincidingNodes~=0))~=numel(unique(coincidingNodes(coincidingNodes~=0)))
    error('Non-unique match.');
end

% Create renumber map for nodal values
addNodes = (coincidingNodes == 0);
renumMap = cumsum(addNodes).*addNodes + inMeshA.nNodes;
renumMap(~addNodes) = coincidingNodes(~addNodes);

% Merge meshes
outMesh.nodes = [ inMeshA.nodes; inMeshB.nodes(addNodes,:) ];
outMesh.nNodes = size(outMesh.nodes, 1);
if useStructElementDefinition
    
    aux = inMeshB.elements;
    for iE = 1:length(aux)
        aux(iE).nodeIds = renumMap(aux(iE).nodeIds);
    end

    outMesh.elements = [ inMeshA.elements, aux ];
    outMesh.nElems = length(outMesh.elements);
else
    outMesh.elements = [ inMeshA.elements; renumMap(inMeshB.elements) ];
    outMesh.elemMats = [ inMeshA.elemMats; inMeshB.elemMats ];
    outMesh.nElems = size(outMesh.elements, 1);
end

if isfield(inMeshA, 'nodeTileID')
    outMesh.nodeTileID = inMeshA.nodeTileID;
end
if isfield(inMeshA, 'nodeNodeID')
    outMesh.nodeNodeID = inMeshA.nodeNodeID;
end
if isfield(inMeshA, 'nodePositionID')
    outMesh.nodePositionID = inMeshA.nodePositionID;
end
if isfield(inMeshA, 'elemPositionID')
    outMesh.elemPositionID = inMeshA.elemPositionID;
end
if isfield(inMeshA, 'elemTileID')
    outMesh.elemTileID = inMeshA.elemTileID;
end

end