
syms sx sy t
assume(sx, 'real')
assume(sy, 'real')
assume(t, 'real')

eigvals = eig([sx, t; t, sy]);
 
s1 = eigvals(1);
s2 = eigvals(2);
 

vMsqr = s1^2 + s2^2 - s1*s2;
simplify(vMsqr)
 

% Final formula for Paraview: sqrt(NodalStresses_0^2 - NodalStresses_0 * NodalStresses_3 + NodalStresses_3^2 + 3 * NodalStresses_1^2)