% GENERATE_PROBLEM_INVERTER creates an input JSON file for MTO.Application 
% that reflects the test inverter mechanism problem presented in
% Wang et al.: On projection methods, convergence and robust formulations in topology optimization. SAMO 2011
%
% Version:  0.2.0 (2021-08-06)
% Author:   Martin Doskar (MartinDoskar@gmail.com)

clearvars;
addpath('./fun');

res = 1 * 100;  % Always keep multiples of 100
L = 1.0;

outputFolder = '';
outputFile = 'input_inverter.json';

%%
charElemSize = L/res;

domainSize = [1.0 0.5] .* L;
domainResolution = [res, res/2];

% Impose BC and objectives
prescribedVals{1} = [1, 0.0];
for i = 0:round(domainResolution(2) / 100)
   prescribedVals{end+1} = [...
       i*(domainResolution(1)+1)*2, ...
       0.0];  %#ok<SAGROW>
end
for i = 0:domainResolution(1)
   prescribedVals{end+1} = [...
       (domainResolution(2)*(domainResolution(1)+1)+i)*2+1, ...
       0.0];  %#ok<SAGROW>
end
prescribedLoads{1} = [ (domainResolution(2)*(domainResolution(1)+1))*2, 1.0 ];
prescribedObjective{1} = [ ((domainResolution(2)+1)*(domainResolution(1)+1)-1)*2, 1.0 ];
additionalStiffness{1} = [(domainResolution(2)*(domainResolution(1)+1))*2, 1];
additionalStiffness{2} = [(domainResolution(2)*(domainResolution(1)+1)+domainResolution(1))*2, 0.001];

%% Create JSON
output.discretisationType = 0;
output.domainResolution = domainResolution;
output.domainSize = domainSize;

output.limIter = 50;
output.limDesignChange = 1e-6;
output.limObjectiveChange = 1e-6;

output.penalisation = 3.0;

output.relativeVolumeConstraint = 0.3;
output.filterType = 0;
output.filterRadius = 3*charElemSize;

output.optimizerType = "OC";
output.versionOC = 1;

output.problems{1} = struct( ...
    'prescribedValues', {prescribedVals}, ...
    'prescribedLoad', {prescribedLoads}, ...
    'objectiveVector', {prescribedObjective}, ...
    'additionalStiffness', {additionalStiffness} ...
);  


%% Save JSON file
if ~isempty(outputFolder)
    if ~exist(outputFolder, 'dir')
        mkdir(outputFolder); 
    end
end

iF = fopen(fullfile(outputFolder, outputFile), 'wt');
fprintf(iF, '%s\n', jsonencode(output));
fclose(iF);