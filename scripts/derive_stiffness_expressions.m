clearvars;

syms x y hx hy E nu;

assume(x, 'real');
assume(y, 'real');
assume(hx, 'real');
assume(hy, 'real');
assume(E, 'real');
assume(nu, 'real');

assume(hx > 0);
assume(hy > 0);

numerator = (1.0 - nu * nu);
G = E / (2.0 * (1.0 + nu));

D = [E / numerator, nu * E / numerator, 0.0; ...
     nu * E / numerator, E / numerator, 0.0; ...
     0.0, 0.0, G ];

N = [...
    1/(hx*hy) * (hx-x) * (hy-y), ...
	1/(hx*hy) * (x) * (hy-y), ...
	1/(hx*hy) * (hx-x) * (y), ...
	1/(hx*hy) * (x) * (y) ];

B = [ diff(N, x); diff(N,y) ]

Bmech = [ diff(N(1),x), 0, diff(N(2),x), 0, diff(N(3),x), 0, diff(N(4),x), 0; ...
    0, diff(N(1),y), 0, diff(N(2),y), 0, diff(N(3),y), 0, diff(N(4),y); ...
    diff(N(1),y), diff(N(1),x), diff(N(2),y), diff(N(2),x) diff(N(3),y), diff(N(3),x), diff(N(4),y), diff(N(4),x) ];


int(int( N, 'x', 0, hx), 'y', 0, hy)

K = int(int( Bmech'*(D*Bmech), 'x', 0, hx), 'y', 0, hy);

NN = int(int( N'*N, 'x', 0, hx), 'y', 0, hy)

BB = int(int( (B*(hx*hy))'*(B*(hx*hy)), 'x', 0, hx), 'y', 0, hy)