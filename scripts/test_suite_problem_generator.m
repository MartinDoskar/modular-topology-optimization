% TEST_SUITE_PROBLEM_GENERATOR creates problem definition (with the related
% partitioning) that correspond to the test problems from Gosselet et al. (2015) Simultaneous FETI and
% block FETI: Robust domain decomposition with multiple search directions.
%
% Features: * Loading is kept fixed for all problems, i.e. clamped at the
%             left edge and subject to prescribed traction at the right one
%           * Traction loading is hard-coded, i.e. linear interpolation is
%             assumed along the boundary
%
% Version:  0.1.0 (2021-05-10)
% Author:   Martin Doskar (MartinDoskar@gmail.com)


clearvars;
addpath('./fun');

problemType = 'gridSquare';        % Pick one of 'beam', 'gridLayered', 'gridSquare';
stiffnessRatio = 1e-6;             % Value of Young modulus of the soft material (stiff one is always set to 1)

prescribedTraction = [0.5, 1];     % Traction prescribed at the right-hand edge

inputFolder = './data/';
outputFolder = './';
outputFileName = 'problem03_gridSquare_1e06';



%% 

% Set correct problem-related options
if strcmpi(problemType, 'beam')
    inputFileName = 'PUC_7layers';
    domainSize = [9, 1];    
    info = 'Test problem no. 1 (Fig. 1) from Gosselet et al., 2015, Simultaneous FETI and Block Feti ';    
elseif strcmpi(problemType, 'gridLayered')
    inputFileName = 'PUC_6layers';
    domainSize = [3, 3];    
    info = 'Test problem no. 2 (Fig. 4) from Gosselet et al., 2015, Simultaneous FETI and Block Feti ';    
elseif strcmpi(problemType, 'gridSquare')
    inputFileName = 'PUC_square';
    domainSize = [4, 4];   
    info = 'Test problem no. 3 (Fig. 5) from Gosselet et al., 2015, Simultaneous FETI and Block Feti ';     
else
    error('Unsupported type variant');
    
end

% Set material parameters
materials(1).D = plane_strain_isotropic_stiffness(stiffnessRatio, 0.3);
materials(2).D = plane_strain_isotropic_stiffness(1, 0.3);
    
% Load doamin FE discretization
mesh = load_T3D_mesh(fullfile(inputFolder, [inputFileName, '.out']), true);

% Set data for a module
modules = parse_module_data(mesh, materials);

% Assemble domain from modules
domains = assemble_modules_into_domain(modules, domainSize);

% Extract loading
loading = extract_loading(domains, modules, domainSize, prescribedTraction);

% Store data
problem = struct( ...
    'loading', loading, ...
    'domains', domains, ...
    'modules', modules, ...
    'info', info);

save(fullfile(outputFolder, [outputFileName, '.mat']), 'problem');



%% Auxiliary functions

function [D] = plane_strain_isotropic_stiffness(E, nu)
    
    numerator = (1.0 - nu * nu);
    G = E / (2.0 * (1.0 + nu));
    
    D = [ E / numerator, nu * E / numerator, 0.0; ...
          nu * E / numerator, E / numerator, 0.0; ...
          0.0, 0.0, G ];

end

function [modules] = parse_module_data(mesh, materials)

    K = construct_stiffness_T3(mesh, materials);


    tempBoundary.vertex = { mesh.boundary.vertex{1}, mesh.boundary.vertex{2}, mesh.boundary.vertex{3}, mesh.boundary.vertex{4} };
    tempBoundary.edge = { mesh.boundary.edge{5}, mesh.boundary.edge{6}, mesh.boundary.edge{1}, mesh.boundary.edge{2} };
    tempBoundary.whole = [ tempBoundary.vertex{1}; tempBoundary.vertex{2}; tempBoundary.vertex{3}; tempBoundary.vertex{4}; ...
                        tempBoundary.edge{1}; tempBoundary.edge{2}; tempBoundary.edge{3}; tempBoundary.edge{4} ];

    modules(1).mesh.nodes = mesh.nodes;
    modules(1).mesh.elements = mesh.elements;
    modules(1).mesh.elemMat = mesh.elemMats;
    modules(1).mesh.nNodes = size(modules(1).mesh.nodes, 1);
    modules(1).mesh.nElems = size(modules(1).mesh.elements, 1); 
    modules(1).mesh.boundary = tempBoundary;                

    modules(1).K = K;
    modules(1).Fint = zeros(size(K,1), 1);

end

function [domains] = assemble_modules_into_domain(modules, domainSize)

    meshSize = max(modules(1).mesh.nodes(:,1:2),[],1) - min(modules(1).mesh.nodes(:,1:2),[],1);

    counter = 0;
    temp(domainSize(1),domainSize(2)) = struct( 'vertex', [], 'edge', []);

    for iY = 1:domainSize(2)
        for iX = 1:domainSize(1)

                if iY == 1
                   if iX == 1
                        local.vertex{1} = counter + 1;
                        local.vertex{2} = counter + 2;
                        local.vertex{3} = counter + 3;
                        local.vertex{4} = counter + 4;
                        counter = counter + 4;

                        local.edge{1} = counter + (1:length(modules(1).mesh.boundary.edge{1}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{1});
                        local.edge{2} = counter + (1:length(modules(1).mesh.boundary.edge{2}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{2});
                        local.edge{3} = counter + (1:length(modules(1).mesh.boundary.edge{3}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{3});
                        local.edge{4} = counter + (1:length(modules(1).mesh.boundary.edge{4}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{4});
                   else
                        local.vertex{1} = temp(iX-1,iY).vertex{2};
                        local.vertex{2} = counter + 1;
                        local.vertex{3} = temp(iX-1,iY).vertex{4};
                        local.vertex{4} = counter + 2;
                        counter = counter + 2;

                        local.edge{1} = temp(iX-1,iY).edge{2};
                        local.edge{2} = counter + (1:length(modules(1).mesh.boundary.edge{2}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{2});
                        local.edge{3} = counter + (1:length(modules(1).mesh.boundary.edge{3}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{3});
                        local.edge{4} = counter + (1:length(modules(1).mesh.boundary.edge{4}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{4});
                   end
                else
                   if iX == 1
                        local.vertex{1} = temp(iX,iY-1).vertex{3};
                        local.vertex{2} = temp(iX,iY-1).vertex{4};
                        local.vertex{3} = counter + 1;
                        local.vertex{4} = counter + 2;
                        counter = counter + 2;

                        local.edge{1} = counter + (1:length(modules(1).mesh.boundary.edge{1}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{1});
                        local.edge{2} = counter + (1:length(modules(1).mesh.boundary.edge{2}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{2});
                        local.edge{3} = temp(iX,iY-1).edge{4};
                        local.edge{4} = counter + (1:length(modules(1).mesh.boundary.edge{4}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{4});
                   else
                        local.vertex{1} = temp(iX,iY-1).vertex{3};
                        local.vertex{2} = temp(iX,iY-1).vertex{4};
                        local.vertex{3} = temp(iX-1,iY).vertex{4};
                        local.vertex{4} = counter + 1;
                        counter = counter + 1;

                        local.edge{1} = temp(iX-1,iY).edge{2};
                        local.edge{2} = counter + (1:length(modules(1).mesh.boundary.edge{2}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{2});
                        local.edge{3} = temp(iX,iY-1).edge{4};
                        local.edge{4} = counter + (1:length(modules(1).mesh.boundary.edge{4}))';
                        counter = counter + length(modules(1).mesh.boundary.edge{4});
                   end
                end

                temp(iX,iY) = local;

                domains((iY-1)*domainSize(1) + iX).moduleId = 1;
                domains((iY-1)*domainSize(1) + iX).boundary = local;
                domains((iY-1)*domainSize(1) + iX).boundary.whole = [ local.vertex{1}; local.vertex{2}; local.vertex{3}; local.vertex{4}; ...
                        local.edge{1}; local.edge{2}; local.edge{3}; local.edge{4} ];
                domains((iY-1)*domainSize(1) + iX).shift = [(iX-1)*meshSize(1), (iY-1)*meshSize(2)];

        end
    end

end

function [loading] = extract_loading(domains, modules, domainSize, prescribedTraction)

    % Prescribe fixed DOFs at left-hand side
    bar = [];
    for iY = 1:domainSize(2)
       bar = [bar; ...
           domains((iY-1)*domainSize(1) + 1).boundary.vertex{1}; ...
           domains((iY-1)*domainSize(1) + 1).boundary.edge{1}; ...
           domains((iY-1)*domainSize(1) + 1).boundary.vertex{3} ];
    end
    bar = unique(bar,'stable');
    bar = reshape( [2*bar' - 1; 2*bar'], [], 1);

    for i = 1:length(bar)
       loading.uPres{i} = [bar(i), 0.0]; 
    end
    
    % Prescribe traction at right-hand side
    nExtDOFs = 0;
    for iD = 1:length(domains)
        nExtDOFs = max(nExtDOFs, max(domains(iD).boundary.whole));
    end
    
    Fext = zeros(nExtDOFs,2);
    for iY = 1:domainSize(2)
        
        mId = domains(iY*domainSize(1)).moduleId;
        
        localNodeIds = [ modules(mId).mesh.boundary.vertex{2}; ...
            modules(mId).mesh.boundary.edge{2}; ...
            modules(mId).mesh.boundary.vertex{4} ];
        
        assert( issorted(modules(mId).mesh.nodes(localNodeIds, 2)), 'Nodal ids for the edge must be sorted');
        
        globalNodeIds = [ domains(iY*domainSize(1)).boundary.vertex{2}; ...
            domains(iY*domainSize(1)).boundary.edge{2}; ...
            domains(iY*domainSize(1)).boundary.vertex{4} ];
        
        for ii = 1:length(localNodeIds)-1            
           L = modules(mId).mesh.nodes(localNodeIds(ii+1), 2) ...
               - modules(mId).mesh.nodes(localNodeIds(ii), 2);
           
           Fext(globalNodeIds(ii),:) = Fext(globalNodeIds(ii),:) + 0.5 * L * [prescribedTraction(1), prescribedTraction(2)];
           Fext(globalNodeIds(ii+1),:) = Fext(globalNodeIds(ii+1),:) + 0.5 * L * [prescribedTraction(1), prescribedTraction(2)];            
        end

    end
    
    counter = 0;
    for ii = 1:size(Fext,1)
        if any(Fext(ii,:) ~= 0)
            counter = counter + 1;
            loading.Fext{counter} = [2*ii - 1; Fext(ii,1)];
            
            counter = counter + 1;
            loading.Fext{counter} = [2*ii; Fext(ii,2)];
        end
    end
    
end