add_executable(MTO.Application
    src/main.cpp 
)

target_link_libraries(MTO.Application 
    PRIVATE 
        MTO.Engine 
        fmt::fmt
        spdlog::spdlog
)

set_target_properties(MTO.Application PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)

# Add project-recommended warning flags
if (MTO_USE_RECOMMENDED_WARNING)
    target_compile_options(MTO.Application
        PRIVATE 
            ${MTO_RECOMMENDED_WARNINGS}
    )
endif()