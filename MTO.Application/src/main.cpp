#include "Config.h"
#include "Task.h"
#include "Utilities.h"

#include <fmt/chrono.h>
#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>

#include <filesystem>
#include <iostream>

int main(int argc, char* argv[])
{
    if (argc != 2)
        throw std::invalid_argument{"MTO.Application requires one command-line input argument -- path to a JSON setting file"};

    try {
        const auto inputFilePath = std::filesystem::path{argv[1]};
        if (std::filesystem::exists(inputFilePath)==false)
            throw std::invalid_argument{"Input file " + inputFilePath.string() + " does not exist."};
        const auto inJSON = mto::load_JSON_file(inputFilePath);
        const auto outputFolder = inJSON.contains("outputFolder")
            ? std::filesystem::path{inJSON["outputFolder"].get<std::string>()}
            : std::filesystem::path{""};

        auto logger = mto::initialize_logger(outputFolder);
        logger->info("-----------------------------------------------");
        logger->info("                MTO.Application");
        logger->info("                  ver. {}.{}.{}", PROJECT_VERSION_MAJOR, PROJECT_VERSION_MINOR, PROJECT_VERSION_PATCH);
        logger->info("         Martin Doskar & Marek Tyburec");
        logger->info("      OpenMechanics Group @ CTU in Prague");
        logger->info("\n- Compiled with {} {}", USED_COMPILER_TYPE, USED_COMPILER_VERSION);
#ifdef MTO_USE_PARDISO
        logger->info("- Linked with Intel MKL and MKL PARDISO solver");
#elif defined(MTO_USE_INTELMKL)
        logger->info("- Linked with MKL");
#endif
#ifdef MTO_USE_PARALLEL_ALGORITHMS
        logger->info("- Using parallel execution in STL algorithms and OpenMP {} via {}",USED_OMP_VERSION, USED_OMP_LIBRARY);
#endif
        logger->info("-----------------------------------------------\n");
        logger->info("{0:%Y-%m-%d}\n{0:%H:%M:%S}: Optimisation started with input file {1}",
            fmt::localtime(std::time(nullptr)), inputFilePath.string());

        auto sw = spdlog::stopwatch{};
        auto task = mto::Task{inJSON};
        if (PRINT_DETAILED_TIMING)
            logger->info("Task initialized in {} s", sw);

        sw.reset();
        const auto objectiveValue = task.optimize();
        if (PRINT_DETAILED_TIMING)
            logger->info("Task optimization finished in {} s", sw);

        logger->info("{0:%H:%M:%S}: MTO.Application has finished\n{0:%Y-%m-%d}", fmt::localtime(std::time(nullptr)));

    } catch (const std::exception& e) {
        if (auto existingLogger = spdlog::get("MTO.Logger");
            existingLogger == nullptr) {
            std::cout << e.what() << '\n';
            std::cerr << e.what() << '\n';
        }
        else
            existingLogger->error(e.what());

        return 1;
    }

    return 0;
}
