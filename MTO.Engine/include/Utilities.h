#pragma once

#include <nlohmann/json.hpp>
#include <spdlog/spdlog.h>

#include <array>
#include <filesystem>
#include <fstream>
#include <memory>
#include <string_view>

namespace mto {

auto load_JSON_file(const std::filesystem::path& inFilePath) -> nlohmann::json;

auto initialize_logger(std::filesystem::path outputFolder = "") -> std::shared_ptr<spdlog::logger>;

struct LoadPair
{
    LoadPair(size_t inDOFid, double inValue)
        : DOFid{inDOFid}, value{inValue} {};
    size_t DOFid;
    double value;
};

class Triplet : public std::array<std::size_t, 3>
{
public:
    [[nodiscard]] auto get_number_of_elements() const -> std::size_t;
};

auto double_to_formated_string(double num) -> std::string;

}    // namespace mto