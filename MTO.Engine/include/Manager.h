#pragma once

#include "Discretisation.h"
#include "LinearApproximation.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <nlohmann/json.hpp>

#include <optional>
#include <vector>

namespace mto {

enum class DensityType {
    Design,
    Filtered,
    Projected
};

enum class FilterType {
    Kernel,
    Helmholtz,
    Sensitivity
};

class Manager
{
public:
    Manager(const nlohmann::json& inJSON, Discretisation* inDiscretisation);

    auto get_filter_type() const { return this->filterType; };
    auto get_current_design() const -> const Eigen::VectorXd& { return (this->modularFlag) ? this->aModular : this->aDesign; };
    auto get_normalized_relative_volume_constraint_sensitivity() const -> LinearApproximation;
    auto print_densities_for_VTK(const DensityType type = DensityType::Design, const bool useBinary = false) const -> std::string;
    auto transform_from_physical_to_design_sensitivity(const LinearApproximation& approximation) const -> LinearApproximation;
    auto quantify_grayness_of_physical_densities() const -> double;
    void update_sensitivity_filter();
    void update_unknown_design_densities(const Eigen::VectorXd& updatedDensities);

private:
    FilterType filterType;
    double filterRadius;

    Eigen::VectorXd aDesign;
    Eigen::VectorXd aFiltered;
    Eigen::VectorXd aProjected;

    Discretisation* discretisation;
    std::optional<double> relVolumeConstraint{std::nullopt};

    bool modularFlag{false};
    Eigen::SparseMatrix<double> modularProjection{};
    Eigen::VectorXd aModular{};

    std::optional<Eigen::SparseMatrix<double>> filterKernelMtrx{std::nullopt};
    std::optional<Eigen::SparseMatrix<double>> filterKernelMtrxInitial{std::nullopt};
    std::optional<Eigen::SparseMatrix<double>> filterHelmholtzMtrx{std::nullopt};
    std::optional<Eigen::SparseMatrix<double>> filterHelmholtzPe2n{std::nullopt};
    std::optional<Eigen::SparseMatrix<double>> filterHelmholtzPn2e{std::nullopt};
    Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> filterHelmholtzSolver{};

    void apply_filter();
    void apply_projection();
    void initialize_Helmholtz_filter();
    void initialize_kernel_filter();
    void initialize_sensitivity_filter();
    void update_densities_in_discretisation();
};

}    // namespace mto