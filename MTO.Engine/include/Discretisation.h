#pragma once

#include "LinearApproximation.h"
#include "OptimizerInterface.h"

#include <Eigen/Dense>
#include <nlohmann/json.hpp>

#include <array>
#include <filesystem>
#include <optional>
#include <vector>

namespace mto {

struct BoundaryBox
{
    BoundaryBox();
    BoundaryBox(const nlohmann::json& inJSON);
    BoundaryBox(const Eigen::Vector3d minCorner, const Eigen::Vector3d maxCorner);
    BoundaryBox(const Eigen::Vector2d minCorner, const Eigen::Vector2d maxCorner);

    void update_with_point(const Eigen::Vector3d& vector);
    void update_with_point(const Eigen::Vector2d& vector);

    size_t dim;
    Eigen::Vector3d minCorner;
    Eigen::Vector3d maxCorner;
};

struct Boundary
{
    std::array<std::optional<size_t>, 4> vertices{std::nullopt, std::nullopt, std::nullopt, std::nullopt};
    std::array<std::vector<size_t>, 4> edges{std::vector<size_t>{}, std::vector<size_t>{}, std::vector<size_t>{}, std::vector<size_t>{}};
};

struct ElementOrigin
{
    size_t moduleId;
    size_t elementId;
    size_t subdomainId;
};

struct Node
{
    Node(double inX, double inY, double inZ = 0.0)
        : coord{inX, inY, inZ} {};
    Node(const Eigen::Vector3d& inCoord)
        : coord{inCoord} {};

    auto is_inside(const BoundaryBox& box, const double relEps = 1e-6) const -> bool;
    auto is_coinciding(const Node& otherNode, const double geomEps = 1e-6) const -> bool;

    Eigen::Vector3d coord{};
};

class Discretisation;

struct Element
{
    Element(const std::array<int, 4> inNodeIds, const Discretisation* owningDiscretisation, double initialDensity = 1.0, bool inIsFixed = false)
        : nodeIds{std::move(inNodeIds)}, parent{owningDiscretisation}, density{initialDensity}, isFixed{inIsFixed}
    {
        this->compute_volume();
    };

    auto is_fixed() const -> bool { return this->isFixed; };
    auto get_density() const -> double { return this->density; };
    void set_density(double inDensity) { this->density = inDensity; };
    void set_fixed_density(double inDensity)
    {
        this->isFixed = true;
        this->density = inDensity;
    };

    void compute_volume();

    auto get_element_type() const { return this->type; };
    auto get_number_of_nodes() const { return this->nodeIds.size(); };
    auto get_node_ids() const -> const std::array<int, 4>& { return this->nodeIds; };

    auto get_whole_volume() const -> double { return this->volume; };
    auto get_current_volume() const -> double { return (this->density * this->volume); };

    auto get_centre_of_gravity() const -> Eigen::Vector3d;

    auto get_superconvergence_coord_and_B() const -> std::pair<Eigen::Vector3d, Eigen::MatrixXd>;

    const Discretisation* parent;
    std::array<int, 4> nodeIds;
    double volume;
    double density;
    bool isFixed;
    int type{8};
    int freeIndex{-1};
};

struct IndexDistancePair
{
    size_t index;
    double distance;
};

enum class DiscretisationType { RegularGrid,
    Mesh,
    ModuleAssembly };

class Discretisation
{
public:
    Discretisation() = default;
    Discretisation(const std::filesystem::path& inFilePath);
    Discretisation(const nlohmann::json& inJSON);

    void merge_discretisation(const Discretisation& addedDiscretisation, const double geomEps = 1e-6);

    auto get_number_of_nodes() const { return this->nodes.size(); };
    auto get_number_of_elements() const { return this->elements.size(); };
    auto get_number_of_free_elements() const -> size_t;

    auto get_ith_node_coordinates(size_t i) const { return this->nodes.at(i).coord; };
    auto get_ith_element(size_t i) const -> const Element& { return this->elements.at(i); };
    auto get_elements_begin() const { return std::begin(this->elements); };
    auto get_elements_end() const { return std::end(this->elements); };

    auto get_element_centres_of_gravity() const -> std::vector<Eigen::Vector3d>;
    auto get_element_whole_volumes() const -> std::vector<double>;
    auto get_node_element_adjacency() const -> std::vector<std::vector<size_t>>;
    auto get_adjacent_free_elements_within_given_radius(const double radius) const -> std::vector<std::vector<IndexDistancePair>>;

    void update_element_densities(double newConstantDensity);
    void update_element_densities(const Eigen::VectorXd& newDensities);

    auto is_modular() const { return this->modularFlag; };
    auto get_modular_element_map() -> const std::vector<ElementOrigin>& { return this->subdomainElementMapping; };
    auto get_renum_map() const -> std::vector<size_t>;

    auto get_whole_volume() const -> double;
    auto get_current_volume() const -> double;
    auto get_relative_current_volume() const -> double;

    auto get_whole_free_volume() const -> double;
    auto get_current_free_volume() const -> double;
    auto get_relative_current_free_volume() const -> double;

    auto get_relative_volume_sensitivity() const -> LinearApproximation;

    auto print_data_for_VTK(const bool useBinary = false) const -> std::string;
    auto print_data_for_SVG(double multiplier = 1.0) const -> std::string;
    auto print_modules_data_for_SVG(double multiplier = 1.0) const -> std::pair<std::vector<std::string>, std::vector<BoundaryBox>>;

    auto get_node_ids_inside_box(const BoundaryBox& box, double relEps = 1e-6) const -> std::vector<size_t>;

    auto get_bounding_box() const -> BoundaryBox;

private:
    std::vector<Node> nodes{};
    std::vector<Element> elements{};

    bool modularFlag{false};
    std::vector<ElementOrigin> subdomainElementMapping;

    mutable std::optional<double> volume{std::nullopt};
    mutable std::optional<double> freeVolume{std::nullopt};

    void enumerate_free_elements();
};

}    // namespace mto