#pragma once

#include <Eigen/Sparse>

#include <memory>

namespace mto {

class SolverInterface
{
public:
    SolverInterface(const SolverInterface&) = delete;
    SolverInterface(SolverInterface&&) = delete;
    SolverInterface& operator=(const SolverInterface&) = delete;
    SolverInterface& operator=(SolverInterface&&) = delete;
    virtual ~SolverInterface() = default;

    virtual auto clone() -> std::unique_ptr<SolverInterface> = 0;

    virtual void analyzePattern(const Eigen::SparseMatrix<double>& A) = 0;
    virtual void factorize(const Eigen::SparseMatrix<double>& A) = 0;
    virtual auto solve(const Eigen::VectorXd& b) const -> Eigen::VectorXd = 0;
    virtual auto info() const -> Eigen::ComputationInfo = 0;
    virtual auto name() const -> std::string = 0;
    virtual auto is_initialized() const -> bool = 0;

protected:
    SolverInterface() = default;
};

}    // namespace mto