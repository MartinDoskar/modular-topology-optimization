#pragma once

#include "OptimizerInterface.h"

namespace mto {

struct ParametersOC
{
    ParametersOC() = default;
    /*
    version:
    0   OC for compliance, based on Bendsoe, M., Sigmund, O. (2003) Topology optimization: theory, methods and applications. Springer, Berlin.
    1   OC method with inconsistent modification (1e-10 by default), based on Bendsoe, M., Sigmund, O. (2003) Topology optimization: theory, methods and applications. Springer, Berlin.
    2   Generalized OC, based on Kim, N. H., Dong, T., Weinberg, D., & Dalidd, J. (2021). Generalized Optimality Criteria Method for Topology Optimization. Applied Sciences, 11(7), 3175.
    */
    int version = 0;
    double lOCbeg = 0.0;
    double lOCend = 1e09;
    double stepOC = 0.1;
    double dampingOC = 0.5;
    double lOCminLim = 1e-40;
    double lOCstepLim = 1e-6;
    double inconsistentModification = -1e20;
    // Parameters for continuation based on Groenwold, A. A., & Etman, L. F. P. (2009). A simple heuristic for gray-scale suppression in optimality criterion-based topology optimization. Structural and Multidisciplinary Optimization, 39(2), 217–225.
    double qmax = 1.0;
    double qMultiplier = 1.01;
    int qAfterIter = 20;
};

class OptimizerOC
    : public OptimizerInterface
{
public:
    OptimizerOC() = default;
    OptimizerOC(ParametersOC params)
        : parameters{std::move(params)} {};

    auto clone() -> std::unique_ptr<OptimizerInterface> override;

    auto update(
        const Eigen::VectorXd& x,
        const LinearApproximation& objective,
        const std::vector<LinearApproximation>& constraints) -> Eigen::VectorXd override;

    void set_parameters(ParametersOC inParams) { this->parameters = std::move(inParams); };

private:
    // Method parameters
    ParametersOC parameters{};

    // Continuation parameters
    int iter{0};      // iteration number
    double q{1.0};    // continuation parameter

    // Internal variables for GOCM (Version 2)
    Eigen::VectorXd gl;        // normalized constraints at the previous iteration
    Eigen::VectorXd lOCmid;    // Lagrange multipliers
    double objInit{0.0};       // needed to memorize objective function scaling
};

}    // namespace mto