#pragma once

#include "Discretisation.h"
#include "Manager.h"
#include "OptimizerInterface.h"
#include "Problem.h"
#include "Utilities.h"

#include <Eigen/Dense>
#include <spdlog/spdlog.h>

#include <filesystem>
#include <memory>

namespace mto {

auto create_optimizer(const nlohmann::json& inJSON) -> std::unique_ptr<OptimizerInterface>;

class TaskParameters
{
public:
    TaskParameters() = default;
    TaskParameters(const nlohmann::json& inJSON);

    auto get_penalisation(const int iteration = 1) const -> double;
    auto get_iteration_limit() const { return this->nMaxIter; };
    auto get_design_change_limit() const { return this->limDesignChange; };
    auto get_objective_change_limit() const { return this->limObjectiveChange; };
    auto get_grayness_limit() const { return this->limGrayness; }
    auto use_binary_VTK() const { return this->useBinaryVTK; };
    auto are_elementwise_duals_required() const { return this->storeElementwiseDuals; };
    auto are_nodalwise_duals_required() const { return this->storeNodalwiseDuals; };
    auto get_state_saving_stepVTK() const { return this->saveEachNthStepVTK; };
    auto get_state_saving_stepSVG() const { return this->saveEachNthStepSVG; };
    auto get_state_saving_step_modulesSVG() const { return this->saveModulesEachNthStepSVG; };
    auto get_output_folder() const { return this->outputFolder; };

    auto is_at_final_penalisation(const int iteration) const -> bool;

private:
    size_t nMaxIter{100};
    double limDesignChange{1e-6};
    double limObjectiveChange{1e-3};
    double limGrayness{1e-3};

    double penalisation{3.0};

    bool useContinuation{false};
    double penalisationInit{3.0};
    double penalisationIncrement{0.0};
    size_t penalisationStep{1};
    double penalisationLim{3.0};

    bool useBinaryVTK{true};
    bool storeNodalwiseDuals{true};
    bool storeElementwiseDuals{false};

    size_t saveEachNthStepVTK{1};
    size_t saveEachNthStepSVG{1000};
    size_t saveModulesEachNthStepSVG{1000};
    std::filesystem::path outputFolder{"./"};
};

class Task
{
public:
    Task(const std::filesystem::path& inFilePath);
    Task(const nlohmann::json& inJSON);

    auto optimize() -> double;
    void save_state_in_VTK(const std::filesystem::path& outFilePath, const bool useBinary = false, const bool printIncrement = false) const;
    void save_state_in_SVG(const std::filesystem::path& outFilePath) const;
    void save_state_modules_in_SVG(const std::filesystem::path& outFilePath) const;

private:
    TaskParameters parameters;
    Discretisation discretisation;
    Manager designManager;
    std::unique_ptr<OptimizerInterface> optimizer;
    std::vector<Problem> problems;
    std::shared_ptr<spdlog::logger> logger{nullptr};
};

}    // namespace mto
