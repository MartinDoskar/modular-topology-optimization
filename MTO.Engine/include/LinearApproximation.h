#pragma once

#include <Eigen/Dense>

namespace mto {

enum class LinearApproximationType { Undefined,
    State,
    Volume };

struct LinearApproximation
{
    double funValue{};
    Eigen::VectorXd funGradient{};
    LinearApproximationType type{LinearApproximationType::Undefined};

    auto operator+=(const double a) -> LinearApproximation&;
    auto operator-=(const double a) -> LinearApproximation&;
    auto operator*=(const double a) -> LinearApproximation&;
    auto operator/=(const double a) -> LinearApproximation&;

    friend auto operator+(LinearApproximation lhs, const double rhs) -> LinearApproximation;
    friend auto operator+(const double lhs, LinearApproximation rhs) -> LinearApproximation;
    friend auto operator-(LinearApproximation lhs, const double rhs) -> LinearApproximation;
    friend auto operator-(const double lhs, LinearApproximation rhs) -> LinearApproximation;
    friend auto operator*(LinearApproximation lhs, const double rhs) -> LinearApproximation;
    friend auto operator*(const double lhs, LinearApproximation rhs) -> LinearApproximation;
    friend auto operator/(LinearApproximation lhs, const double rhs) -> LinearApproximation;
};

}    // namespace mto