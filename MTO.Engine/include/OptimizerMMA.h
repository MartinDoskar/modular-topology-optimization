#pragma once

#include "OptimizerInterface.h"

#include <Eigen/Dense>

#include <limits>
#include <vector>

namespace mto {

std::string vector_as_string(const Eigen::VectorXd& v);

struct ParametersMMA
{
    double sInit{0.5};                                   // Initial value for asymptote values (width deduced from xMax - xMin)
    double sIncrease{1.2};                               // Initial value for asymptote values
    double sDecrease{0.7};                               // Initial value for asymptote values
    double xMinMaxDiffThreshold{1.0e-6};                 // Numerical threshold when using the difference between bounds
    double maxRelAsymptoteWidth{100.0};                  // Max relative (w.r.t. xMinMaxDiff) distance when updating asymptotes
    double minRelAsymptoteWidth{1.0e-5};                 // Min relative (w.r.t. xMinMaxDiff) distance when updating asymptotes
    double alphaBetaCoeff{0.1};                          // Coefficient of convex combination [alpha/beta] = (1-abCoeff) * [L/U] + abCoeff * x
    double alphaBetaMove{0.5};                           // Coefficient of alpha/beta bound moving with x based on a fraction of xMax-xMin [introduced in Ref. 2]
    double initEpsilonValue{1.0};                        // Initial value of epsilon (~ relaxation of complementarity) for each subproblem solve
    double epsilonReductionCoeff{0.1};                   // Coefficient for reduction epsilon value when epsilonReductionThreshold is satisfied
    double epsilonReductionThreshold{0.1};               // Relative threshold of infty-norm of residuum below which the epsilon is reduced (see above)
    double epsilonConvergenceLimit{0.1};                 // Limit value of epsilon for suproblem convergence
    double addedAbsGradientCoeff{0.001};                 // Additional term added to p nad q coefficients related to abs value of the gradient
    double addedXdiffInverseCoeff{1.0e-5};               // Additional term added to p and q coefficients related to inverse of max-min difference
    double strictIneqCoeff{0.01};                        // Coefficient for strict inequality check during line search
    int nMaxIterations{500};                             // Maximum number of subproblem iterations
    double minRelSteplength{1.0 / std::pow(2.0, 50)};    // Minimal relative length of step length in line search
};

//
// Treated specifially if: i)  when moving asymptotes, they should no be further then ???*(xmax-xmin) and closer then ???*(xmax-xmin) --- or even epsilon difference
//                         ii) current value is close to the extreme limits xmin and xmax ... actually below ... how come???
// prepared for different xmina nd xmax if the need arises
// L < alpha <= x <= beta < U; xmin <= alpha <= x <= beta <= xmax
// TODO: Follow the implementation, i.e. i = 1,...,M, j = 1,...,N
// P and Q are tranposed

class OptimizerMMA
    : public OptimizerInterface
{
public:
    OptimizerMMA(size_t nVars, size_t nConstraints, Eigen::VectorXd xMin, Eigen::VectorXd xMax,
        double a0, Eigen::VectorXd a, Eigen::VectorXd c, Eigen::VectorXd d);
    OptimizerMMA(size_t nVars, size_t nConstraints, double xMinScal, double xMaxScal,
        double a0 = 1.0, double a = 0.0, double c = 1.0e4, double d = 0.0);

    auto clone() -> std::unique_ptr<OptimizerInterface> override;

    auto update(
        const Eigen::VectorXd& x,
        const LinearApproximation& objective,
        const std::vector<LinearApproximation>& constraints) -> Eigen::VectorXd override;

    void set_parameters(ParametersMMA inParams) { this->parameters = inParams; };

private:
    struct SolverState
    {
        SolverState() = delete;
        SolverState(Eigen::VectorXd inX, Eigen::VectorXd inY, double inZ, Eigen::VectorXd inLambda,
            Eigen::VectorXd inKsi, Eigen::VectorXd inEta, Eigen::VectorXd inMu, double inZeta, Eigen::VectorXd inS);

        SolverState& operator+=(const SolverState& rhs);
        SolverState& operator-=(const SolverState& rhs);
        SolverState& operator*=(const double coeff);

        friend SolverState operator+(SolverState lhs, const SolverState& rhs)
        {
            lhs += rhs;
            return lhs;
        }
        friend SolverState operator-(SolverState lhs, const SolverState& rhs)
        {
            lhs -= rhs;
            return lhs;
        }
        friend SolverState operator*(const double coeff, SolverState lhs)
        {
            lhs *= coeff;
            return lhs;
        }
        friend SolverState operator*(SolverState lhs, const double coeff)
        {
            lhs *= coeff;
            return lhs;
        }
        friend std::ostream& operator<<(std::ostream& output, const SolverState& s)
        {
            output << "Solver state:\n";
            output << "x = " << vector_as_string(s.x) << '\n';
            output << "y = " << vector_as_string(s.y) << '\n';
            output << "z = " << s.z << '\n';
            output << "λ = " << vector_as_string(s.lambda) << '\n';
            output << "ξ = " << vector_as_string(s.ksi) << '\n';
            output << "η = " << vector_as_string(s.eta) << '\n';
            output << "μ = " << vector_as_string(s.mu) << '\n';
            output << "ζ = " << s.zeta << '\n';
            output << "s = " << vector_as_string(s.s) << '\n';

            return output;
        }

        Eigen::VectorXd x{};
        Eigen::VectorXd y{};
        double z{};
        Eigen::VectorXd lambda{};
        Eigen::VectorXd ksi{};
        Eigen::VectorXd eta{};
        Eigen::VectorXd mu{};
        double zeta{};
        Eigen::VectorXd s{};
    };

    void update_asymptotes(const Eigen::VectorXd& x);    // TODO: Eigenize
    void update_alphas_betas(const Eigen::VectorXd& x);

    void construct_subproblem(const Eigen::VectorXd& x, const LinearApproximation& objective, const std::vector<LinearApproximation>& constraints);
    auto construct_p_and_q_coeffs(const Eigen::VectorXd& dfdx, const Eigen::VectorXd& Ux, const Eigen::VectorXd& xL) const -> std::pair<Eigen::VectorXd, Eigen::VectorXd>;

    void print_subproblem_info() const;

    auto solve_subproblem() const -> SolverState;
    auto initialize_solver_state() const -> SolverState;
    auto compute_relaxed_subproblem_residuum(const SolverState& state, const double epsilon) const -> Eigen::VectorXd;
    auto compute_Newton_direction(const SolverState& state, const double epsilon) const -> SolverState;
    auto compute_line_search_length(const SolverState& state, const SolverState& direction, const double epsilon) const -> double;
    auto compute_dpsidx(const SolverState& state) const -> Eigen::VectorXd;
    auto compute_gi(const SolverState& state) const -> Eigen::VectorXd;

    void update_previously_stored_states(const Eigen::VectorXd& Xnew);

    // Problem definition
    const size_t N;
    const size_t M;

    const Eigen::VectorXd xMin;
    const Eigen::VectorXd xMax;

    const double a0;
    const Eigen::VectorXd a;
    const Eigen::VectorXd c;
    const Eigen::VectorXd d;

    // Method parameters
    ParametersMMA parameters{};

    // Internal state variables of the algorithm
    size_t nRuns{0};          // No. of cycles MMA has run
    Eigen::VectorXd L;        // Lower asymptote
    Eigen::VectorXd U;        // Upper asymptote
    Eigen::VectorXd alpha;    // Shifted lower bound for state x
    Eigen::VectorXd beta;     // Shifted upper bound for state x

    // State variables of the MMA approximation
    Eigen::VectorXd b;
    Eigen::VectorXd p0;
    Eigen::VectorXd q0;
    Eigen::MatrixXd P;
    Eigen::MatrixXd Q;

    // Previous two states
    Eigen::VectorXd xMinus1;
    Eigen::VectorXd xMinus2;
};

}    // namespace mto