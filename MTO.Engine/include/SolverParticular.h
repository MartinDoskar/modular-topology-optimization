#pragma once

#include "SolverInterface.h"

#ifdef MTO_USE_PARDISO
#include <Eigen/PardisoSupport>
#endif

namespace mto {

class SolverParticular
    : public SolverInterface
{
public:
    SolverParticular() = default;

    auto clone() -> std::unique_ptr<SolverInterface> override
    {
        return std::make_unique<SolverParticular>();
    };

    auto analyzePattern(const Eigen::SparseMatrix<double>& A) -> void override
    {
        if (!this->alreadyAnalyzed) {
            this->solver.analyzePattern(A);
            this->alreadyAnalyzed = true;
        }
    };
    auto factorize(const Eigen::SparseMatrix<double>& A) -> void override
    {
        this->solver.factorize(A);
    }
    auto solve(const Eigen::VectorXd& b) const -> Eigen::VectorXd override
    {
        return this->solver.solve(b);
    }
    auto info() const -> Eigen::ComputationInfo override
    {
        return this->solver.info();
    }
    auto name() const -> std::string override
    {
        return this->solverName;
    }
    auto is_initialized() const -> bool override
    {
        return this->alreadyAnalyzed;
    };

private:
#ifdef MTO_USE_PARDISO
    Eigen::PardisoLLT<Eigen::SparseMatrix<double>> solver{};
    std::string solverName{"PardisoLLT"};
#else
    Eigen::SimplicialLLT<Eigen::SparseMatrix<double>> solver{};
    std::string solverName{"SimplicialLLT"};
#endif
    bool alreadyAnalyzed{false};
};

}    // namespace mto