#pragma once

#include "LinearApproximation.h"

#include <Eigen/Dense>

#include <memory>

namespace mto {

enum class OptimizerType {
    OC,
    MMA
};


class OptimizerInterface
{
public:
    virtual ~OptimizerInterface() = default;

    virtual auto clone() -> std::unique_ptr<OptimizerInterface> = 0;

    virtual auto update(
        const Eigen::VectorXd& x,
        const LinearApproximation& objective,
        const std::vector<LinearApproximation>& constraints) -> Eigen::VectorXd = 0;

protected:
    OptimizerInterface() = default;
    OptimizerInterface(const OptimizerInterface&) = default;
    OptimizerInterface(OptimizerInterface&&) = default;
    OptimizerInterface& operator=(const OptimizerInterface&) = default;
    OptimizerInterface& operator=(OptimizerInterface&&) = default;
};

}    // namespace mto