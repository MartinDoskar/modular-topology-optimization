#pragma once

#include "Config.h"
#include "Discretisation.h"
#include "OptimizerInterface.h"
#include "SolverInterface.h"
#include "Utilities.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <nlohmann/json.hpp>

#include <memory>
#include <optional>
#include <vector>

namespace mto {

class DOFManager
{
public:
    DOFManager() = default;
    DOFManager(const std::vector<LoadPair>& prescribedDOFs, const size_t nNodes, const size_t nDOFsPerNode);

    auto extract_unknown_part(const Eigen::VectorXd& fromVec) const -> Eigen::VectorXd;
    void update_with_unknown_part(Eigen::VectorXd& updateVec /*inout*/, const Eigen::VectorXd& fromVec) const;

    auto map_DOFs_to_unknowns(const std::vector<size_t>& inDOFs) const -> std::vector<std::optional<size_t>>;
    auto map_ith_DOF_to_unknown(const size_t i) const -> std::optional<size_t> { return this->dof2unknown.at(i); };

    auto get_number_of_DOFs() const { return this->nDOFs; };
    auto get_number_of_unknowns() const { return this->nUnknowns; };

private:
    size_t nDOFs{0};
    size_t nUnknowns{0};
    std::vector<std::optional<size_t>> dof2unknown;
};

class Problem
{
public:
    Problem(const nlohmann::json& inJSON, Discretisation* inDiscretisation);

    Problem(const Problem&) = delete;
    Problem(Problem&&) noexcept = default;
    Problem& operator=(const Problem&) = delete;
    Problem& operator=(Problem&&) noexcept = default;
    ~Problem() = default;

    void solve_state(const double p);
    auto compute_objective() const -> double { return this->cAll.dot(this->uAll); };
    auto compute_sensitivity(const double p) const -> LinearApproximation;

    bool is_simple_compliance() const { return isSimpleCompliance; };

    void compute_duals_elementwise(const double p);
    void compute_duals_nodalwise(const double p, const bool useSPR = false);

    auto print_vertex_state_for_VTK(const bool useBinary = false) const -> std::string;
    auto print_element_state_for_VTK(const bool useBinary = false) const -> std::string;

private:
    Discretisation* discretisation;
    DOFManager manager;

    bool isSimpleCompliance;

    double Emin{1e-6};
    double E0{1.0};
    double nu{0.3};

    std::unique_ptr<SolverInterface> solver;

    Eigen::VectorXd uAll;         // State vector (all DOFs)
    Eigen::VectorXd lambdaAll;    // Adjoint state vector (all DOFs)
    Eigen::VectorXd FextAll;      // External load vector (all DOFs)
    Eigen::VectorXd cAll;         // Objective vector (all DOFs)

    Eigen::VectorXd u;         // Current state (unknowns only)
    Eigen::VectorXd lambda;    // Adjoint state (unknowns only)
    Eigen::VectorXd Fext;      // External load vector (unknowns only)
    Eigen::VectorXd c;         // Objective vector (unknowns only)

    std::vector<LoadPair> prescribedValuePairs;
    std::optional<std::vector<LoadPair>> prescribedLoadPairs{std::nullopt};
    std::optional<std::vector<LoadPair>> objectiveVectorPairs{std::nullopt};
    std::optional<std::vector<LoadPair>> additionalSprings{std::nullopt};
    std::optional<std::vector<Eigen::Vector3d>> storedDualsNodalwise{std::nullopt};
    std::optional<std::vector<Eigen::Vector3d>> storedDualsElementwise{std::nullopt};

    auto get_material_stiffness(double p, double rho) const -> Eigen::Matrix<double, 3, 3>;
    auto compute_element_stiffness(double hx, double hy) const -> Eigen::Matrix<double, 8, 8>;
};

}    // namespace mto