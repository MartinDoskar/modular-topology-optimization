#include "Discretisation.h"

#include "Config.h"
#include "Utilities.h"

#include <base64.h>
#include <fmt/core.h>
#include <spdlog/stopwatch.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>

namespace mto {

BoundaryBox::BoundaryBox()
    : dim{3},
      minCorner{Eigen::Vector3d::Constant(std::numeric_limits<double>::infinity())},
      maxCorner{Eigen::Vector3d::Constant(-std::numeric_limits<double>::infinity())}
{
}

BoundaryBox::BoundaryBox(const nlohmann::json& inJSON)
{
    if (!inJSON.contains("lowerBound") || !inJSON.contains("upperBound"))
        throw std::invalid_argument{"BoundaryBox::BoundaryBox: Input JSON entry must contain lowerBound and upperBound arrays"};

    const auto lb = inJSON["lowerBound"].get<std::vector<double>>();
    const auto ub = inJSON["upperBound"].get<std::vector<double>>();

    if (lb.size() != ub.size())
        throw std::invalid_argument{"BoundaryBox::BoundaryBox: Input entries do not have the sime size."};

    this->dim = lb.size();
    if (this->dim == 2) {
        this->minCorner = {lb.at(0), lb.at(1), 0.0};
        this->maxCorner = {ub.at(0), ub.at(1), 0.0};
    }
    else if (this->dim == 3) {
        this->minCorner = {lb.at(0), lb.at(1), lb.at(2)};
        this->maxCorner = {ub.at(0), ub.at(1), ub.at(2)};
    }
    else
        throw std::invalid_argument{"BoundaryBox::BoundaryBox: Only two- and three-dimensional entries are supported."};
}

BoundaryBox::BoundaryBox(const Eigen::Vector3d inMinCorner, const Eigen::Vector3d inMaxCorner)
    : dim{3}, minCorner{inMinCorner}, maxCorner{inMaxCorner}
{
}

BoundaryBox::BoundaryBox(const Eigen::Vector2d inMinCorner, const Eigen::Vector2d inMaxCorner)
    : dim{2}, minCorner{inMinCorner(0), inMinCorner(1), 0.0}, maxCorner{inMaxCorner(0), inMaxCorner(1), 0.0}
{
}

void BoundaryBox::update_with_point(const Eigen::Vector3d& vector)
{
    if (this->dim != 3)
        throw std::invalid_argument{"BoundaryBox::update_with_point: Boundary box and updating vector have different dimensions."};
    minCorner = minCorner.cwiseMin(vector);
    maxCorner = maxCorner.cwiseMax(vector);
}

void BoundaryBox::update_with_point(const Eigen::Vector2d& vector)
{
    if (this->dim != 2)
        throw std::invalid_argument{"BoundaryBox::update_with_point: Boundary box and updating vector have different dimensions."};
    for (auto i = 0; i < 2; ++i) {
        minCorner(i) = std::min(minCorner(i), vector(i));
        maxCorner(i) = std::max(maxCorner(i), vector(i));
    }
}

auto Node::is_inside(const BoundaryBox& box, const double relEps) const -> bool
{
    auto isInside = true;
    for (auto i = 0; i < box.dim; ++i) {
        const auto eps = relEps * (box.maxCorner(i) - box.minCorner(i));
        isInside &= (((box.minCorner(i) - eps) <= this->coord(i)) && (this->coord(i) <= (box.maxCorner(i) - eps)));
    }

    return isInside;
}

auto Node::is_coinciding(const Node& other, const double geomEps) const -> bool
{
    return ((this->coord - other.coord).norm() < geomEps);
}

void Element::compute_volume()
{
    const auto& A = this->parent->get_ith_node_coordinates(this->nodeIds[0]);
    const auto& B = this->parent->get_ith_node_coordinates(this->nodeIds[1]);
    const auto& C = this->parent->get_ith_node_coordinates(this->nodeIds[2]);

    this->volume = ((B - A).norm() * (C - A).norm());
}

auto Element::get_centre_of_gravity() const -> Eigen::Vector3d
{
    return 0.25 * (this->parent->get_ith_node_coordinates(this->nodeIds[0]) + this->parent->get_ith_node_coordinates(this->nodeIds[1]) + this->parent->get_ith_node_coordinates(this->nodeIds[2]) + this->parent->get_ith_node_coordinates(this->nodeIds[3]));
}

auto Element::get_superconvergence_coord_and_B() const -> std::pair<Eigen::Vector3d, Eigen::MatrixXd>
{
    const auto& A = this->parent->get_ith_node_coordinates(this->nodeIds[0]);
    const auto& B = this->parent->get_ith_node_coordinates(this->nodeIds[1]);
    const auto& C = this->parent->get_ith_node_coordinates(this->nodeIds[2]);

    const auto hx = (B - A).norm();
    const auto hy = (C - A).norm();

    // clang-format off
    return std::pair{
        this->get_centre_of_gravity(),
        (Eigen::MatrixXd(3, 8) 
            << -1.0 / (2.0 * hx),   0.0,                1.0 / (2.0 * hx),   0.0,               -1.0 / (2.0 * hx),   0.0,               1.0 / (2.0 * hx),  0.0,
                0.0,               -1.0 / (2.0 * hy),   0.0,               -1.0 / (2.0 * hy),   0.0,                1.0 / (2.0 * hy),  0.0,               1.0 / (2.0 * hy),
               -1.0 / (2.0 * hy),  -1.0 / (2.0 * hx),  -1.0 / (2.0 * hy),   1.0 / (2.0 * hx),   1.0 / (2.0 * hy),  -1.0 / (2.0 * hx),  1.0 / (2.0 * hy),  1.0 / (2.0 * hx)
        ).finished()
        };
    // clang-format on
}

Discretisation::Discretisation(const std::filesystem::path& inFilePath)
    : Discretisation::Discretisation{load_JSON_file(inFilePath)}
{
}

Discretisation::Discretisation(const nlohmann::json& inJSON)
{
    auto sw = spdlog::stopwatch{};

    const auto initialGuess = inJSON.contains("constantInitialGuess")
        ? inJSON.at("constantInitialGuess").get<double>()
        : (inJSON.contains("relativeVolumeConstraint")
                  ? inJSON.at("relativeVolumeConstraint").get<double>()
                  : 0.5);

    const auto discretisationType = inJSON.at("discretisationType").get<DiscretisationType>();
    if (discretisationType == DiscretisationType::RegularGrid) {

        if (!(inJSON.contains("domainResolution") && inJSON["domainResolution"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Regular grid discretisation requires domainResolution array entry"};

        const auto resolution = inJSON["domainResolution"].get<std::vector<int>>();
        const auto domainSize = std::invoke(
            [&inJSON, &resolution]() {
                if (inJSON.contains("domainSize")) {
                    const auto tempSize = inJSON["domainSize"].get<std::vector<double>>();
                    if (tempSize.size() != resolution.size())
                        throw std::invalid_argument{"Discretisation::Discretisation: Regular grid discretisation requires domainSize to have the same length as domainResolution"};

                    return tempSize;
                }
                else
                    return std::vector<double>(std::begin(resolution), std::end(resolution));
            });
        const auto elemSize = std::invoke(
            [&resolution, &domainSize]() {
                auto tempSize = std::vector<double>{};
                tempSize.reserve(3);

                std::transform(std::begin(resolution), std::end(resolution),
                    std::begin(domainSize), std::back_inserter(tempSize),
                    [](const auto& r, const auto& s) { return s / r; });

                return tempSize;
            });
        const auto domainOrigin = std::invoke(
            [&inJSON]() {
                if (inJSON.contains("domainOrigin")) {
                    auto temp = Eigen::Vector3d{0.0, 0.0, 0.0};
                    for (auto i = 0; i < std::min(static_cast<size_t>(3), inJSON["domainOrigin"].size()); ++i)
                        temp(i) = inJSON["domainOrigin"].at(i).get<double>();

                    return temp;
                }
                else
                    return Eigen::Vector3d{0.0, 0.0, 0.0};
            });

        if (const auto nDim = resolution.size();
            nDim == 2) {
            // Two-dimensional grid
            for (auto iY = 0; iY <= resolution[1]; ++iY)
                for (auto iX = 0; iX <= resolution[0]; ++iX)
                    this->nodes.emplace_back(domainOrigin(0) + iX * elemSize.at(0), domainOrigin(1) + iY * elemSize.at(1), domainOrigin(2) + 0.0);

            const auto nNodesPerRow = resolution[0] + 1;
            for (auto iY = 0; iY < resolution[1]; ++iY)
                for (auto iX = 0; iX < resolution[0]; ++iX)
                    this->elements.emplace_back(
                        std::array<int, 4>{
                            iY * nNodesPerRow + iX,
                            iY * nNodesPerRow + iX + 1,
                            (iY + 1) * nNodesPerRow + iX,
                            (iY + 1) * nNodesPerRow + iX + 1},
                        this,
                        initialGuess);
        }
        else if (nDim == 3) {
            // Three-dimensional grid
            throw std::runtime_error{"Currently unsupported"};
        }
        else
            throw std::invalid_argument{"Discretisation::Discretisation: Regular grid discretisation requires domainResolution array entry with 2 or 3 members"};
    }
    else if (discretisationType == DiscretisationType::Mesh) {

        if (!(inJSON.contains("nodes") && inJSON["nodes"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Modular discretisation requires nodes array entry"};
        if (!(inJSON.contains("elements") && inJSON["elements"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Modular discretisation requires elements array entry"};

        for (const auto& nodeEntry : inJSON["nodes"])
            this->nodes.emplace_back(nodeEntry.at(0).get<double>(), nodeEntry.at(1).get<double>(), nodeEntry.at(2).get<double>());

        for (const auto& elemEntry : inJSON["elements"]) {
            if (!(elemEntry.contains("type") && elemEntry.contains("matID") && elemEntry.contains("nodes")))
                throw std::invalid_argument{"Discretisation::Discretisation: Each element entry must contain type, matID and nodes entries."};

            this->elements.emplace_back(
                elemEntry["nodes"].get<std::array<int, 4>>(),
                this,
                initialGuess);
        }
    }
    else if (discretisationType == DiscretisationType::ModuleAssembly) {

        if (!(inJSON.contains("assemblyResolution") && inJSON["assemblyResolution"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Modular discretisation requires assemblyResolution array entry"};
        if (!(inJSON.contains("assemblyPlan") && inJSON["assemblyPlan"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Modular discretisation requires assemblyPlan array entry"};
        if (!(inJSON.contains("moduleResolution") && inJSON["moduleResolution"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Modular discretisation requires moduleResolution array entry"};
        if (!(inJSON.contains("moduleSize") && inJSON["moduleSize"].is_array()))
            throw std::invalid_argument{"Discretisation::Discretisation: Modular discretisation requires moduleSize array entry"};

        const auto assemblyResolution = inJSON["assemblyResolution"].get<std::vector<int>>();
        const auto assemblyPlan = inJSON["assemblyPlan"].get<std::vector<int>>();
        const auto moduleResolution = inJSON["moduleResolution"].get<std::vector<int>>();
        const auto moduleSize = inJSON["moduleSize"].get<std::vector<double>>();

        if (const auto nEntries = std::accumulate(std::begin(assemblyResolution), std::end(assemblyResolution), 1, std::multiplies<>());
            nEntries != assemblyPlan.size())
            throw std::invalid_argument{"Discretisation::Discretisation: assemblyResolution entry does not comply with the length of assemblyPlan array"};

        this->modularFlag = true;

        // Identify unique modules (excluding 0 for an empty slot)
        auto tempPlan = assemblyPlan;
        std::sort(std::begin(tempPlan), std::end(tempPlan));
        auto last = std::unique(std::begin(tempPlan), std::end(tempPlan));
        tempPlan.erase(last, std::end(tempPlan));
        if (tempPlan.front() == 0)
            tempPlan.erase(std::begin(tempPlan));

        auto mapDomains = std::vector<std::optional<size_t>>{};
        mapDomains.reserve(assemblyPlan.size());
        std::transform(std::begin(assemblyPlan), std::end(assemblyPlan), std::back_inserter(mapDomains),
            [&tempPlan](const auto& a) -> std::optional<size_t> {
                const auto found = std::find(std::begin(tempPlan), std::end(tempPlan), a);
                if (found == std::end(tempPlan))
                    return std::nullopt;
                else
                    return static_cast<size_t>(std::distance(std::begin(tempPlan), found));
            });
        const auto nNonemptyDomains = std::count_if(std::begin(mapDomains), std::end(mapDomains),
            [](const auto& v) { return v.has_value(); });

        // Create local discretisation
        const auto elemSize = std::array<double, 2>{moduleSize[0] / moduleResolution[0], moduleSize[1] / moduleResolution[1]};
        auto localNodes = std::vector<Node>{};
        auto localElems = std::vector<std::array<int, 4>>{};

        const auto nLocalNodes = (moduleResolution[0] + 1) * (moduleResolution[1] + 1);
        const auto nLocalElems = moduleResolution[0] * moduleResolution[1];

        localNodes.reserve(nLocalNodes);
        for (auto iY = 0; iY <= moduleResolution[1]; ++iY)
            for (auto iX = 0; iX <= moduleResolution[0]; ++iX)
                localNodes.emplace_back(iX * (elemSize.at(0)), iY * elemSize.at(1), 0.0);

        localElems.reserve(nLocalElems);
        const auto nNodesPerRow = moduleResolution[0] + 1;
        for (auto iY = 0; iY < moduleResolution[1]; ++iY)
            for (auto iX = 0; iX < moduleResolution[0]; ++iX)
                localElems.push_back(
                    std::array<int, 4>{
                        iY * nNodesPerRow + iX,
                        iY * nNodesPerRow + iX + 1,
                        (iY + 1) * nNodesPerRow + iX,
                        (iY + 1) * nNodesPerRow + iX + 1});

        auto localBoundary = Boundary{};
        localBoundary.vertices[0] = 0;
        localBoundary.vertices[1] = moduleResolution[0];
        localBoundary.vertices[2] = moduleResolution[1] * (moduleResolution[0] + 1);
        localBoundary.vertices[3] = (moduleResolution[1] + 1) * (moduleResolution[0] + 1) - 1;
        for (auto iX = 1; iX < moduleResolution[0]; ++iX) {
            localBoundary.edges[2].push_back(iX);
            localBoundary.edges[3].push_back(localBoundary.vertices[2].value() + iX);
        }
        for (auto iY = 1; iY < moduleResolution[1]; ++iY) {
            localBoundary.edges[0].push_back(iY * (moduleResolution[0] + 1));
            localBoundary.edges[1].push_back(localBoundary.vertices[1].value() + iY * (moduleResolution[0] + 1));
        }

        this->nodes.reserve(nNonemptyDomains * nLocalNodes);
        this->elements.reserve(nNonemptyDomains * nLocalElems);

        // Loop over assembly plan
        auto nodeCounter = 0;

        auto globalBoundary = std::vector<Boundary>(assemblyPlan.size(), Boundary{});

        for (auto iY = 0; iY < assemblyResolution[1]; ++iY)
            for (auto iX = 0; iX < assemblyResolution[0]; ++iX) {

                // Populate current boundary struct from previously known

                const auto currInd = iY * assemblyResolution[0] + iX;
                const auto prevIndX = iY * assemblyResolution[0] + iX - 1;
                const auto prevIndY = (iY - 1) * assemblyResolution[0] + iX;
                if (iX == 0) {
                    if (iY != 0) {
                        globalBoundary.at(currInd).vertices[0] = globalBoundary.at(prevIndY).vertices[2];
                        globalBoundary.at(currInd).vertices[1] = globalBoundary.at(prevIndY).vertices[3];
                        globalBoundary.at(currInd).edges[2] = globalBoundary.at(prevIndY).edges[3];
                    }
                }
                else {
                    globalBoundary.at(currInd).vertices[0] = globalBoundary.at(prevIndX).vertices[1];
                    globalBoundary.at(currInd).vertices[2] = globalBoundary.at(prevIndX).vertices[3];
                    globalBoundary.at(currInd).edges[0] = globalBoundary.at(prevIndX).edges[1];
                    if (iY != 0) {
                        globalBoundary.at(currInd).vertices[1] = globalBoundary.at(prevIndY).vertices[3];
                        globalBoundary.at(currInd).edges[2] = globalBoundary.at(prevIndY).edges[3];
                    }
                }

                if (assemblyPlan.at(currInd) > 0) {

                    // Remap nodes
                    auto nodeMask = std::vector<bool>(localNodes.size(), true);
                    auto nodeMap = std::vector<int>(localNodes.size(), 0);

                    for (auto iV = 0; iV < 4; ++iV)
                        if (globalBoundary.at(currInd).vertices[iV].has_value()) {
                            nodeMask.at(localBoundary.vertices[iV].value()) = false;
                            nodeMap.at(localBoundary.vertices[iV].value()) = globalBoundary.at(currInd).vertices[iV].value();
                        }

                    for (auto iE = 0; iE < 4; ++iE)
                        if (globalBoundary.at(currInd).edges[iE].size() > 0)
                            for (auto ii = 0; ii < globalBoundary.at(currInd).edges[iE].size(); ++ii) {
                                nodeMask.at(localBoundary.edges[iE].at(ii)) = false;
                                nodeMap.at(localBoundary.edges[iE].at(ii)) = globalBoundary.at(currInd).edges[iE].at(ii);
                            }

                    // Append nodes and elements
                    const auto coordShift = Eigen::Vector3d{iX * moduleSize[0], iY * moduleSize[1], 0.0};
                    for (auto iN = 0; iN < nodeMap.size(); ++iN)
                        if (nodeMask[iN]) {
                            this->nodes.emplace_back(localNodes.at(iN).coord + coordShift);
                            nodeMap[iN] = nodeCounter++;
                        }

                    const auto moduleId = mapDomains.at(currInd).value();
                    for (auto iE = 0; iE < localElems.size(); ++iE) {
                        this->elements.emplace_back(
                            std::array<int, 4>{
                                nodeMap.at(localElems.at(iE)[0]),
                                nodeMap.at(localElems.at(iE)[1]),
                                nodeMap.at(localElems.at(iE)[2]),
                                nodeMap.at(localElems.at(iE)[3])},
                            this,
                            initialGuess);
                        this->subdomainElementMapping.push_back({moduleId, static_cast<size_t>(iE), static_cast<size_t>(currInd)});
                    }

                    // Update boundary map
                    for (auto iV = 0; iV < 4; ++iV)
                        if (localBoundary.vertices[iV].has_value())
                            if (nodeMask.at(localBoundary.vertices[iV].value()))
                                globalBoundary.at(currInd).vertices[iV] = nodeMap.at(localBoundary.vertices[iV].value());
                    for (auto iE = 0; iE < 4; ++iE)
                        if (localBoundary.edges[iE].size() > 0)
                            if (nodeMask.at(localBoundary.edges[iE].front()))
                                for (auto ii = 0; ii < localBoundary.edges[iE].size(); ++ii)
                                    if (nodeMask.at(localBoundary.edges[iE].at(ii)))
                                        globalBoundary.at(currInd).edges[iE].push_back(nodeMap.at(localBoundary.edges[iE].at(ii)));
                }
            }
    }
    else
        throw std::invalid_argument{"Discretisation::Discretisation: Unsupported value of discretisationType entry."};

    if (inJSON.contains("alwaysFilledElems")) {
        const auto fixedElemsIds = inJSON["alwaysFilledElems"].get<std::vector<size_t>>();
        for (auto& eId : fixedElemsIds)
            this->elements.at(eId).set_fixed_density(1.0);
    }
    if (inJSON.contains("alwaysEmptyElems")) {
        const auto fixedElemsIds = inJSON["alwaysEmptyElems"].get<std::vector<size_t>>();
        for (auto& eId : fixedElemsIds)
            this->elements.at(eId).set_fixed_density(0.0);
    }

    if (inJSON.contains("additionalDiscretisation")) {
        const auto additionalDiscretisation = Discretisation{inJSON["additionalDiscretisation"]};
        this->merge_discretisation(additionalDiscretisation);
    }

    // Enumerate free (non-fixed elements)
    this->enumerate_free_elements();

    if (PRINT_DETAILED_TIMING) {
        auto logger = spdlog::get("MTO.Logger");
        if (logger != nullptr)
            logger->info("\t\t\tDiscretisation was constructed in {} s", sw);
    }
}

auto Discretisation::get_number_of_free_elements() const -> size_t
{
    return std::count_if(std::begin(this->elements), std::end(this->elements),
        [](const auto& e) { return !e.is_fixed(); });
}

void Discretisation::merge_discretisation(const Discretisation& addedDiscretisation, const double geomEps)
{
    auto nodesInCurrentDiscretisation = std::vector<std::optional<size_t>>{};
    nodesInCurrentDiscretisation.reserve(addedDiscretisation.get_number_of_nodes());
    for (const auto& nA : addedDiscretisation.nodes) {
        auto coincidingNodes = std::vector<size_t>{};
        for (auto i = size_t{0}; i < this->nodes.size(); ++i)
            if (this->nodes[i].is_coinciding(nA, geomEps))
                coincidingNodes.push_back(i);

        if (const auto nCoincidingNodes = coincidingNodes.size();
            nCoincidingNodes == 0)
            nodesInCurrentDiscretisation.push_back(std::nullopt);
        else if (nCoincidingNodes == 1)
            nodesInCurrentDiscretisation.push_back(coincidingNodes[0]);
        else
            throw std::logic_error{"Discretisation::merge_discretisation: Multiple nodes coindcide with a node from added discretisation."};
    }

    auto renumMap = std::vector<size_t>{};
    renumMap.reserve(nodesInCurrentDiscretisation.size());
    auto counter = size_t{this->get_number_of_nodes()};
    for (auto i = size_t{0}; i < addedDiscretisation.get_number_of_nodes(); ++i)
        if (const auto& n = nodesInCurrentDiscretisation.at(i);
            n.has_value())
            renumMap.push_back(n.value());
        else {
            renumMap.push_back(counter++);
            this->nodes.push_back(addedDiscretisation.nodes.at(i));
        }

    for (const auto& e : addedDiscretisation.elements) {
        auto newNodes = std::array<int, 4>{};
        std::transform(std::begin(e.nodeIds), std::end(e.nodeIds), std::begin(newNodes),
            [&renumMap](const auto i) { return renumMap.at(i); });

        this->elements.emplace_back(newNodes, this, e.density, e.is_fixed());
    }

    this->enumerate_free_elements();
}

auto Discretisation::get_element_whole_volumes() const -> std::vector<double>
{
    auto elementVolumes = std::vector<double>{};
    elementVolumes.reserve(this->get_number_of_elements());
    std::transform(std::begin(this->elements), std::end(this->elements), std::back_inserter(elementVolumes),
        [](const auto& e) { return e.get_whole_volume(); });

    return elementVolumes;
}

auto Discretisation::get_element_centres_of_gravity() const -> std::vector<Eigen::Vector3d>
{
    auto centres = std::vector<Eigen::Vector3d>{};
    centres.reserve(this->get_number_of_elements());
    std::transform(std::begin(this->elements), std::end(this->elements), std::back_inserter(centres),
        [](const auto& e) { return e.get_centre_of_gravity(); });

    return centres;
}

auto Discretisation::get_node_element_adjacency() const -> std::vector<std::vector<size_t>>
{
    const auto nNodes = this->get_number_of_nodes();
    const auto nElems = this->get_number_of_elements();

    auto adjacency = std::vector<std::vector<size_t>>(nNodes);
    for (auto iE = size_t{0}; iE < nElems; ++iE) {
        const auto& e = this->elements.at(iE);
        for (const auto nId : e.get_node_ids())
            adjacency.at(nId).push_back(iE);
    }

    return adjacency;
}

auto Discretisation::get_adjacent_free_elements_within_given_radius(const double radius) const -> std::vector<std::vector<IndexDistancePair>>
{
    const auto nNodes = this->get_number_of_nodes();
    const auto nElems = this->get_number_of_elements();

    const auto elementCentres = this->get_element_centres_of_gravity();
    const auto node2elemsAdjacency = this->get_node_element_adjacency();

    auto output = std::vector<std::vector<IndexDistancePair>>{};
    output.reserve(nElems);
    for (auto iE = size_t{0}; iE < nElems; ++iE) {
        if (this->elements.at(iE).is_fixed())
            output.emplace_back();
        else {
            auto unusedNodes = std::vector<bool>(nNodes, true);
            auto unusedElements = std::vector<bool>(nElems, true);
            unusedElements.at(iE) = false;

            auto relatedElements = std::vector<IndexDistancePair>{};
            relatedElements.push_back({iE, 0.0});

            // Find connected elements below radius
            auto proceed = true;
            auto nextElemIds = std::vector<size_t>{iE};
            while (proceed) {
                auto currentElemIds = nextElemIds;
                nextElemIds.clear();

                auto nextNodeIds = std::vector<size_t>{};
                for (const auto& eId : currentElemIds)
                    for (const auto nId : this->elements.at(eId).nodeIds)
                        if (unusedNodes.at(nId)) {
                            nextNodeIds.push_back(nId);
                            unusedNodes.at(nId) = false;
                        }

                for (const auto& nId : nextNodeIds)
                    for (const auto& eId : node2elemsAdjacency.at(nId))
                        if (unusedElements.at(eId) && !this->elements.at(eId).is_fixed()) {
                            nextElemIds.push_back(eId);
                            unusedElements.at(eId) = false;
                        }

                proceed = false;
                for (const auto& eId : nextElemIds)
                    if (const auto d = (elementCentres.at(eId) - elementCentres.at(iE)).norm();
                        (d - radius) < 0) {
                        relatedElements.push_back({eId, d});
                        proceed = true;
                    }
            }

            output.emplace_back(std::move(relatedElements));
        }
    }

    return output;
}

void Discretisation::update_element_densities(double newConstantDensity)
{
    this->update_element_densities(Eigen::VectorXd::Constant(this->get_number_of_free_elements(), newConstantDensity));
}

void Discretisation::update_element_densities(const Eigen::VectorXd& newDensities)
{
    if (static_cast<size_t>(newDensities.size()) != this->get_number_of_free_elements())
        throw std::invalid_argument{"Discretisation::update_element_densities: The length of the input vector must correspond to the number of free elements in the discretisation."};

    auto i = 0;
    for (auto& e : this->elements)
        if (!e.is_fixed())
            e.set_density(newDensities(i++));
}

auto Discretisation::get_renum_map() const -> std::vector<size_t>
{
    const auto nElems = this->get_number_of_elements();

    auto renumMap = std::vector<size_t>(nElems, -1);
    auto counter = size_t{0};
    for (auto iE = size_t{0}; iE < nElems; ++iE)
        if (!this->elements.at(iE).is_fixed())
            renumMap.at(iE) = counter++;

    return renumMap;
}

auto Discretisation::get_whole_volume() const -> double
{
    if (!this->volume.has_value())
        this->volume = std::accumulate(
            std::begin(this->elements), std::end(this->elements), 0.0,
            [](const auto& partSum, const auto& elem) { return partSum + elem.get_whole_volume(); });

    return this->volume.value();
}

auto Discretisation::get_current_volume() const -> double
{
    return std::accumulate(
        std::begin(this->elements), std::end(this->elements), 0.0,
        [](const auto& partSum, const auto& elem) { return partSum + elem.get_current_volume(); });
}

auto Discretisation::get_relative_current_volume() const -> double
{
    return this->get_current_volume() / this->get_whole_volume();
}

auto Discretisation::get_whole_free_volume() const -> double
{
    if (!this->freeVolume.has_value())
        this->freeVolume = std::accumulate(
            std::begin(this->elements), std::end(this->elements), 0.0,
            [](const auto& partSum, const auto& elem) {
                return partSum + (elem.is_fixed() ? 0.0 : elem.get_whole_volume());
            });

    return this->freeVolume.value();
}

auto Discretisation::get_current_free_volume() const -> double
{
    return std::accumulate(
        std::begin(this->elements), std::end(this->elements), 0.0,
        [](const auto& partSum, const auto& elem) {
            return partSum + (elem.is_fixed() ? 0.0 : elem.get_current_volume());
        });
}

auto Discretisation::get_relative_current_free_volume() const -> double
{
    return this->get_current_free_volume() / this->get_whole_free_volume();
}

auto Discretisation::get_relative_volume_sensitivity() const -> LinearApproximation
{
    const auto nFreeElems = this->get_number_of_free_elements();
    const auto wholeFreeVolume = this->get_whole_free_volume();

    auto gradient = Eigen::VectorXd{Eigen::VectorXd::Constant(nFreeElems, 0.0)};
    auto iE = 0;
    for (const auto& e : this->elements)
        if (!e.is_fixed()) {
            gradient(iE) = e.get_whole_volume() / wholeFreeVolume;
            iE++;
        }

    return LinearApproximation{this->get_relative_current_free_volume(), gradient, LinearApproximationType::Volume};
}

auto Discretisation::print_data_for_VTK(const bool useBinary) const -> std::string
{
    auto out = std::string{};

    if (useBinary) {
        const auto nNodes = this->get_number_of_nodes();
        const auto nElems = this->get_number_of_elements();

        auto nodalCoords = std::vector<float>{};
        nodalCoords.reserve(3 * nNodes);

        for (const auto& n : this->nodes)
            nodalCoords.insert(nodalCoords.end(),
                {static_cast<float>(n.coord(0)), static_cast<float>(n.coord(1)), float{0.0}});

        auto elemTypes = std::vector<uint8_t>{};
        elemTypes.reserve(nElems);
        auto elemConnectivity = std::vector<int32_t>{};
        elemConnectivity.reserve(nElems * 4);
        auto elemConnectivityOffset = std::vector<int32_t>{};
        elemConnectivityOffset.reserve(nElems);

        auto cummulOffset = int32_t{0};
        for (const auto& e : this->elements) {
            const auto& nodeIds = e.get_node_ids();
            std::transform(std::begin(nodeIds), std::end(nodeIds), std::back_inserter(elemConnectivity),
                [](const auto& i) { return static_cast<int32_t>(i); });

            cummulOffset += nodeIds.size();
            elemConnectivityOffset.push_back(cummulOffset);

            elemTypes.emplace_back(static_cast<uint8_t>(e.get_element_type()));
        }

        out.reserve(400 +
            static_cast<size_t>(std::ceil(nodalCoords.size() * sizeof(float) * 8 / 6)) +
            static_cast<size_t>(std::ceil(elemConnectivity.size() * sizeof(int32_t) * 8 / 6)) +
            static_cast<size_t>(std::ceil(elemConnectivityOffset.size() * sizeof(int32_t) * 8 / 6)) +
            static_cast<size_t>(std::ceil(elemTypes.size() * sizeof(uint8_t) * 8 / 6)));

        fmt::format_to(std::back_inserter(out), "\t\t\t<Points>\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"binary\">\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t\t\t{}",
            base64::encode_vector_into_base64_string_with_length(nodalCoords));
        fmt::format_to(std::back_inserter(out), "\n\t\t\t\t</DataArray>\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t</Points>\n");

        fmt::format_to(std::back_inserter(out), "\t\t\t<Cells>\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Int32\" Name=\"connectivity\" format=\"binary\">\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t\t\t{}",
            base64::encode_vector_into_base64_string_with_length(elemConnectivity));
        fmt::format_to(std::back_inserter(out), "\n\t\t\t\t</DataArray>\n");

        fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Int32\" Name=\"offsets\" format=\"binary\">\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t\t\t{}",
            base64::encode_vector_into_base64_string_with_length(elemConnectivityOffset));
        fmt::format_to(std::back_inserter(out), "\n\t\t\t\t</DataArray>\n");

        fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"UInt8\" Name=\"types\" format=\"binary\">\n");
        fmt::format_to(std::back_inserter(out), "\t\t\t\t\t{}",
            base64::encode_vector_into_base64_string_with_length(elemTypes));
        fmt::format_to(std::back_inserter(out), "\n\t\t\t\t</DataArray>\n");

        fmt::format_to(std::back_inserter(out), "\t\t\t</Cells>\n");
    }
    else {
        // Write nodal data
        out.append("POINTS " + std::to_string(this->get_number_of_nodes()) + " float\n");
        for (const auto& n : this->nodes)
            out.append(double_to_formated_string(n.coord(0)) + ' ' + double_to_formated_string(n.coord(1)) + ' ' + double_to_formated_string(n.coord(2)) + '\n');
        out.append("\n");

        // Write cell data
        const auto nEntries = std::accumulate(std::begin(this->elements), std::end(this->elements),
            0, [](const auto a, const auto& b) { return a + (b.get_number_of_nodes() + 1); });
        out.append("CELLS " + std::to_string(this->get_number_of_elements()) + ' ' + std::to_string(nEntries) + '\n');
        for (const auto& e : this->elements) {
            out.append(std::to_string(e.get_number_of_nodes()));
            for (const auto& nid : e.nodeIds)
                out.append(' ' + std::to_string(nid));
            out.append("\n");
        }
        out.append("\n");

        out.append("CELL_TYPES " + std::to_string(this->get_number_of_elements()) + '\n');
        for (const auto& e : this->elements)
            out.append(std::to_string(e.get_element_type()) + '\n');
        out.append("\n");
    }

    return out;
}

auto Discretisation::print_data_for_SVG(double multiplier) const -> std::string
{
    auto out = std::string{};
    auto order = std::array<int, 4>{0, 1, 3, 2};    // todo: we need convex order! Check for other elements

    for (const auto& e : this->elements) {

        out.append("<polygon points=\"");
        for (const auto& o : order)
            out.append(double_to_formated_string(this->nodes[e.nodeIds[o]].coord(0) * multiplier) + ',' + double_to_formated_string(this->nodes[e.nodeIds[o]].coord(1) * multiplier) + ' ');
        auto dens = double_to_formated_string(255.0 - (255.0 * e.get_density()));
        out.append("\" fill=\"rgb(" + dens + "," + dens + "," + dens + ")\"/>\n");
    }

    return out;
}

auto Discretisation::print_modules_data_for_SVG(double multiplier) const -> std::pair<std::vector<std::string>, std::vector<BoundaryBox>>
{
    auto nModules = 0U;
    for (const auto& ee : this->subdomainElementMapping)
        if (ee.moduleId > nModules)
            nModules = ee.moduleId;
    nModules++;

    auto out_modular = std::vector<std::string>(nModules);
    auto out_bbox = std::vector<BoundaryBox>(nModules);
    auto subdomainIds = std::vector<int>(nModules, -1);
    auto order = std::array<int, 4>{0, 1, 3, 2};    // todo: we need convex order! Check for other elements

    auto eId = 0U;
    for (const auto& ee : this->subdomainElementMapping) {
        auto mId = ee.moduleId;
        auto sId = ee.subdomainId;

        if (subdomainIds[mId] == -1) {
            subdomainIds[mId] = sId;
            auto n = this->nodes[this->elements[eId].nodeIds[0]].coord;
        }

        if (subdomainIds[mId] == sId) {
            auto e = this->elements[eId];
            out_modular[mId].append("<polygon points=\"");
            for (const auto& o : order) {
                out_bbox[mId].update_with_point(this->nodes[e.nodeIds[o]].coord);
                auto str1 = double_to_formated_string(this->nodes[e.nodeIds[o]].coord(0) * multiplier);
                auto str2 = double_to_formated_string(this->nodes[e.nodeIds[o]].coord(1) * multiplier);
                out_modular[mId].append(str1 + ',' + str2 + ' ');
            }
            auto dens = double_to_formated_string(255.0 - (255.0 * e.get_density()));
            out_modular[mId].append("\" fill=\"rgb(" + dens + "," + dens + "," + dens + ")\"/>\n");
        }
        eId++;
    }
    for (auto i = 0; i < nModules; ++i) {
        auto tx = double_to_formated_string(-out_bbox[i].minCorner(0) * multiplier);
        auto ty = double_to_formated_string(-out_bbox[i].maxCorner(1) * multiplier);    // we must flip
        out_modular[i] = "<g transform=\"scale(1,-1) translate(" + tx + "," + ty + ")\">\n" + out_modular[i] + "</g>\n";
    }

    return {out_modular, out_bbox};
}

auto Discretisation::get_node_ids_inside_box(const BoundaryBox& box, double relEps) const -> std::vector<size_t>
{
    auto nodeIds = std::vector<size_t>{};

    const auto nNodes = this->get_number_of_nodes();
    for (auto iN = size_t{0}; iN < nNodes; ++iN)
        if (this->nodes.at(iN).is_inside(box, relEps))
            nodeIds.push_back(iN);

    return nodeIds;
}

auto Discretisation::get_bounding_box() const -> BoundaryBox
{
    auto bbox = BoundaryBox(this->nodes[0].coord, this->nodes[0].coord);
    for (const auto& n : this->nodes)
        bbox.update_with_point(n.coord);
    return bbox;
}

void Discretisation::enumerate_free_elements()
{
    auto freeElemCounter = 0;
    for (auto& e : this->elements)
        if (!e.is_fixed())
            e.freeIndex = freeElemCounter++;
}

}    // namespace mto