#include "OptimizerMMA.h"

#include <algorithm>
#include <execution>
#include <functional>
#include <iostream>
#include <numeric>
#include <stdexcept>

namespace mto {

std::string vector_as_string(const Eigen::VectorXd& v)
{
    auto o = std::string{};
    o += '[';
    for (auto i = 0; i < v.size(); ++i) {
        o += ' ' + std::to_string(v(i));
        if (i < (v.size() - 1))
            o += ',';
    }
    o += " ]";
    return o;
}

OptimizerMMA::SolverState::SolverState(Eigen::VectorXd inX, Eigen::VectorXd inY, double inZ, Eigen::VectorXd inLambda,
    Eigen::VectorXd inKsi, Eigen::VectorXd inEta, Eigen::VectorXd inMu, double inZeta, Eigen::VectorXd inS)
    : x{std::move(inX)},
      y{std::move(inY)},
      z{inZ},
      lambda{std::move(inLambda)},
      ksi{std::move(inKsi)},
      eta{std::move(inEta)},
      mu{std::move(inMu)},
      zeta{inZeta},
      s{std::move(inS)} {}

auto OptimizerMMA::SolverState::operator+=(const SolverState& rhs) -> OptimizerMMA::SolverState&
{
    this->x += rhs.x;
    this->y += rhs.y;
    this->z += rhs.z;
    this->lambda += rhs.lambda;
    this->ksi += rhs.ksi;
    this->eta += rhs.eta;
    this->mu += rhs.mu;
    this->zeta += rhs.zeta;
    this->s += rhs.s;

    return *this;
}

auto OptimizerMMA::SolverState::operator-=(const SolverState& rhs) -> OptimizerMMA::SolverState&
{
    this->x -= rhs.x;
    this->y -= rhs.y;
    this->z -= rhs.z;
    this->lambda -= rhs.lambda;
    this->ksi -= rhs.ksi;
    this->eta -= rhs.eta;
    this->mu -= rhs.mu;
    this->zeta -= rhs.zeta;
    this->s -= rhs.s;

    return *this;
}

auto OptimizerMMA::SolverState::operator*=(const double coeff) -> OptimizerMMA::SolverState&
{
    this->x *= coeff;
    this->y *= coeff;
    this->z *= coeff;
    this->lambda *= coeff;
    this->ksi *= coeff;
    this->eta *= coeff;
    this->mu *= coeff;
    this->zeta *= coeff;
    this->s *= coeff;

    return *this;
}

OptimizerMMA::OptimizerMMA(
    size_t nVars, size_t nConstraints, Eigen::VectorXd inXmin, Eigen::VectorXd inXmax,
    double inA0, Eigen::VectorXd inA, Eigen::VectorXd inC, Eigen::VectorXd inD)
    : N{nVars},
      M{nConstraints},
      xMin{std::move(inXmin)},
      xMax{std::move(inXmax)},
      a0{inA0},
      a{std::move(inA)},
      c{std::move(inC)},
      d{std::move(inD)},
      L{nVars},
      U{nVars},
      alpha{nVars},
      beta{nVars},
      b{nConstraints},
      p0{nVars},
      q0{nVars},
      P{nVars, nConstraints},
      Q{nVars, nConstraints},
      xMinus1{nVars},
      xMinus2{nVars} {}

OptimizerMMA::OptimizerMMA(
    size_t nVars, size_t nConstraints, double xMinScal, double xMaxScal,
    double ina0, double aScal, double cScal, double dScal)
    : OptimizerMMA(
          nVars, nConstraints, Eigen::VectorXd::Constant(nVars, xMinScal), Eigen::VectorXd::Constant(nVars, xMaxScal),
          ina0, Eigen::VectorXd::Constant(nConstraints, aScal), Eigen::VectorXd::Constant(nConstraints, cScal), Eigen::VectorXd::Constant(nConstraints, dScal)) {}

auto OptimizerMMA::clone() -> std::unique_ptr<OptimizerInterface>
{
    return std::make_unique<OptimizerMMA>(*this);
}

auto OptimizerMMA::update(const Eigen::VectorXd& x, const LinearApproximation& objective, const std::vector<LinearApproximation>& constraints) -> Eigen::VectorXd
{
    ++this->nRuns;

    this->update_asymptotes(x);
    this->update_alphas_betas(x);

    this->construct_subproblem(x, objective, constraints);

    // this->print_subproblem_info();

    auto state = this->solve_subproblem();

    this->update_previously_stored_states(state.x);

    return state.x;
}

void OptimizerMMA::update_asymptotes(const Eigen::VectorXd& x)
{
    // Update asymptotes U and L
    if (this->nRuns < 3) {
        this->L = x - this->parameters.sInit * (this->xMax - this->xMin);
        this->U = x + this->parameters.sInit * (this->xMax - this->xMin);
    }
    else {
        const auto xMinMaxDiff = (this->xMax - this->xMin).cwiseMax(this->parameters.xMinMaxDiffThreshold).eval();
        const auto maxMove = (this->parameters.maxRelAsymptoteWidth * xMinMaxDiff).eval();
        const auto minMove = (this->parameters.minRelAsymptoteWidth * xMinMaxDiff).eval();

        for (auto i = 0; i < x.size(); ++i) {
            const auto sCurrent = std::invoke(
                [&i, &x, this]() {
                    if (const auto signedDifferences = (this->xMinus1[i] - x[i]) * (this->xMinus2[i] - this->xMinus1[i]);
                        signedDifferences > 0.0)
                        return this->parameters.sIncrease;
                    else if (signedDifferences < 0.0)
                        return this->parameters.sDecrease;
                    else
                        return 1.0;
                });

            this->L[i] = x[i] - std::max(std::min(sCurrent * (this->xMinus1[i] - this->L[i]), maxMove(i)), minMove(i));
            this->U[i] = x[i] + std::max(std::min(sCurrent * (this->U[i] - this->xMinus1[i]), maxMove(i)), minMove(i));
        }
    }
}

void OptimizerMMA::update_alphas_betas(const Eigen::VectorXd& x)
{
    this->alpha = (this->L + this->parameters.alphaBetaCoeff * (x - this->L))
                      .cwiseMax(this->xMin)
                      .cwiseMax(x - this->parameters.alphaBetaMove * (this->xMax - this->xMin))
                      .cwiseMin(this->xMax);
    this->beta = (this->U + this->parameters.alphaBetaCoeff * (x - this->U))
                     .cwiseMin(this->xMax)
                     .cwiseMin(x + this->parameters.alphaBetaMove * (this->xMax - this->xMin))
                     .cwiseMax(this->xMin);
}

void OptimizerMMA::construct_subproblem(const Eigen::VectorXd& x, const LinearApproximation& objective, const std::vector<LinearApproximation>& constraints)
{
    auto Ux = (this->U - x).eval();
    auto xL = (x - this->L).eval();

    std::tie(this->p0, this->q0) = this->construct_p_and_q_coeffs(objective.funGradient, Ux, xL);

    for (auto j = size_t{0}; j < this->M; ++j) {

        const auto [Pcol, Qcol] = this->construct_p_and_q_coeffs(constraints.at(j).funGradient, Ux, xL);
        this->P.col(j) = Pcol;
        this->Q.col(j) = Qcol;

        this->b(j) = -constraints.at(j).funValue +
            this->P.col(j).dot(Ux.cwiseInverse()) +
            this->Q.col(j).dot(xL.cwiseInverse());
    }
}

auto OptimizerMMA::construct_p_and_q_coeffs(const Eigen::VectorXd& dfdx, const Eigen::VectorXd& Ux, const Eigen::VectorXd& xL) const -> std::pair<Eigen::VectorXd, Eigen::VectorXd>
{
    auto p = Eigen::VectorXd{this->N};
    p = this->parameters.addedAbsGradientCoeff * dfdx.cwiseAbs() +
        this->parameters.addedXdiffInverseCoeff * (this->xMax - this->xMin).cwiseMax(this->parameters.xMinMaxDiffThreshold).cwiseInverse();
    auto q = Eigen::VectorXd{p};

    p += dfdx.cwiseMax(0.0);
    q -= dfdx.cwiseMin(0.0);

    p = p.cwiseProduct(Ux.cwiseProduct(Ux));
    q = q.cwiseProduct(xL.cwiseProduct(xL));

    return std::make_pair(p, q);
}

void OptimizerMMA::print_subproblem_info() const
{
    std::cout << "MMA state:\n";
    std::cout << "L = " << vector_as_string(this->L) << '\n';
    std::cout << "U = " << vector_as_string(this->U) << '\n';
    std::cout << "α = " << vector_as_string(this->alpha) << '\n';
    std::cout << "β = " << vector_as_string(this->beta) << '\n';
    std::cout << '\n';
    std::cout << "p0 = " << vector_as_string(this->p0) << '\n';
    std::cout << "q0 = " << vector_as_string(this->q0) << '\n';
    std::cout << '\n';
    std::cout << "b = " << vector_as_string(this->b) << '\n';
    std::cout << "P = \n"
              << this->P << '\n';
    std::cout << "Q = \n"
              << this->Q << '\n';
    std::cout << std::endl;
}

auto OptimizerMMA::solve_subproblem() const -> SolverState
{
    auto state = this->initialize_solver_state();
    auto epsilon = this->parameters.initEpsilonValue;

    // std::cout << state << std::endl;

    auto iterate = true;
    auto nIter = 0;
    while (iterate) {
        ++nIter;
        if (nIter > this->parameters.nMaxIterations)
            throw std::range_error{"OptimizerMMA::solve_subproblem(): Reached maximum number of iterations without converging."};

        const auto direction = this->compute_Newton_direction(state, epsilon);
        const auto stepLength = this->compute_line_search_length(state, direction, epsilon);

        state += stepLength * direction;
        const auto residuum = this->compute_relaxed_subproblem_residuum(state, epsilon);

        // std::cout << "Iteration no. " << nIter << '\n';
        // std::cout << "\tstep length = " << stepLength << '\n';
        // std::cout << state << std::endl;
        // std::cout << vector_as_string(residuum) << std::endl;
        // std::cout << residuum.lpNorm<Eigen::Infinity>() << std::endl;

        if (residuum.lpNorm<Eigen::Infinity>() < this->parameters.epsilonReductionThreshold * epsilon) {
            if (epsilon < this->parameters.epsilonConvergenceLimit)
                break;
            else
                epsilon *= this->parameters.epsilonReductionCoeff;
        }
    }

    return state;
}

auto OptimizerMMA::initialize_solver_state() const -> SolverState
{
    const auto x = 0.5 * (this->alpha + this->beta);

    return SolverState{
        x,
        Eigen::VectorXd{this->M}.setConstant(1.0),
        1.0,
        Eigen::VectorXd{this->M}.setConstant(1.0),
        (x - this->alpha).cwiseInverse().cwiseMax(1.0),
        (this->beta - x).cwiseInverse().cwiseMax(1.0),
        (0.5 * this->c).cwiseMax(1.0),
        1.0,
        Eigen::VectorXd{this->M}.setConstant(1.0)};
}

auto OptimizerMMA::compute_relaxed_subproblem_residuum(const SolverState& state, const double epsilon) const -> Eigen::VectorXd
{
    // clang-format off
    return (Eigen::VectorXd{3 * this->N + 4 * this->M + 2}
        << this->compute_dpsidx(state) - state.ksi + state.eta,
           this->c + this->d.cwiseProduct(state.y) - state.lambda - state.mu,
           this->a0 - state.zeta - this->a.dot(state.lambda),
           this->compute_gi(state) - state.z * this->a - state.y + state.s - this->b,
           state.ksi.cwiseProduct(state.x - this->alpha).array() - epsilon,
           state.eta.cwiseProduct(this->beta - state.x).array() - epsilon,
           state.mu.cwiseProduct(state.y).array() - epsilon,
           state.zeta * state.z - epsilon,
           state.lambda.cwiseProduct(state.s).array() - epsilon)
        .finished();
    // clang-format on
}

auto OptimizerMMA::compute_Newton_direction(const SolverState& state, const double epsilon) const -> SolverState
{
    // clang-format off
    const auto Ux = (this->U - state.x).eval();
    const auto xL = (state.x - this->L).eval();

    const auto psi = (2.0 * (this->p0 + this->P * state.lambda).cwiseQuotient(Ux.cwiseProduct(Ux.cwiseProduct(Ux))) 
                    + 2.0 * (this->q0 + this->Q * state.lambda).cwiseQuotient(xL.cwiseProduct(xL.cwiseProduct(xL))))
        .eval();
    const auto G = (Ux.cwiseAbs2().cwiseInverse().asDiagonal() * this->P 
                  - xL.cwiseAbs2().cwiseInverse().asDiagonal() * this->Q)
        .transpose().eval();

    // Common matrices from RHS of the Newton system in Eq. (5.14) with explicit inversion expression when useful
    const auto DyInv = (this->d + state.mu.cwiseQuotient(state.y))
        .cwiseInverse()
        .asDiagonal();
    const auto Dl = (state.s.cwiseQuotient(state.lambda)).asDiagonal();
    const auto Dly = Dl + DyInv;

    // RHS of the Newton system in Eq. (5.14) with `tdx` denoting $\tilde{\delta}_{x}$ etc.
    const auto tdx = (this->compute_dpsidx(state) 
        - epsilon * (state.x - this->alpha).cwiseInverse() 
        + epsilon * (this->beta - state.x).cwiseInverse())
        .eval();
    const auto tdy = (this->c + this->d.cwiseProduct(state.y) - state.lambda - epsilon * state.y.cwiseInverse()).eval();
    const auto tdz = this->a0 - this->a.dot(state.lambda) - epsilon / state.z;
    const auto tdl = (this->compute_gi(state) 
        - state.z * this->a - state.y - this->b 
        + epsilon * state.lambda.cwiseInverse())
        .eval();
    const auto tdly = (tdl + DyInv * tdy).eval();

    // Construct and solve the most suitable Newton system
    auto dX = Eigen::VectorXd{this->N};
    auto dZ = 0.0;
    auto dL = Eigen::VectorXd{this->M};

    if (this->N > this->M) {
        const auto DxInv = (psi
            + state.ksi.cwiseQuotient(state.x - this->alpha)
            + state.eta.cwiseQuotient(this->beta - state.x)).cwiseInverse().asDiagonal();

        const auto A = (Eigen::MatrixXd{this->M + 1, this->M + 1}
            << Dly.toDenseMatrix() + G * DxInv * G.transpose(), this->a,
               this->a.transpose(), -state.zeta / state.z)
            .finished();
        const auto RHS = (Eigen::VectorXd{this->M + 1} << tdly - G * DxInv * tdx, tdz).finished();        
        const auto foo = Eigen::VectorXd{ A.householderQr().solve(RHS) };

        dL = foo.head(this->M);
        dZ = foo(this->M);
        dX = -1.0 * (DxInv * G.transpose() * dL) - DxInv * tdx;
    }
    else {
        const auto Dx = (psi
            + (state.ksi.cwiseQuotient(state.x - this->alpha)) 
            + (state.eta.cwiseQuotient(this->beta - state.x))).asDiagonal();
        const auto DlyInv = Dly.diagonal().cwiseInverse().asDiagonal();

        const auto A = (Eigen::MatrixXd{this->N + 1, this->N + 1}
            << Dx.toDenseMatrix() + G.transpose() * DlyInv * G, -G.transpose() * DlyInv * this->a
               -this->a.transpose() * DlyInv * G, state.zeta / state.z + this->a.transpose() * DlyInv * this->a)
            .finished();
        const auto RHS = (Eigen::VectorXd{this->N + 1} 
            << -tdx - G.transpose() * DlyInv * tdly, 
               -tdz + this->a.transpose() * DlyInv * tdly)
            .finished();
        const auto foo = Eigen::VectorXd{ A.householderQr().solve(RHS) };

        dX = foo.head(this->N);
        dZ = foo(this->N);
        dL = DlyInv * G * dX - DlyInv * this->a * dZ + DlyInv * tdly;
    }
    // clang-format on

    // Extract solution and back-substitute
    const auto dY = (DyInv * dL - DyInv * tdy).eval();
    const auto dKsi = (-dX.cwiseProduct(state.ksi).cwiseQuotient(state.x - this->alpha) - state.ksi + epsilon * (state.x - this->alpha).cwiseInverse()).eval();
    const auto dEta = (dX.cwiseProduct(state.eta).cwiseQuotient(this->beta - state.x) - state.eta + epsilon * (this->beta - state.x).cwiseInverse()).eval();
    const auto dMu = (-dY.cwiseProduct(state.mu).cwiseQuotient(state.y) - state.mu + epsilon * state.y.cwiseInverse()).eval();
    const auto dZeta = -state.zeta / state.z * dZ - state.zeta + epsilon / state.z;
    const auto dS = (-state.s.cwiseProduct(dL).cwiseQuotient(state.lambda) - state.s + epsilon * state.lambda.cwiseInverse()).eval();

    return SolverState{
        std::move(dX),
        std::move(dY),
        dZ,
        std::move(dL),
        std::move(dKsi),
        std::move(dEta),
        std::move(dMu),
        dZeta,
        std::move(dS)};
}

auto OptimizerMMA::compute_line_search_length(const SolverState& state, const SolverState& direction, const double epsilon) const -> double
{
    // Initial steplength t (from 0.0 to 1.0) such that the inequality constraints are satisfied
    // (computed as inverse in order to avoid troubles with negative values of t)
    const auto invCoeff = 1.0 / (this->parameters.strictIneqCoeff - 1.0);
    const auto maxInvStepLengths = Eigen::Vector<double, 9>{
        (invCoeff * direction.x.cwiseQuotient(state.x - this->alpha)).maxCoeff(),
        (invCoeff * direction.x.cwiseQuotient(state.x - this->beta)).maxCoeff(),
        (invCoeff * direction.y.cwiseQuotient(state.y)).maxCoeff(),
        (invCoeff * direction.z / state.z),
        (invCoeff * direction.ksi.cwiseQuotient(state.ksi)).maxCoeff(),
        (invCoeff * direction.eta.cwiseQuotient(state.eta)).maxCoeff(),
        (invCoeff * direction.mu.cwiseQuotient(state.mu)).maxCoeff(),
        (invCoeff * direction.zeta / state.zeta),
        (invCoeff * direction.s.cwiseQuotient(state.s)).maxCoeff()};

    // std::cout << maxInvStepLengths << std::endl;

    // std::cout << state.y << std::endl;
    // std::cout << direction.y << std::endl;

    // std::cout << direction.x << std::endl;

    const auto maxStepLength = 1.0 / std::max(maxInvStepLengths.maxCoeff(), 1.0);

    // Find the longest step that reduced the residuum
    const auto res0 = this->compute_relaxed_subproblem_residuum(state, epsilon);
    const auto err0 = res0.norm();

    // std::cout << res0.norm() << ' ' << res0.lpNorm<Eigen::Infinity>() << std::endl;

    auto stepLength = maxStepLength;
    auto res = this->compute_relaxed_subproblem_residuum(state + stepLength * direction, epsilon);

    // std::cout << res.norm() << ' ' << res.lpNorm<Eigen::Infinity>() << std::endl;

    while (err0 < res.norm()) {
        if (const auto relStepLength = stepLength / maxStepLength;
            relStepLength < this->parameters.minRelSteplength)
            throw std::range_error{"OptimizerMMA::compute_line_search_length(): Length of the step decreased below minimal threshold."};

        stepLength /= 2.0;
        res = this->compute_relaxed_subproblem_residuum(state + stepLength * direction, epsilon);

        // std::cout << res.norm() << ' ' << res.lpNorm<Eigen::Infinity>() << std::endl;
    }

    return stepLength;
}

auto OptimizerMMA::compute_dpsidx(const SolverState& state) const -> Eigen::VectorXd
{
    const auto Ux = (this->U - state.x);
    const auto xL = (state.x - this->L);

    return (this->p0 + this->P * state.lambda).cwiseQuotient(Ux.cwiseProduct(Ux)) -
        (this->q0 + this->Q * state.lambda).cwiseQuotient(xL.cwiseProduct(xL));
}

auto OptimizerMMA::compute_gi(const SolverState& state) const -> Eigen::VectorXd
{
    const auto Ux = (this->U - state.x);
    const auto xL = (state.x - this->L);

    return this->P.transpose() * Ux.cwiseInverse() + this->Q.transpose() * xL.cwiseInverse();
}

void OptimizerMMA::update_previously_stored_states(const Eigen::VectorXd& Xnew)
{
    std::swap(this->xMinus1, this->xMinus2);
    this->xMinus1 = Xnew;
}
}    // namespace mto