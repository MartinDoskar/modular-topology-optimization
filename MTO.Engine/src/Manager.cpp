#include "Manager.h"

#include "Config.h"
#include "Utilities.h"

#include <base64.h>
#include <fmt/core.h>
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>

#include <algorithm>
#include <stdexcept>

namespace mto {

Manager::Manager(const nlohmann::json& inJSON, Discretisation* inDiscretisation)
    : discretisation{inDiscretisation},
      filterType{inJSON.at("filterType").get<FilterType>()},
      filterRadius{inJSON.at("filterRadius").get<double>()}
{
    auto sw = spdlog::stopwatch{};

    if (inJSON.contains("relativeVolumeConstraint"))
        this->relVolumeConstraint = inJSON.at("relativeVolumeConstraint").get<double>();

    if (filterType == FilterType::Helmholtz) {
        this->initialize_Helmholtz_filter();
    }
    else if (filterType == FilterType::Kernel) {
        this->initialize_kernel_filter();
    }
    else if (filterType == FilterType::Sensitivity) {
        this->initialize_sensitivity_filter();
    }
    else
        throw std::invalid_argument{"Manager::Manager: Unsupported filter type."};

    if (PRINT_DETAILED_TIMING) {
        auto logger = spdlog::get("MTO.Logger");
        if (logger != nullptr)
            logger->info("\t\t\tFilter was constructed in {} s", sw);
    }

    const auto relVol = inJSON.at("relativeVolumeConstraint").get<double>();

    // Support modularity
    if (this->discretisation->is_modular()) {
        this->modularFlag = true;

        const auto& foo = this->discretisation->get_modular_element_map();
        const auto nElems = foo.size();
        const auto nElemsPerModule = 1 + std::max_element(std::begin(foo), std::end(foo), [](const auto& a, const auto& b) { return a.elementId < b.elementId; })->elementId;
        const auto nModules = 1 + std::max_element(std::begin(foo), std::end(foo), [](const auto& a, const auto& b) { return a.moduleId < b.moduleId; })->moduleId;

        auto triplets = std::vector<Eigen::Triplet<double>>{};
        triplets.reserve(nElems);
        for (auto i = 0; i < nElems; ++i)
            triplets.emplace_back(i, foo.at(i).moduleId * nElemsPerModule + foo.at(i).elementId, 1.0);

        this->aModular = Eigen::VectorXd::Constant(nModules * nElemsPerModule, relVol);

        // Enable module-wise constant initial guess
        if (inJSON.contains("modulewiseConstantInitialGuess")) {
            const auto& initGuess = inJSON["modulewiseConstantInitialGuess"];
            if (initGuess.size() != nModules)
                throw std::invalid_argument{"Manager::Manager: The length of the modulewiseConstantInitialGuess does not correspond to the number of modules in the problem."};

            for (auto i = 0; i < nModules; i++) {
                const auto& guessEntry = initGuess.at(i);
                if (guessEntry.is_array()) {
                    auto tempData = guessEntry.get<std::vector<double>>();
                    if (tempData.size() != nElemsPerModule)
                        throw std::invalid_argument{fmt::format("Manager::Manager: Provided initial guess for module no. {} does not correspond to the number of modules in the problem.", i)};

                    aModular.segment(i * nElemsPerModule, nElemsPerModule) = Eigen::Map<Eigen::VectorXd>(tempData.data(), tempData.size());
                }
                else if (guessEntry.is_number())
                    aModular.segment(i * nElemsPerModule, nElemsPerModule).setConstant(guessEntry.get<double>());
                else
                    throw std::invalid_argument{"Manager::Manager: Entry in modulewiseConstantInitialGuess array must be either a number or an array."};
            }
        }

        this->modularProjection = Eigen::SparseMatrix<double>{static_cast<Eigen::Index>(nElems), static_cast<Eigen::Index>(nModules * nElemsPerModule)};
        this->modularProjection.setFromTriplets(std::begin(triplets), std::end(triplets));
    }
    else {
        const auto nFreeElems = this->discretisation->get_number_of_free_elements();
        this->aDesign = Eigen::VectorXd::Constant(nFreeElems, relVol);
    }

    // Initialize unknowns
    sw.reset();
    this->apply_filter();
    this->apply_projection();

    this->update_densities_in_discretisation();

    if (PRINT_DETAILED_TIMING) {
        auto logger = spdlog::get("MTO.Logger");
        if (logger != nullptr)
            logger->info("\t\t\tDensities were initialized in {} s", sw);
    }

    if (this->filterType == FilterType::Sensitivity) {
        sw.reset();
        this->update_sensitivity_filter();

        if (PRINT_DETAILED_TIMING) {
            auto logger = spdlog::get("MTO.Logger");
            if (logger != nullptr)
                logger->info("\t\t\tSensitivity filter was updated in {} s", sw);
        }
    }
}

auto Manager::get_normalized_relative_volume_constraint_sensitivity() const -> LinearApproximation
{
    if (this->relVolumeConstraint.has_value())
        return (this->discretisation->get_relative_volume_sensitivity() - this->relVolumeConstraint.value()) / this->relVolumeConstraint.value();
    else
        throw std::logic_error{"Manager::get_normalized_relative_volume_constraint_sensitivity: relVolumeConstraint is not set."};
}

auto Manager::transform_from_physical_to_design_sensitivity(const LinearApproximation& inApproximation) const -> LinearApproximation
{
    auto tempGrad = Eigen::VectorXd{};
    if (this->filterType == FilterType::Helmholtz)
        tempGrad = (this->filterHelmholtzPe2n.value().transpose() * this->filterHelmholtzSolver.solve(this->filterHelmholtzPn2e.value().transpose() * inApproximation.funGradient));
    else if (this->filterType == FilterType::Kernel)
        tempGrad = (this->filterKernelMtrx.value().transpose() * inApproximation.funGradient);
    else if (this->filterType == FilterType::Sensitivity)
        tempGrad = (inApproximation.type != LinearApproximationType::Volume)
            ? this->filterKernelMtrx.value() * inApproximation.funGradient
            : inApproximation.funGradient;

    if (this->modularFlag)
        tempGrad = this->modularProjection.transpose() * tempGrad;

    return LinearApproximation{inApproximation.funValue, tempGrad, inApproximation.type};
}

auto Manager::quantify_grayness_of_physical_densities() const -> double
{
    const auto sum = std::accumulate(
        std::begin(this->aProjected), std::end(this->aProjected), 0.0,
        [](const auto& sum, const auto& density) {
            return sum + (((density) * (1.0 - density)) / 0.25);
        });

    return sum / this->aProjected.size();
}

void Manager::update_unknown_design_densities(const Eigen::VectorXd& updatedDensities)
{
    if (this->modularFlag)
        this->aModular = updatedDensities;
    else
        this->aDesign = updatedDensities;

    this->apply_filter();
    this->apply_projection();
    this->update_densities_in_discretisation();

    if (this->filterType == FilterType::Sensitivity) {
        this->update_sensitivity_filter();
    }
}

void Manager::initialize_Helmholtz_filter()
{
    const auto nNodes = this->discretisation->get_number_of_nodes();
    const auto nElems = this->discretisation->get_number_of_elements();

    auto trpPe2n = std::vector<Eigen::Triplet<double>>{};
    trpPe2n.reserve(nElems * 4);
    auto trpPn2e = std::vector<Eigen::Triplet<double>>{};
    trpPn2e.reserve(nElems * 4);
    auto trpMtrx = std::vector<Eigen::Triplet<double>>{};
    trpMtrx.reserve(nElems * 4 * 4);

    for (auto iE = size_t{0}; iE < nElems; ++iE) {
        const auto& nIds = this->discretisation->get_ith_element(iE).nodeIds;

        const auto& A = this->discretisation->get_ith_node_coordinates(nIds[0]);
        const auto& B = this->discretisation->get_ith_node_coordinates(nIds[1]);
        const auto& C = this->discretisation->get_ith_node_coordinates(nIds[2]);

        const auto hx = (B - A).norm();
        const auto hy = (C - A).norm();
        const auto V = hx * hy;

        const auto NN = (Eigen::Matrix4d{} << V / 9.0, V / 18.0, V / 18.0, V / 36.0,
            V / 18.0, V / 9.0, V / 36.0, V / 18.0,
            V / 18.0, V / 36.0, V / 9.0, V / 18.0,
            V / 36.0, V / 18.0, V / 18.0, V / 9.0)
                            .finished();

        const auto BB = (Eigen::Matrix4d{} << (hx * hx + hy * hy) / (3.0 * V), (hx * hx - 2 * hy * hy) / (6.0 * V), -(2 * hx * hx - hy * hy) / (6.0 * V), -(hx * hx + hy * hy) / (6.0 * V),
            (hx * hx - 2 * hy * hy) / (6.0 * V), (hx * hx + hy * hy) / (3.0 * V), -(hx * hx + hy * hy) / (6.0 * V), -(2 * hx * hx - hy * hy) / (6.0 * V),
            -(2 * hx * hx - hy * hy) / (6.0 * V), -(hx * hx + hy * hy) / (6.0 * V), (hx * hx + hy * hy) / (3.0 * V), (hx * hx - 2 * hy * hy) / (6.0 * V),
            -(hx * hx + hy * hy) / (6.0 * V), -(2 * hx * hx - hy * hy) / (6.0 * V), (hx * hx - 2 * hy * hy) / (6.0 * V), (hx * hx + hy * hy) / (3.0 * V))
                            .finished();

        const auto K = Eigen::Matrix4d{NN + this->filterRadius * this->filterRadius * BB};

        for (auto ir = size_t{0}; ir < size_t{4}; ++ir)
            for (auto ic = size_t{0}; ic < size_t{4}; ++ic)
                trpMtrx.emplace_back(nIds[ir], nIds[ic], K(ir, ic));

        // Fill element-to-node and node-to-element mapping matrix entries
        for (auto i = size_t{0}; i < size_t{4}; ++i) {
            trpPe2n.emplace_back(nIds[i], static_cast<int>(iE), 0.25 * V);
            trpPn2e.emplace_back(static_cast<int>(iE), nIds[i], 0.25);
        }
    }

    this->filterHelmholtzPe2n = Eigen::SparseMatrix<double>{static_cast<Eigen::Index>(nNodes), static_cast<Eigen::Index>(nElems)};
    this->filterHelmholtzPe2n->setFromTriplets(std::begin(trpPe2n), std::end(trpPe2n));

    this->filterHelmholtzPn2e = Eigen::SparseMatrix<double>{static_cast<Eigen::Index>(nElems), static_cast<Eigen::Index>(nNodes)};
    this->filterHelmholtzPn2e->setFromTriplets(std::begin(trpPn2e), std::end(trpPn2e));

    this->filterHelmholtzMtrx = Eigen::SparseMatrix<double>{static_cast<Eigen::Index>(nNodes), static_cast<Eigen::Index>(nNodes)};
    this->filterHelmholtzMtrx->setFromTriplets(std::begin(trpMtrx), std::end(trpMtrx));

    this->filterHelmholtzSolver.compute(this->filterHelmholtzMtrx.value());
}

void Manager::initialize_kernel_filter()
{
    auto weightFun = [this](double dist) { return std::max(1.0 - dist / this->filterRadius, 0.0); };

    const auto freeElemRemap = this->discretisation->get_renum_map();
    const auto elemVolumes = this->discretisation->get_element_whole_volumes();
    const auto elemAdjacency = this->discretisation->get_adjacent_free_elements_within_given_radius(this->filterRadius);

    auto trpMtrx = std::vector<Eigen::Triplet<double>>{};
    const auto nEntries = std::accumulate(std::begin(elemAdjacency), std::end(elemAdjacency), size_t{0},
        [](const auto sum, const auto ea) { return sum + ea.size(); });
    trpMtrx.reserve(nEntries);
    const auto nElems = this->discretisation->get_number_of_elements();
    for (auto iE = size_t{0}; iE < nElems; ++iE)
        if (!this->discretisation->get_ith_element(iE).is_fixed()) {

            const auto& relatedElements = elemAdjacency.at(iE);
            const auto norm = std::accumulate(
                std::begin(relatedElements), std::end(relatedElements),
                0.0,
                [&weightFun, &elemVolumes](const auto sum, const auto re) {
                    return sum + weightFun(re.distance) * elemVolumes.at(re.index);
                });

            for (const auto [eId, d] : relatedElements)
                trpMtrx.emplace_back(
                    freeElemRemap.at(iE),
                    freeElemRemap.at(eId),
                    (elemVolumes.at(eId) * weightFun(d)) / norm);
        }

    const auto nFreeElems = this->discretisation->get_number_of_free_elements();
    this->filterKernelMtrx = Eigen::SparseMatrix<double>{static_cast<Eigen::Index>(nFreeElems), static_cast<Eigen::Index>(nFreeElems)};
    filterKernelMtrx->setFromTriplets(std::begin(trpMtrx), std::end(trpMtrx));
}

void Manager::update_sensitivity_filter()
{
    constexpr auto eps = 1.0e-3;

    if (this->filterType != FilterType::Sensitivity)
        throw std::logic_error{"Manager::update_sensitivity_filter: Filter is not set to sensitivity."};

    const auto freeElemRemap = this->discretisation->get_renum_map();
    this->filterKernelMtrx =
        (this->aProjected.cwiseMax(eps).cwiseInverse()).eval().asDiagonal() * (this->filterKernelMtrxInitial.value() * this->aProjected.asDiagonal());

    auto logger = spdlog::get("MTO.Logger");
}

void Manager::initialize_sensitivity_filter()
{
    // Using “energetical” sensitivity filter proposed in Sigmund, O. (2007). Morphology-based black and white filters for topology optimization. Structural and Multidisciplinary Optimization, 33(4-5), 401-424. https://doi.org/10.1007/s00158-006-0087-x
    auto weightFun = [this](double dist) { return std::max(1.0 - dist / this->filterRadius, 0.0); };

    const auto freeElemRemap = this->discretisation->get_renum_map();
    const auto elemVolumes = this->discretisation->get_element_whole_volumes();
    const auto elemAdjacency = this->discretisation->get_adjacent_free_elements_within_given_radius(this->filterRadius);

    auto trpMtrx = std::vector<Eigen::Triplet<double>>{};
    const auto nEntries = std::accumulate(std::begin(elemAdjacency), std::end(elemAdjacency), size_t{0},
        [](const auto sum, const auto ea) { return sum + ea.size(); });
    trpMtrx.reserve(nEntries);
    const auto nElems = this->discretisation->get_number_of_elements();
    for (auto iE = size_t{0}; iE < nElems; ++iE)
        if (!this->discretisation->get_ith_element(iE).is_fixed()) {

            const auto& relatedElements = elemAdjacency.at(iE);
            const auto norm = std::accumulate(
                std::begin(relatedElements), std::end(relatedElements),
                0.0,
                [&weightFun, &elemVolumes](const auto sum, const auto re) {
                    return sum + weightFun(re.distance);
                });

            for (const auto [eId, d] : relatedElements)
                trpMtrx.emplace_back(
                    freeElemRemap.at(iE),
                    freeElemRemap.at(eId),
                    (weightFun(d) / elemVolumes.at(eId)) / (norm / elemVolumes.at(iE)));
        }

    const auto nFreeElems = this->discretisation->get_number_of_free_elements();
    this->filterKernelMtrxInitial = Eigen::SparseMatrix<double>{static_cast<Eigen::Index>(nFreeElems), static_cast<Eigen::Index>(nFreeElems)};
    filterKernelMtrxInitial->setFromTriplets(std::begin(trpMtrx), std::end(trpMtrx));
};

void Manager::apply_filter()
{
    if (this->modularFlag)
        this->aDesign = this->modularProjection * this->aModular;

    if (this->filterType == FilterType::Helmholtz)
        this->aFiltered = this->filterHelmholtzPn2e.value() * this->filterHelmholtzSolver.solve(this->filterHelmholtzPe2n.value() * this->aDesign);
    else if (this->filterType == FilterType::Kernel)
        this->aFiltered = this->filterKernelMtrx.value() * this->aDesign;
    else if (this->filterType == FilterType::Sensitivity) {
        // Sensitivity filter does not affect densities
        this->aFiltered = this->aDesign;
    }
}

void Manager::apply_projection()
{
    // TODO: Modify when additional projection is added
    this->aProjected = this->aFiltered;
}

void Manager::update_densities_in_discretisation()
{
    this->discretisation->update_element_densities(this->aProjected);
}

auto Manager::print_densities_for_VTK(const DensityType type, const bool useBinary) const -> std::string
{
    const auto outputVec = std::invoke(
        [this, &type]() -> const std::vector<float> {
            auto tempVec = Eigen::VectorXd{};
            switch (type) {
                case DensityType::Design:
                    tempVec = this->aDesign;
                    break;
                case DensityType::Filtered:
                    tempVec = this->aFiltered;
                    break;
                case DensityType::Projected:
                    tempVec = this->aProjected;
                    break;
                default:
                    throw std::invalid_argument{"Manager::print_densities_for_VTK: Unsupported density type."};
            };

            const auto nElems = this->discretisation->get_number_of_elements();
            const auto nFreeElems = this->discretisation->get_number_of_free_elements();
            if (nElems != nFreeElems) {
                const auto renumMap = this->discretisation->get_renum_map();

                auto outVec = Eigen::VectorXd{Eigen::VectorXd::Constant(nElems, -1.0)};
                for (auto iE = 0; iE < nElems; ++iE) {
                    if (!this->discretisation->get_ith_element(iE).is_fixed())
                        outVec(iE) = tempVec(renumMap.at(iE));
                    else
                        outVec(iE) = this->discretisation->get_ith_element(iE).get_density();
                }

                tempVec = outVec;
            }

            auto floatVec = std::vector<float>{};
            floatVec.reserve(tempVec.size());
            std::transform(std::begin(tempVec), std::end(tempVec), std::back_inserter(floatVec),
                [](const auto& v) { return static_cast<float>(v); });

            return floatVec;
        });

    const auto label = std::invoke(
        [this, &type]() -> std::string {
            switch (type) {
                case DensityType::Design:
                    return "DensityDesign";
                case DensityType::Filtered:
                    return "DensityFiltered";
                case DensityType::Projected:
                    return "DensityProjected";
                default:
                    throw std::invalid_argument{"Manager::print_densities_for_VTK: Unsupported density type."};
            };
        });

    auto out = std::string{};
    if (useBinary) {
        out.reserve(100 + static_cast<size_t>(std::ceil(outputVec.size() * sizeof(float) * 8 / 6)));
        fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Float32\" Name=\"{}\" format=\"binary\">\n", label);
        fmt::format_to(std::back_inserter(out), "{}",
            base64::encode_vector_into_base64_string_with_length(outputVec));
        fmt::format_to(std::back_inserter(out), "\t\t\t\t</DataArray>\n");
    }
    else {
        out.append("SCALARS " + label + " double 1\n");
        out.append("LOOKUP_TABLE default\n");
        for (const auto& a : outputVec)
            out.append(double_to_formated_string(a) + '\n');
    }

    return out;
}

}    // namespace mto