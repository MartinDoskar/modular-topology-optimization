#include "OptimizerOC.h"

#include <spdlog/spdlog.h>

#include <complex>
#include <memory>
#include <stdexcept>

namespace mto {

auto OptimizerOC::clone() -> std::unique_ptr<OptimizerInterface>
{
    return std::make_unique<OptimizerOC>(*this);
}

auto OptimizerOC::update(const Eigen::VectorXd& x, const LinearApproximation& objective, const std::vector<LinearApproximation>& constraints) -> Eigen::VectorXd
{
    const auto N = x.size();
    const auto nConstraints = constraints.size();

    auto xNew = Eigen::VectorXd{N}.setZero();

    auto numComplex = 0U;
    auto numZeroDivision = 0U;

    switch (this->parameters.version) {
        case 0:
            [[fallthrough]];
        case 1: {
            if (nConstraints != 1)
                throw std::invalid_argument{"OptimizerOC::Update: OC optimizers 0 and 1 permit only one constraint to be used."};

            auto L1 = this->parameters.lOCbeg;
            auto L2 = this->parameters.lOCend;

            while ((L2 - L1) / (L1 + L2) > this->parameters.lOCstepLim &&
                L2 > this->parameters.lOCminLim) {
                const auto Lmid = (L1 + L2) / 2.0;

#pragma omp parallel for
                for (auto i = 0; i < N; ++i) {
                    const auto Be = std::pow(
                            std::complex<double>(
                                std::max(
                                    -objective.funGradient(i) / constraints[0].funGradient(i) / Lmid, 
                                    this->parameters.inconsistentModification)),
                            parameters.dampingOC);

                    if (std::abs(Be.imag()) >= 1e-15)
#pragma omp atomic
                        numComplex += 1;
                    else if (std::abs(constraints[0].funGradient(i)) <= 1e-15)
#pragma omp atomic
                        numZeroDivision += 1;

                    const auto aTrial = std::pow(x(i) * Be.real(), this->q);
                    const auto aLower = std::max(0.0, x(i) - this->parameters.stepOC);
                    const auto aUpper = std::min(1.0, x(i) + this->parameters.stepOC);

                    xNew(i) = (aTrial < aUpper) ? ((aTrial > aLower) ? aTrial : aLower) : aUpper;
                }

                if (constraints[0].funValue + constraints[0].funGradient.dot(xNew - x) > 0.0)
                    L1 = Lmid;
                else
                    L2 = Lmid;
            }
            break;
        }
        // GOCM based on Kim, N. H., Dong, T., Weinberg, D., & Dalidd, J. (2021). Generalized Optimality Criteria Method for Topology Optimization. Applied Sciences, 11(7), 3175.
        case 2: {
            if (this->iter == 0) {
                this->objInit = objective.funValue;
                this->gl = Eigen::VectorXd{nConstraints}.setZero();
                this->lOCmid = Eigen::VectorXd{nConstraints}.setOnes();
            }

            const auto eps = 0.05;
            auto g = Eigen::VectorXd{nConstraints}.setZero();
            for (auto i = 0; i < nConstraints; ++i)
                g(i) = constraints[i].funValue;

            const auto dg = g - this->gl;
            this->gl = g;

            for (auto i = 0; i < nConstraints; ++i) {
                const auto p0 = (g(i) * dg(i) > .0) 
                    ? 1.0 
                    : (((g(i) > 0.0 && dg(i) > -eps) || (g(i) < 0.0 && dg(i) < eps)) ? 0.5 : 0.0);
                this->lOCmid(i) *= (1.0 + p0 * (g(i) + dg(i)));
            }

#pragma omp parallel for
            for (auto i = 0; i < N; ++i) {
                auto posPart = std::max(objective.funGradient(i) / this->objInit, 0.0);
                auto negPart = std::min(objective.funGradient(i) / this->objInit, 0.0);
                for (auto j = 0; j < nConstraints; ++j)
                    (constraints[j].funGradient(i) > 0) 
                        ? (posPart += (this->lOCmid(j) * constraints[j].funGradient(i))) 
                        : (negPart += (this->lOCmid(j) * constraints[j].funGradient(i)));

                const auto Be = std::pow(std::complex<double>(std::max(-negPart / posPart, this->parameters.inconsistentModification)), this->parameters.dampingOC);

                if (std::abs(Be.imag()) >= 1e-15)
#pragma omp atomic
                    numComplex += 1;
                else if (std::abs(posPart) <= 1e-15)
#pragma omp atomic
                    numZeroDivision += 1;

                const auto aTrial = std::pow(x(i) * Be.real(), this->q);

                const auto aLower = std::max(0.0, x(i) - this->parameters.stepOC);
                const auto aUpper = std::min(1.0, x(i) + this->parameters.stepOC);

                xNew(i) = (aTrial < aUpper) ? ((aTrial > aLower) ? aTrial : aLower) : aUpper;
            }
            break;
        }
        default:
            throw std::invalid_argument{"OptimizerOC::Update: Unknown OC version."};
    }

    if (numComplex > 0) {
        auto logger = spdlog::get("MTO.Logger");
        logger->error("OC update produced {} complex domain outputs.", numComplex);
    }
    if (numZeroDivision > 0) {
        auto logger = spdlog::get("MTO.Logger");
        logger->error("OC update produced {} divisions by zero.", numZeroDivision);
    }

    this->iter++;
    if (this->iter > this->parameters.qAfterIter)
        q = std::min(q * this->parameters.qMultiplier, this->parameters.qmax);

    return xNew;
}

}    // namespace mto