#include "Task.h"

#include "Config.h"
#include "Discretisation.h"
#include "OptimizerMMA.h"
#include "OptimizerOC.h"

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <fmt/chrono.h>
#include <fmt/core.h>
#include <nlohmann/json.hpp>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <random>
#include <stdexcept>
#include <utility>

namespace mto {

auto create_optimizer(const nlohmann::json& inJSON, int defaultOCversion = 0) -> std::unique_ptr<OptimizerInterface>
{
    const auto type = std::invoke(
        [&inJSON]() {
            if (inJSON.contains("optimizerType")) {
                const auto typeStr = inJSON["optimizerType"].get<std::string>();

                if (typeStr.find("OC") != std::string::npos)
                    return OptimizerType::OC;
                // else if (typeStr.find("MMA") != std::string::npos)
                //     return OptimizerType::MMA;
                else
                    throw std::invalid_argument{"create_optimizer: Unsupported optimizer type."};
            }
            else
                return OptimizerType::OC;
        });
    inJSON.contains("optimizerType");

    if (type == OptimizerType::OC) {
        auto params = ParametersOC{};

        params.version = defaultOCversion;
        if (inJSON.contains("versionOC"))
            params.version = inJSON["versionOC"].get<int>();

        params.dampingOC = (params.version == 1) ? 0.3 : 0.5;
        if (inJSON.contains("dampingOC"))
            params.dampingOC = inJSON["dampingOC"].get<double>();

        if (inJSON.contains("stepOC"))
            params.stepOC = inJSON["stepOC"].get<double>();

        if (inJSON.contains("qMultiplierOC"))
            params.qMultiplier = inJSON["qMultiplierOC"].get<double>();

        if (inJSON.contains("qMaxOC"))
            params.qmax = inJSON["qMaxOC"].get<double>();

        if (inJSON.contains("qAfterIterOC"))
            params.qAfterIter = inJSON["qAfterIterOC"].get<int>();

        params.inconsistentModification = (params.version == 1) ? 1e-10 : -1e38;
        if (inJSON.contains("inconsistentModificationOC"))
            params.inconsistentModification = inJSON["inconsistentModificationOC"].get<double>();

        return std::make_unique<OptimizerOC>(params);
    }
    else {
        throw std::invalid_argument{"create_optimizer: Unsupported optimizer type."};
    }
}

TaskParameters::TaskParameters(const nlohmann::json& inJSON)
{
    // Load convergence parameters
    if (inJSON.contains("limIter"))
        this->nMaxIter = inJSON["limIter"].get<size_t>();
    if (inJSON.contains("limDesignChange"))
        this->limDesignChange = inJSON["limDesignChange"].get<double>();
    if (inJSON.contains("limObjectiveChange"))
        this->limObjectiveChange = inJSON["limObjectiveChange"].get<double>();
    if (inJSON.contains("limGrayness"))
        this->limGrayness = inJSON["limGrayness"].get<double>();

    // Load penalisation parameters (also with continuation)
    const auto continuationParamsPresence = std::array<bool, 4>{
        inJSON.contains("penalisationInit"),
        inJSON.contains("penalisationIncrement"),
        inJSON.contains("penalisationStep"),
        inJSON.contains("penalisationLimit")};

    if (inJSON.contains("penalisation")) {
        if (std::any_of(std::begin(continuationParamsPresence), std::end(continuationParamsPresence), [](const auto& a) { return a; }))
            throw std::invalid_argument{"TaskParameters::TaskParameters: Provide either penalisation or all penalisation continuation parameters."};

        this->penalisation = inJSON["penalisation"].get<double>();
    }
    else if (std::any_of(std::begin(continuationParamsPresence), std::end(continuationParamsPresence), [](const auto& a) { return a; })) {
        if (std::any_of(std::begin(continuationParamsPresence), std::end(continuationParamsPresence), [](const auto& a) { return !a; }))
            throw std::invalid_argument{"TaskParameters::TaskParameters: Provide all four penalisation continuation parameters."};

        this->penalisationInit = inJSON["penalisationInit"].get<double>();
        this->penalisationIncrement = inJSON["penalisationIncrement"].get<double>();
        this->penalisationStep = inJSON["penalisationStep"].get<size_t>();
        this->penalisationLim = inJSON["penalisationLimit"].get<double>();

        this->useContinuation = true;
    }

    // Load output parameters
    if (inJSON.contains("outputFolder"))
        this->outputFolder = inJSON["outputFolder"].get<std::string>();
    if (inJSON.contains("useBinaryVTK"))
        this->saveEachNthStepVTK = inJSON["useBinaryVTK"].get<bool>();
    if (inJSON.contains("storeNodalwiseDuals"))
        this->saveEachNthStepVTK = inJSON["storeNodalwiseDuals"].get<bool>();
    if (inJSON.contains("storeElementwiseDuals"))
        this->saveEachNthStepVTK = inJSON["storeElementwiseDuals"].get<bool>();
    if (inJSON.contains("saveEachNthStepVTK"))
        this->saveEachNthStepVTK = inJSON["saveEachNthStepVTK"].get<size_t>();
    if (inJSON.contains("saveEachNthStepSVG"))
        this->saveEachNthStepSVG = inJSON["saveEachNthStepSVG"].get<size_t>();
    if (inJSON.contains("saveModulesEachNthStepSVG"))
        this->saveModulesEachNthStepSVG = inJSON["saveModulesEachNthStepSVG"].get<size_t>();
}

auto TaskParameters::get_penalisation(const int iteration) const -> double
{
    if (this->useContinuation)
        return std::min(
            this->penalisationInit + (iteration / this->penalisationStep) * this->penalisationIncrement,
            this->penalisationLim);
    else
        return this->penalisation;
}

auto TaskParameters::is_at_final_penalisation(const int iteration) const -> bool
{
    return (this->useContinuation)
        ? (this->get_penalisation(iteration) >= this->penalisationLim)
        : true;
}

Task::Task(const std::filesystem::path& inFilePath)
    : Task::Task{load_JSON_file(inFilePath)}
{
}

Task::Task(const nlohmann::json& inJSON)
    : parameters{inJSON}, discretisation{inJSON}, designManager{inJSON, &this->discretisation}
{
    // Initialize log
    if (auto existingLogger = spdlog::get("MTO.Logger");
        existingLogger == nullptr)
        this->logger = initialize_logger(this->parameters.get_output_folder());
    else
        this->logger = existingLogger;

    // Initialize individual problems
    if (inJSON.contains("problems"))
        if (inJSON["problems"].is_array()) {
            this->problems.reserve(inJSON["problems"].size());
            for (const auto& pjson : inJSON["problems"])
                this->problems.emplace_back(pjson, &this->discretisation);
        }

    // Initialize optimizer
    int defaultOCversion = 0;
    for (auto i = 0; i < this->problems.size(); ++i)
        if (this->problems[i].is_simple_compliance() == false)
            defaultOCversion = 1;
    this->optimizer = create_optimizer(inJSON, defaultOCversion);
}

auto Task::optimize() -> double
{
    // Initial step
    auto iIter = 0;
    auto penalisationCoeff = this->parameters.get_penalisation(iIter);
    if (PRINT_DETAILED_TIMING)
        this->logger->info("{:%H:%M:%S}: i = {} (p = {:.2f}):", fmt::localtime(std::time(nullptr)), iIter, penalisationCoeff);

    for (auto& problem : this->problems)
        problem.solve_state(penalisationCoeff);
    auto objectiveValue = this->problems.back().compute_objective();

    if (this->parameters.get_state_saving_stepVTK() > 0)
        this->save_state_in_VTK(
            this->parameters.get_output_folder() / ("step_" + std::to_string(iIter) + (this->parameters.use_binary_VTK() ? ".vtu" : ".vtk")),
            this->parameters.use_binary_VTK());
    // if (this->parameters.get_state_saving_stepSVG() > 0)
    //     this->save_state_in_SVG(this->parameters.get_output_folder() / ("step_" + std::to_string(iIter) + ".svg"));
    // if (this->parameters.get_state_saving_step_modulesSVG() > 0)
    //     this->save_state_modules_in_SVG(this->parameters.get_output_folder() / ("step_" + std::to_string(iIter) + "_module_"));

    if (PRINT_DETAILED_TIMING)
        this->logger->info("\tObj = {:+e}\n\tV = {:.3f}\n\tg = {:.6f}",
            objectiveValue, this->discretisation.get_relative_current_volume(),
            this->designManager.quantify_grayness_of_physical_densities());
    else
        this->logger->info("{:%H:%M:%S}: i = {} (p = {:.2f}): Obj = {:+e}, V = {:.3f}, g = {:.6f}",
            fmt::localtime(std::time(nullptr)), iIter, penalisationCoeff,
            objectiveValue,
            this->discretisation.get_relative_current_volume(),
            this->designManager.quantify_grayness_of_physical_densities());

    // Iterate until convergence
    auto proceed = true;
    while (proceed) {
        ++iIter;
        penalisationCoeff = this->parameters.get_penalisation(iIter);
        if (PRINT_DETAILED_TIMING)
            this->logger->info("{:%H:%M:%S}: i = {} (p = {:.2f}):", fmt::localtime(std::time(nullptr)), iIter, penalisationCoeff);

        // Compute sensitivities
        auto sw = spdlog::stopwatch{};
        const auto dvda = this->designManager.transform_from_physical_to_design_sensitivity(
            this->designManager.get_normalized_relative_volume_constraint_sensitivity());
        const auto dcda = this->designManager.transform_from_physical_to_design_sensitivity(
            this->problems.back().compute_sensitivity(penalisationCoeff));
        if (PRINT_DETAILED_TIMING)
            this->logger->info("\t\tSensitivities computed in {} s", sw);

        // Update design variables
        sw.reset();
        const auto oldDesign = this->designManager.get_current_design();
        const auto newDesign = this->optimizer->update(oldDesign, dcda, {dvda});

        this->designManager.update_unknown_design_densities(newDesign);
        if (PRINT_DETAILED_TIMING)
            this->logger->info("\t\tDensities updated in {} s", sw);

        // Recompute state and objective
        sw.reset();
        for (auto& problem : this->problems)
            problem.solve_state(penalisationCoeff);
        const auto newObjectiveValue = this->problems.back().compute_objective();
        const auto dObjective = newObjectiveValue - objectiveValue;
        objectiveValue = newObjectiveValue;
        if (PRINT_DETAILED_TIMING)
            this->logger->info("\t\tCurrent state recomputed in {} s", sw);

        // Store state if requested
        sw.reset();
        if ((this->parameters.get_state_saving_stepVTK() > 0) && ((iIter % this->parameters.get_state_saving_stepVTK()) == 0))
            this->save_state_in_VTK(
                this->parameters.get_output_folder() / ("step_" + std::to_string(iIter) + (this->parameters.use_binary_VTK() ? ".vtu" : ".vtk")),
                this->parameters.use_binary_VTK());
        if ((this->parameters.get_state_saving_stepSVG() > 0) && ((iIter % this->parameters.get_state_saving_stepSVG()) == 0))
            this->save_state_in_SVG(this->parameters.get_output_folder() / "SVG" / ("step_" + std::to_string(iIter) + ".svg"));
        if (this->discretisation.is_modular())
            if ((this->parameters.get_state_saving_step_modulesSVG() > 0) && ((iIter % this->parameters.get_state_saving_step_modulesSVG()) == 0))
                this->save_state_modules_in_SVG(this->parameters.get_output_folder() / "SVG" / ("step_" + std::to_string(iIter) + "_module_"));

        if (PRINT_DETAILED_TIMING)
            this->logger->info("\t\tIteration data stored in {} s", sw);

        // Print iteration info
        if (PRINT_DETAILED_TIMING)
            this->logger->info("\tObj = {:+e}\n\tV = {:.3f}\n\t(dObj = {:+e}, |dX|inf = {:.3e})\n\tg = {:.6f}",
                objectiveValue,
                this->discretisation.get_relative_current_volume(),
                dObjective,
                (newDesign - oldDesign).lpNorm<Eigen::Infinity>(),
                this->designManager.quantify_grayness_of_physical_densities());
        else
            this->logger->info("{:%H:%M:%S}: i = {} (p = {:.2f}): Obj = {:+e}, V = {:.3f}, dObj = {:+e}, |dX|inf = {:.3e}, g = {:.6f}",
                fmt::localtime(std::time(nullptr)), iIter, penalisationCoeff,
                objectiveValue,
                this->discretisation.get_relative_current_volume(),
                dObjective,
                (newDesign - oldDesign).lpNorm<Eigen::Infinity>(),
                this->designManager.quantify_grayness_of_physical_densities());

        // Check convergence
        if ((std::abs(dObjective) < this->parameters.get_objective_change_limit()) &&
            (this->parameters.is_at_final_penalisation(iIter)) &&
            (this->parameters.get_grayness_limit() > this->designManager.quantify_grayness_of_physical_densities())) {
            proceed = false;
            this->logger->info("Optimization converged w.r.t. the chosen limit of relative objective value change.");
        }
        else if (((newDesign - oldDesign).lpNorm<Eigen::Infinity>() < this->parameters.get_design_change_limit()) &&
            (this->parameters.is_at_final_penalisation(iIter)) &&
            (this->parameters.get_grayness_limit() > this->designManager.quantify_grayness_of_physical_densities())) {
            proceed = false;
            this->logger->info("Optimization converged w.r.t. the chosen limit of relative design change.");
        }
        else if (iIter >= this->parameters.get_iteration_limit()) {
            proceed = false;
            this->logger->info("Optimization reached iteration limit without converging.");
        }
    }

    // Compute nodal representation of duals
    for (auto& problem : this->problems) {
        if (this->parameters.are_nodalwise_duals_required())
            problem.compute_duals_nodalwise(penalisationCoeff);
        if (this->parameters.are_elementwise_duals_required())
            problem.compute_duals_elementwise(penalisationCoeff);
    }

    // Plot the last state if not plotted
    if ((this->parameters.get_state_saving_stepVTK() > 0) &&
        (((iIter % this->parameters.get_state_saving_stepVTK()) != 0) || (this->parameters.are_nodalwise_duals_required() || this->parameters.are_elementwise_duals_required())))
        this->save_state_in_VTK(
            this->parameters.get_output_folder() / ("step_" + std::to_string(iIter) + (this->parameters.use_binary_VTK() ? ".vtu" : ".vtk")),
            this->parameters.use_binary_VTK());

    if ((this->parameters.get_state_saving_stepSVG() > 0) && ((iIter % this->parameters.get_state_saving_stepSVG()) != 0))
        this->save_state_in_SVG(this->parameters.get_output_folder() / "SVG" / ("step_" + std::to_string(iIter) + ".svg"));
    if (this->discretisation.is_modular())
        if ((this->parameters.get_state_saving_step_modulesSVG() > 0) && ((iIter % this->parameters.get_state_saving_step_modulesSVG()) != 0))
            this->save_state_modules_in_SVG(this->parameters.get_output_folder() / "SVG" / ("step_" + std::to_string(iIter) + "_module_"));

    return objectiveValue;
}

void Task::save_state_in_VTK(const std::filesystem::path& outFilePath, const bool useBinary, const bool printIncrement) const
{
    if (!std::filesystem::exists(outFilePath.parent_path()) && !outFilePath.parent_path().empty())
        std::filesystem::create_directory(outFilePath.parent_path());

    auto outF = std::ofstream{outFilePath};
    if (!outF.is_open())
        throw std::invalid_argument{"Task::store_state_in_VTK: Unable to open the requested output file."};

    if (useBinary) {

        outF << R"(<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">)" << '\n';
        outF << "\t<UnstructuredGrid>\n";
        outF << "\t\t"
             << R"(<Piece NumberOfPoints=")" << this->discretisation.get_number_of_nodes()
             << R"(" NumberOfCells=")" << this->discretisation.get_number_of_elements() << R"(">)" << '\n';

        outF << this->discretisation.print_data_for_VTK(useBinary);

        outF << "\t\t\t"
             << R"(<PointData Vectors="Displacements">)" << '\n';
        outF << this->problems.back().print_vertex_state_for_VTK(useBinary);
        outF << "\t\t\t</PointData>\n";

        outF << "\t\t\t"
             << R"(<CellData Scalars="DensityDesign">)" << '\n';
        outF << this->designManager.print_densities_for_VTK(DensityType::Design, useBinary);
        outF << this->designManager.print_densities_for_VTK(DensityType::Projected, useBinary);
        outF << this->problems.back().print_element_state_for_VTK(useBinary);
        outF << "\t\t\t</CellData>\n";

        outF << "\t\t</Piece>\n";
        outF << "\t</UnstructuredGrid>\n";
        outF << "</VTKFile>\n";
    }
    else {
        // Write header
        outF << "# vtk DataFile Version 3.0\n";
        outF << "Snapshot of Modular Topology Optimization iteration\n";
        outF << "ASCII\n\n";
        outF << "DATASET UNSTRUCTURED_GRID\n";

        // Write FE mesh definition
        outF << this->discretisation.print_data_for_VTK(useBinary);

        // Write data
        outF << "POINT_DATA " << std::to_string(this->discretisation.get_number_of_nodes()) << '\n';
        outF << this->problems.back().print_vertex_state_for_VTK(useBinary);
        outF << '\n';

        outF << "CELL_DATA " << std::to_string(this->discretisation.get_number_of_elements()) << '\n';
        outF << this->designManager.print_densities_for_VTK(DensityType::Design, useBinary);
        outF << this->designManager.print_densities_for_VTK(DensityType::Projected, useBinary);
        outF << this->problems.back().print_element_state_for_VTK(useBinary);
    }

    outF.close();
}

void Task::save_state_in_SVG(const std::filesystem::path& outFilePath) const
{
    if (!std::filesystem::exists(outFilePath.parent_path()) && !outFilePath.parent_path().empty())
        std::filesystem::create_directory(outFilePath.parent_path());

    auto outF = std::ofstream{outFilePath};
    if (!outF.is_open())
        throw std::invalid_argument{"Task::store_state_in_SVG: Unable to open the requested output file."};

    auto bbox = this->discretisation.get_bounding_box();
    auto w = bbox.maxCorner(0) - bbox.minCorner(0);
    auto h = bbox.maxCorner(1) - bbox.minCorner(1);

    // compute multiplier based on width, height and number of elements
    auto area = this->discretisation.get_whole_volume();
    auto nelem = this->discretisation.get_number_of_elements();
    auto elemSize = std::sqrt(area / nelem);    // works well for square elemnts only!
    auto nelemx = std::round(w / elemSize);
    auto nelemy = std::round(h / elemSize);
    auto multiplier = std::max((double)nelemx / w, (double)nelemy / h);

    // Write header
    outF << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n";
    outF << "<svg version=\"1.1\" width=\"" << double_to_formated_string(w * multiplier) << "\" height=\""
         << double_to_formated_string(h * multiplier)
         << "\" shape-rendering=\"crispEdges\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n";
    outF << "<desc>Snapshot of Modular Topology Optimization iteration</desc>\n";
    // SVG y axis is in opposite (top-down) direction, so we need to flip
    outF << "<g transform=\"scale(1 -1) translate(0.0 " << double_to_formated_string(-h * multiplier) << ")\">\n";

    // Write FE mesh definition
    outF << this->discretisation.print_data_for_SVG(multiplier);

    outF << "</g>\n</svg>\n";
    outF.close();
}

void Task::save_state_modules_in_SVG(const std::filesystem::path& outFilePath) const
{
    if (!std::filesystem::exists(outFilePath.parent_path()) && !outFilePath.parent_path().empty())
        std::filesystem::create_directory(outFilePath.parent_path());

    auto bboxOrig = this->discretisation.get_bounding_box();
    auto wOrig = bboxOrig.maxCorner(0) - bboxOrig.minCorner(0);
    auto hOrig = bboxOrig.maxCorner(1) - bboxOrig.minCorner(1);

    // compute multiplier based on width, height and number of elements
    auto area = this->discretisation.get_whole_volume();
    auto nelem = this->discretisation.get_number_of_elements();
    auto elemSize = std::sqrt(area / nelem);    // works well for square elemnts only!
    auto nelemx = std::round(wOrig / elemSize);
    auto nelemy = std::round(hOrig / elemSize);
    auto multiplier = std::max((double)nelemx / wOrig, (double)nelemy / hOrig);

    const auto [tset, bbox] = this->discretisation.print_modules_data_for_SVG(multiplier);
    for (auto i = 0; i < tset.size(); ++i) {
        auto outF = std::ofstream{outFilePath.string() + std::to_string(i) + ".svg"};

        if (!outF.is_open())
            throw std::invalid_argument{"Task::store_state_in_SVG: Unable to open the requested output file."};

        auto w = bbox[i].maxCorner(0) - bbox[i].minCorner(0);
        auto h = bbox[i].maxCorner(1) - bbox[i].minCorner(1);

        outF << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n";
        outF << "<svg version=\"1.1\" width=\"" << double_to_formated_string(w * multiplier)
             << "\" height=\"" << double_to_formated_string(h * multiplier)
             << "\" shape-rendering=\"crispEdges\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n";
        outF << "<desc>Snapshot of Modular Topology Optimization iteration</desc>\n";
        outF << tset[i];
        outF << "</svg>\n";
        outF.close();
    }
}

}    // namespace mto