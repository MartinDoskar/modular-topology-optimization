#include "LinearApproximation.h"

namespace mto {

auto LinearApproximation::operator+=(const double c) -> LinearApproximation&
{
    this->funValue += c;
    return *this;
}

auto LinearApproximation::operator-=(const double c) -> LinearApproximation&
{
    this->funValue -= c;
    return *this;
}

auto LinearApproximation::operator*=(const double c) -> LinearApproximation&
{
    this->funValue *= c;
    this->funGradient *= c;
    return *this;
}

auto LinearApproximation::operator/=(const double c) -> LinearApproximation&
{
    this->funValue /= c;
    this->funGradient /= c;
    return *this;
}

auto operator+(LinearApproximation lhs, const double rhs) -> LinearApproximation
{
    lhs += rhs;
    return lhs;
}

auto operator+(const double lhs, LinearApproximation rhs) -> LinearApproximation
{
    rhs += lhs;
    return rhs;
}

auto operator-(LinearApproximation lhs, const double rhs) -> LinearApproximation
{
    lhs -= rhs;
    return lhs;
}

auto operator-(const double lhs, LinearApproximation rhs) -> LinearApproximation
{
    rhs -= lhs;
    return rhs;
}

auto operator*(LinearApproximation lhs, const double rhs) -> LinearApproximation
{
    lhs *= rhs;
    return lhs;
}

auto operator*(const double lhs, LinearApproximation rhs) -> LinearApproximation
{
    rhs *= lhs;
    return rhs;
}

auto operator/(LinearApproximation lhs, const double rhs) -> LinearApproximation
{
    lhs /= rhs;
    return lhs;
}

}    // namespace mto