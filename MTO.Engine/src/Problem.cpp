#include "Problem.h"

#include "Config.h"
#include "SolverParticular.h"

#include <Eigen/QR>
#include <Eigen/Sparse>
#include <base64.h>
#include <fmt/core.h>
#ifdef MTO_USE_PARDISO
#include <Eigen/PardisoSupport>
#endif
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>

#include <algorithm>
#include <execution>
#include <iostream>
#include <mutex>
#include <utility>

namespace mto {

DOFManager::DOFManager(const std::vector<LoadPair>& prescribedDOFs, const size_t nNodes, const size_t nDOFsPerNode)
    : nDOFs{nNodes * nDOFsPerNode}, nUnknowns{nNodes * nDOFsPerNode}, dof2unknown(nNodes * nDOFsPerNode, 1)
{
    for (const auto& d : prescribedDOFs)
        this->dof2unknown.at(d.DOFid) = std::nullopt;

    auto counter = size_t{0};
    for (auto& d : this->dof2unknown)
        if (d.has_value())
            d = counter++;

    this->nUnknowns = counter;
}

auto DOFManager::extract_unknown_part(const Eigen::VectorXd& fromVec) const -> Eigen::VectorXd
{
    if (static_cast<size_t>(fromVec.size()) != this->nDOFs)
        throw std::invalid_argument{"DOFManager::extract_unknown_part: Input vector's length does not correspond to number of unknowns."};

    auto outVec = Eigen::VectorXd{Eigen::VectorXd::Constant(this->nUnknowns, 0.0)};
    for (auto i = 0; i < this->nDOFs; ++i)
        if (this->dof2unknown.at(i).has_value())
            outVec(this->dof2unknown.at(i).value()) = fromVec(i);

    return outVec;
};

void DOFManager::update_with_unknown_part(Eigen::VectorXd& updateVec /*inout*/, const Eigen::VectorXd& fromVec) const
{
    for (auto i = 0; i < this->nDOFs; ++i)
        if (this->dof2unknown.at(i).has_value())
            updateVec(i) = fromVec(this->dof2unknown.at(i).value());
};

auto DOFManager::map_DOFs_to_unknowns(const std::vector<size_t>& inDOFs) const -> std::vector<std::optional<size_t>>
{
    auto outUnknowns = std::vector<std::optional<size_t>>{};
    outUnknowns.reserve(inDOFs.size());

    std::transform(std::begin(inDOFs), std::end(inDOFs), std::back_inserter(outUnknowns),
        [this](const auto& inDOF) {
            return this->dof2unknown.at(inDOF);
        });

    return outUnknowns;
};

Problem::Problem(const nlohmann::json& inJSON, Discretisation* inDiscretisation)
    : discretisation{inDiscretisation}
{
    // Check for required data fields
    if (!inJSON.contains("prescribedValues"))
        throw std::invalid_argument{"Problem::Problem: prescribedValues entry must be provided."};
    if (!inJSON.contains("prescribedLoad") && !inJSON.contains("objectiveVector"))
        throw std::invalid_argument{"Problem::Problem: Either prescribedLoad or objectiveVector entry must be provided."};

    // Load prescribed values
    if (inJSON.contains("prescribedValues"))
        for (const auto& entry : inJSON["prescribedValues"]) {
            if (entry.is_array())
                if (entry.size() == 2)
                    this->prescribedValuePairs.emplace_back(entry[0].get<size_t>(), entry[1].get<double>());
                else
                    throw std::invalid_argument{"Problem::Problem: Prescribed values contain arrays of other size than 2."};
            else if (entry.is_object()) {
                if (!entry.contains("component") || !entry.contains("value") || !entry.contains("lowerBound") || !entry.contains("upperBound"))
                    throw std::invalid_argument{"Problem::Problem: Prescribed values' object must contain 'lowerBound', 'upperBound', 'component', and 'value' entries."};

                const auto box = BoundaryBox{entry};
                const auto component = entry["component"].get<double>();
                const auto value = entry["value"].get<double>();

                const auto nodeIds = this->discretisation->get_node_ids_inside_box(box);
                if (nodeIds.size() == 0)
                    throw std::logic_error{"Problem::Problem: No node found within a provided box for prescribedValues."};
                for (const auto& nid : nodeIds)
                    this->prescribedValuePairs.emplace_back(2 * nid + component, value);
            }
            else
                throw std::invalid_argument{"Problem::Problem: Prescribed values must contain either 2-length arrays or box objects."};
        }

    // Load prescribed loads
    if (inJSON.contains("prescribedLoad")) {
        if (!inJSON.contains("objectiveVector"))
            this->isSimpleCompliance = true;

        this->prescribedLoadPairs = std::vector<LoadPair>{};
        for (const auto& entry : inJSON["prescribedLoad"]) {
            if (entry.is_array())
                if (entry.size() == 2)
                    this->prescribedLoadPairs->emplace_back(entry[0].get<size_t>(), entry[1].get<double>());
                else
                    throw std::invalid_argument{"Problem::Problem: Prescribed load contain arrays of other size than 2."};
            else if (entry.is_object()) {
                if (!entry.contains("component") || !entry.contains("value") || !entry.contains("lowerBound") || !entry.contains("upperBound"))
                    throw std::invalid_argument{"Problem::Problem: Prescribed load's object must contain 'lowerBound', 'upperBound', 'component', and 'value' entries."};

                const auto box = BoundaryBox{entry};
                const auto component = entry["component"].get<double>();
                const auto value = entry["value"].get<double>();

                const auto nodeIds = this->discretisation->get_node_ids_inside_box(box);
                if (nodeIds.size() == 0)
                    throw std::logic_error{"Problem::Problem: No node found within a provided box for prescribedLoad."};
                for (const auto& nid : nodeIds)
                    this->prescribedLoadPairs->emplace_back(2 * nid + component, value);
            }
            else
                throw std::invalid_argument{"Problem::Problem: Prescribed load must contain either 2-length arrays or box objects."};
        }
    }

    // Load objective vector
    if (inJSON.contains("objectiveVector")) {
        this->isSimpleCompliance = false;

        this->objectiveVectorPairs = std::vector<LoadPair>{};
        for (const auto& entry : inJSON["objectiveVector"]) {
            if (entry.is_array())
                if (entry.size() == 2)
                    this->objectiveVectorPairs->emplace_back(entry[0].get<size_t>(), entry[1].get<double>());
                else
                    throw std::invalid_argument{"Problem::Problem: Prescribed objective vector entries contain arrays of other size than 2."};
            else if (entry.is_object()) {
                if (!entry.contains("component") || !entry.contains("value") || !entry.contains("lowerBound") || !entry.contains("upperBound"))
                    throw std::invalid_argument{"Problem::Problem: Prescribed objective vector entry's object must contain 'lowerBound', 'upperBound', 'component', and 'value' entries."};

                const auto box = BoundaryBox{entry};
                const auto component = entry["component"].get<double>();
                const auto value = entry["value"].get<double>();

                const auto nodeIds = this->discretisation->get_node_ids_inside_box(box);
                if (nodeIds.size() == 0)
                    throw std::logic_error{"Problem::Problem: No node found within a provided box for objectiveVector."};
                for (const auto& nid : nodeIds)
                    this->objectiveVectorPairs->emplace_back(2 * nid + component, value);
            }
            else
                throw std::invalid_argument{"Problem::Problem: Prescribed objective vector entries must contain either 2-length arrays or box objects."};
        }
    }

    // Load addeded stiffness
    if (inJSON.contains("additionalStiffness")) {
        this->additionalSprings = std::vector<LoadPair>{};
        for (const auto& entry : inJSON["additionalStiffness"]) {
            if (entry.is_array())
                if (entry.size() == 2)
                    this->additionalSprings->emplace_back(entry[0].get<size_t>(), entry[1].get<double>());
                else
                    throw std::invalid_argument{"Problem::Problem: Prescribed additional stiffness entries contain arrays of other size than 2."};
            else if (entry.is_object()) {
                if (!entry.contains("component") || !entry.contains("value") || !entry.contains("lowerBound") || !entry.contains("upperBound"))
                    throw std::invalid_argument{"Problem::Problem: Prescribed additional stiffness entry's object must contain 'lowerBound', 'upperBound', 'component', and 'value' entries."};

                const auto box = BoundaryBox{entry};
                const auto component = entry["component"].get<double>();
                const auto value = entry["value"].get<double>();

                const auto nodeIds = this->discretisation->get_node_ids_inside_box(box);
                if (nodeIds.size() == 0)
                    throw std::logic_error{"Problem::Problem: No node found within a provided box for additionalStiffness."};
                for (const auto& nid : nodeIds)
                    this->additionalSprings->emplace_back(2 * nid + component, value);
            }
            else
                throw std::invalid_argument{"Problem::Problem: Prescribed additional stiffness entries must contain either 2-length arrays or box objects."};
        }
    }

    // Initialize manager
    this->manager = DOFManager{this->prescribedValuePairs, inDiscretisation->get_number_of_nodes(), 2};

    // Set material properties
    if (inJSON.contains("Emin"))
        this->Emin = inJSON["Emin"].get<double>();
    if (inJSON.contains("E0"))
        this->E0 = inJSON["E0"].get<double>();
    if (inJSON.contains("nu"))
        this->nu = inJSON["nu"].get<double>();

    // Initialize state variables
    this->uAll = Eigen::VectorXd::Constant(this->manager.get_number_of_DOFs(), 0.0);
    for (const auto [i, v] : this->prescribedValuePairs)
        this->uAll(i) = v;
    this->lambdaAll = Eigen::VectorXd::Constant(this->manager.get_number_of_DOFs(), 0.0);
    this->u = this->manager.extract_unknown_part(this->uAll);
    this->lambda = this->manager.extract_unknown_part(this->lambdaAll);

    // Initialize load vector
    this->FextAll = Eigen::VectorXd::Constant(this->manager.get_number_of_DOFs(), 0.0);
    if (this->prescribedLoadPairs.has_value())
        for (const auto [i, v] : this->prescribedLoadPairs.value())
            this->FextAll(i) = v;
    this->Fext = this->manager.extract_unknown_part(this->FextAll);

    // Initialize objective vector
    if (this->objectiveVectorPairs.has_value()) {
        this->cAll = Eigen::VectorXd::Constant(this->manager.get_number_of_DOFs(), 0.0);
        for (const auto [i, v] : this->objectiveVectorPairs.value())
            this->cAll(i) = v;
    }
    else
        this->cAll = this->FextAll;
    this->c = this->manager.extract_unknown_part(this->cAll);

    // Initialize solver
    this->solver = std::make_unique<SolverParticular>();
}

auto Problem::compute_sensitivity(const double p) const -> LinearApproximation
{
    const auto nElems = this->discretisation->get_number_of_elements();
    const auto nFreeElems = this->discretisation->get_number_of_free_elements();

    const auto vectorDiff = this->lambdaAll + this->uAll;
    const bool isSelfAdjoint = (vectorDiff.lpNorm<Eigen::Infinity>() <= 1e-15) ? true : false;

    auto gradient = Eigen::VectorXd{Eigen::VectorXd::Constant(nFreeElems, 0.0)};
    std::for_each(
#ifdef MTO_USE_PARALLEL_ALGORITHMS
        std::execution::par,
#else
        std::execution::seq,
#endif
        this->discretisation->get_elements_begin(), this->discretisation->get_elements_end(),
        [this, &gradient, &p, &isSelfAdjoint](const auto& e) {
            if (!e.is_fixed()) {
                const auto dofIds = std::vector<size_t>{
                    static_cast<size_t>(2 * e.nodeIds[0]),
                    static_cast<size_t>(2 * e.nodeIds[0] + 1),
                    static_cast<size_t>(2 * e.nodeIds[1]),
                    static_cast<size_t>(2 * e.nodeIds[1] + 1),
                    static_cast<size_t>(2 * e.nodeIds[2]),
                    static_cast<size_t>(2 * e.nodeIds[2] + 1),
                    static_cast<size_t>(2 * e.nodeIds[3]),
                    static_cast<size_t>(2 * e.nodeIds[3] + 1)};

                const auto& A = this->discretisation->get_ith_node_coordinates(e.nodeIds[0]);
                const auto& B = this->discretisation->get_ith_node_coordinates(e.nodeIds[1]);
                const auto& C = this->discretisation->get_ith_node_coordinates(e.nodeIds[2]);

                const auto hx = (B - A).norm();
                const auto hy = (C - A).norm();

                const auto coeff = p * std::pow(e.density, p - 1) * (this->E0 - this->Emin);
                const auto Kloc = Eigen::Matrix<double, 8, 8>{coeff * this->compute_element_stiffness(hx, hy)};

                gradient(e.freeIndex) = this->lambdaAll(dofIds).dot(Kloc * this->uAll(dofIds));
                if (isSelfAdjoint)
                    gradient(e.freeIndex) = std::min(gradient(e.freeIndex), -1e-16);
            }
        });

    return LinearApproximation{this->compute_objective(), gradient, LinearApproximationType::State};
};

void Problem::solve_state(const double p)
{
    auto logger = spdlog::get("MTO.Logger");
    const auto nElems = this->discretisation->get_number_of_elements();

    // Construct vector of internal forces and stiffness matrix
    auto sw = spdlog::stopwatch{};

    auto Fint = Eigen::VectorXd{Eigen::VectorXd::Constant(this->manager.get_number_of_unknowns(), 0.0)};
    auto mutexFint = std::mutex{};

    const auto estimatedElemStiffnessSize = size_t{8 * 8};
    auto triplets = std::vector<Eigen::Triplet<double>>{};
    triplets.reserve(nElems * estimatedElemStiffnessSize);
    auto mutexTriplets = std::mutex{};

    std::for_each(
        // #ifdef MTO_USE_PARALLEL_ALGORITHMS
        //         std::execution::par,         // Not worthy for the linear problem
        // #else
        std::execution::seq,
        // #endif
        this->discretisation->get_elements_begin(), this->discretisation->get_elements_end(),
        [this, &p, &Fint, &triplets, &mutexFint, &mutexTriplets](const auto& e) {
            const auto dofIds = std::vector<size_t>{
                static_cast<size_t>(2 * e.nodeIds[0]),
                static_cast<size_t>(2 * e.nodeIds[0] + 1),
                static_cast<size_t>(2 * e.nodeIds[1]),
                static_cast<size_t>(2 * e.nodeIds[1] + 1),
                static_cast<size_t>(2 * e.nodeIds[2]),
                static_cast<size_t>(2 * e.nodeIds[2] + 1),
                static_cast<size_t>(2 * e.nodeIds[3]),
                static_cast<size_t>(2 * e.nodeIds[3] + 1)};
            const auto unknownIds = this->manager.map_DOFs_to_unknowns(dofIds);

            const auto& A = this->discretisation->get_ith_node_coordinates(e.nodeIds[0]);
            const auto& B = this->discretisation->get_ith_node_coordinates(e.nodeIds[1]);
            const auto& C = this->discretisation->get_ith_node_coordinates(e.nodeIds[2]);

            const auto hx = (B - A).norm();
            const auto hy = (C - A).norm();

            const auto Ecoeff = this->Emin + std::pow(e.density, p) * (this->E0 - this->Emin);
            const auto Kloc = Eigen::Matrix<double, 8, 8>{Ecoeff * this->compute_element_stiffness(hx, hy)};

            const auto uPresLoc = Eigen::Vector<double, 8>{
                this->uAll(dofIds[0]),
                this->uAll(dofIds[1]),
                this->uAll(dofIds[2]),
                this->uAll(dofIds[3]),
                this->uAll(dofIds[4]),
                this->uAll(dofIds[5]),
                this->uAll(dofIds[6]),
                this->uAll(dofIds[7])};
            const auto FintLoc = Eigen::Vector<double, 8>{Kloc * uPresLoc};

            {
                const auto lock = std::lock_guard<std::mutex>{mutexFint};
                for (auto iRloc = 0; iRloc < 0; ++iRloc)
                    if (const auto iRglob = unknownIds.at(iRloc);
                        iRglob.has_value())
                        Fint(iRglob.value()) += FintLoc(iRloc);
            }

            auto threadTriplets = std::vector<Eigen::Triplet<double>>{};
            threadTriplets.reserve(unknownIds.size() * unknownIds.size());
            for (auto iRloc = 0; iRloc < 8; ++iRloc)
                for (auto iCloc = 0; iCloc < 8; ++iCloc) {
                    const auto iRglob = unknownIds[iRloc];
                    const auto iCglob = unknownIds[iCloc];
                    if (iRglob.has_value() && iCglob.has_value())
                        threadTriplets.emplace_back(iRglob.value(), iCglob.value(), Kloc(iRloc, iCloc));
                }
            {
                const auto lock = std::lock_guard<std::mutex>{mutexTriplets};
                triplets.insert(std::end(triplets), std::begin(threadTriplets), std::end(threadTriplets));
            }
        });

    // Append artificial stiffness
    if (this->additionalSprings.has_value())
        for (const auto [dof, val] : this->additionalSprings.value())
            if (const auto unknown = this->manager.map_ith_DOF_to_unknown(dof);
                unknown.has_value())
                triplets.emplace_back(unknown.value(), unknown.value(), val);

    auto K = Eigen::SparseMatrix<double>{
        static_cast<Eigen::Index>(this->manager.get_number_of_unknowns()),
        static_cast<Eigen::Index>(this->manager.get_number_of_unknowns())};
    K.setFromTriplets(std::begin(triplets), std::end(triplets));

    if (PRINT_DETAILED_TIMING)
        logger->info("\t\t\tSystem built in {} s", sw);

    // Solve
    if (!this->solver->is_initialized()) {
        sw.reset();
        this->solver->analyzePattern(K);
        if (PRINT_DETAILED_TIMING)
            logger->info("\t\t\tSystem sparsity pattern analyzed in {} s", sw);
    }

    sw.reset();
    this->solver->factorize(K);
    if (PRINT_DETAILED_TIMING)
        logger->info("\t\t\tSystem factorized in {} s", sw);

    if (this->solver->info() != Eigen::Success)
        throw std::runtime_error{"Problem::solve_state: Unable to factorize K."};
    if (PRINT_DETAILED_TIMING)
        logger->info("\t\t\tSystem analyzed and factorized in {} s using {}", sw, this->solver->name());

    sw.reset();
    const auto F = Eigen::VectorXd{this->Fext - Fint};
    this->u = this->solver->solve(F);
    if (this->solver->info() != Eigen::Success)
        throw std::runtime_error{"Problem::solve_state: Unable to solve K*u = F."};
    if (PRINT_DETAILED_TIMING)
        logger->info("\t\t\tSystem solved for F in {} s", sw);

    sw.reset();
    if (this->isSimpleCompliance)
        this->lambda = -this->u;
    else {
        this->lambda = -1.0 * this->solver->solve(this->c);
        if (this->solver->info() != Eigen::Success)
            throw std::runtime_error{"Problem::solve_state: Unable to solve K*lambda = -c."};
    }
    if (PRINT_DETAILED_TIMING)
        logger->info("\t\t\tSystem solved for lambda in {} s", sw);

    // Update state
    sw.reset();
    this->manager.update_with_unknown_part(this->uAll, this->u);
    this->manager.update_with_unknown_part(this->lambdaAll, this->lambda);
    if (PRINT_DETAILED_TIMING)
        logger->info("\t\t\tSystem updated in {} s", sw);
}

auto Problem::print_vertex_state_for_VTK(const bool useBinary) const -> std::string
{
    const auto nNodes = this->discretisation->get_number_of_nodes();

    auto out = std::string{};
    if (useBinary) {
        auto nodalDisplacements = std::vector<float>{};
        nodalDisplacements.reserve(3 * nNodes);
        for (auto iN = 0; iN < nNodes; ++iN)
            nodalDisplacements.insert(nodalDisplacements.end(),
                {static_cast<float>(this->uAll(2 * iN)), static_cast<float>(this->uAll(2 * iN + 1)), float{0.0}});

        out.reserve(100 + static_cast<size_t>(std::ceil(nodalDisplacements.size() * sizeof(float) * 8 / 6)));
        fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Float32\" Name=\"Displacements\" NumberOfComponents=\"3\" format=\"binary\">\n");
        fmt::format_to(std::back_inserter(out), "{}",
            base64::encode_vector_into_base64_string_with_length(nodalDisplacements));
        fmt::format_to(std::back_inserter(out), "\t\t\t\t</DataArray>\n");

        if (this->storedDualsNodalwise.has_value()) {
            auto nodalStresses = std::vector<float>{};
            nodalStresses.reserve(4 * nNodes);
            for (auto iN = 0; iN < nNodes; ++iN) {
                const auto& data = this->storedDualsNodalwise.value().at(iN);
                nodalStresses.insert(nodalStresses.end(),
                    {static_cast<float>(data(0)), static_cast<float>(data(2)), static_cast<float>(data(2)), static_cast<float>(data(1))});
            }

            out.reserve(out.size() + 100 + +static_cast<size_t>(std::ceil(nodalStresses.size() * sizeof(float) * 8 / 6)));
            fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Float32\" Name=\"NodalStresses\" NumberOfComponents=\"4\" format=\"binary\">\n");
            fmt::format_to(std::back_inserter(out), "\t\t\t\t\t{}",
                base64::encode_vector_into_base64_string_with_length(nodalStresses));
            fmt::format_to(std::back_inserter(out), "\n\t\t\t\t</DataArray>\n");
        }
    }
    else {
        out.append("VECTORS Displacements double\n");
        for (auto iN = 0; iN < nNodes; ++iN)
            out.append(double_to_formated_string(this->uAll(2 * iN)) + ' ' + double_to_formated_string(this->uAll(2 * iN + 1)) + " 0.0\n");

        if (this->storedDualsNodalwise.has_value()) {
            out.append("\nTENSORS NodalStresses double\n");
            for (auto iN = 0; iN < nNodes; ++iN) {
                const auto& data = this->storedDualsNodalwise.value().at(iN);
                out.append(
                    double_to_formated_string(data(0)) + ' ' + double_to_formated_string(data(2)) + " 0.0 \n" +
                    double_to_formated_string(data(2)) + ' ' + double_to_formated_string(data(1)) + " 0.0 \n" +
                    "0.0 0.0 0.0\n");
            }
        }
    }

    return out;
}

auto Problem::print_element_state_for_VTK(const bool useBinary) const -> std::string
{
    auto out = std::string{};
    if (this->storedDualsElementwise.has_value()) {
        const auto nElems = this->discretisation->get_number_of_elements();
        if (useBinary) {
            auto elementwiseStresses = std::vector<float>{};
            elementwiseStresses.reserve(4 * nElems);
            for (auto iE = 0; iE < nElems; ++iE) {
                const auto& data = this->storedDualsElementwise.value().at(iE);
                elementwiseStresses.insert(elementwiseStresses.end(),
                    {static_cast<float>(data(0)), static_cast<float>(data(2)),
                        static_cast<float>(data(2)), static_cast<float>(data(1))});
            }

            out.reserve(100 + static_cast<size_t>(std::ceil(elementwiseStresses.size() * sizeof(float) * 8 / 6)));
            fmt::format_to(std::back_inserter(out), "\t\t\t\t<DataArray type=\"Float32\" Name=\"ElementStresses\" NumberOfComponents=\"4\" format=\"binary\">\n");
            fmt::format_to(std::back_inserter(out), "\t\t\t\t\t{}",
                base64::encode_vector_into_base64_string_with_length(elementwiseStresses));
            fmt::format_to(std::back_inserter(out), "\n\t\t\t\t</DataArray>\n");
        }
        else {
            out.append("\nTENSORS elementStress double\n");
            for (auto iE = 0; iE < nElems; ++iE) {
                const auto& data = this->storedDualsElementwise.value().at(iE);
                out.append(
                    double_to_formated_string(data(0)) + ' ' + double_to_formated_string(data(2)) + " 0.0 \n" +
                    double_to_formated_string(data(2)) + ' ' + double_to_formated_string(data(1)) + " 0.0 \n" +
                    "0.0 0.0 0.0\n");
            }
        }
    }

    return out;
}

void Problem::compute_duals_elementwise(const double p)
{
    this->storedDualsElementwise = std::vector<Eigen::Vector3d>{};
    this->storedDualsElementwise.value().reserve(this->discretisation->get_number_of_elements());

    for (auto iE = size_t{0}; iE < this->discretisation->get_number_of_elements(); ++iE) {
        const auto& e = this->discretisation->get_ith_element(iE);

        const auto [coords, Bmtrx] = e.get_superconvergence_coord_and_B();
        const auto D = this->get_material_stiffness(p, e.get_density());

        auto indices = std::vector<int>{};
        indices.reserve(8);
        for (const auto i : e.nodeIds) {
            indices.push_back(2 * i);
            indices.push_back(2 * i + 1);
        }
        const auto uLocal = this->uAll(indices);
        const auto s = (D * (Bmtrx * uLocal)).eval();

        this->storedDualsElementwise.value().push_back(s);
    }
}

void Problem::compute_duals_nodalwise(const double p, const bool useSPR)
{
    // Superconvergence patch recovery
    if (useSPR) {
        const auto NodeElemAdjacency = this->discretisation->get_node_element_adjacency();
        auto vertexDuals = std::vector<std::vector<Eigen::Vector3d>>(this->discretisation->get_number_of_nodes());
        for (auto iN = size_t{0}; iN < NodeElemAdjacency.size(); ++iN) {
            if (NodeElemAdjacency.at(iN).size() > 3) {

                const auto vertexCoord = this->discretisation->get_ith_node_coordinates(iN);

                // Extract data for LSQ fit
                auto patchCoords = std::vector<Eigen::Vector3d>{};
                auto patchValues = std::vector<Eigen::Vector3d>{};
                for (const auto iE : NodeElemAdjacency.at(iN)) {
                    const auto& e = this->discretisation->get_ith_element(iE);

                    const auto [coords, Bmtrx] = e.get_superconvergence_coord_and_B();
                    const auto D = this->get_material_stiffness(p, e.get_density());

                    auto indices = std::vector<int>{};
                    indices.reserve(8);
                    for (const auto i : e.nodeIds) {
                        indices.push_back(2 * i);
                        indices.push_back(2 * i + 1);
                    }
                    const auto uLocal = this->uAll(indices);
                    const auto s = (D * (Bmtrx * uLocal)).eval();

                    patchCoords.emplace_back(coords - vertexCoord);
                    patchValues.emplace_back(s);
                }

                // Convert into min_{a} (P*A - B)^2
                const auto nPoints = patchCoords.size();
                auto P = Eigen::MatrixXd(nPoints, 4);
                for (auto i = 0; i < nPoints; ++i) {
                    P.row(i) = Eigen::Vector4d{1.0, patchCoords.at(i)(0), patchCoords.at(i)(1), patchCoords.at(i)(0) * patchCoords.at(i)(1)};
                }
                auto B = Eigen::MatrixXd(nPoints, 3);
                for (auto i = 0; i < nPoints; ++i)
                    B.row(i) = patchValues.at(i);

                auto LSQsolver = Eigen::ColPivHouseholderQR<Eigen::MatrixXd>{};
                LSQsolver.compute(P);
                const auto A = LSQsolver.solve(B).eval();

                // Compute desired central vertex values
                auto queryParams = Eigen::Vector4d{1.0, 0.0, 0.0, 0.0};
                vertexDuals.at(iN).emplace_back(queryParams.transpose() * A);

                // Check for potential boundary vertices
                auto oneAroundVertices = std::vector<size_t>{};
                for (const auto iE : NodeElemAdjacency.at(iN)) {
                    const auto& e = this->discretisation->get_ith_element(iE);
                    for (const auto iV : e.nodeIds) {
                        if ((iV != iN) &&
                            (NodeElemAdjacency.at(iV).size() < 4) &&
                            (std::find(std::begin(oneAroundVertices), std::end(oneAroundVertices), iV) == std::end(oneAroundVertices)))
                            oneAroundVertices.emplace_back(iV);
                    }
                }

                for (const auto iV : oneAroundVertices) {
                    const auto queryCoord = (this->discretisation->get_ith_node_coordinates(iV) - vertexCoord).eval();
                    auto queryParams2 = Eigen::Vector4d{1.0, queryCoord(0), queryCoord(1), queryCoord(0) * queryCoord(1)};
                    vertexDuals.at(iV).emplace_back(queryParams2.transpose() * A);
                }
            }
        }

        // Average if several values are present
        for (auto& v : vertexDuals) {
            if (v.size() == 0)
                throw std::logic_error{"Problem::compute_duals_nodalwise: Vertex without assigned value detected."};
            else if (v.size() > 1) {
                const auto average = (std::accumulate(std::begin(v), std::end(v), Eigen::VectorXd(v.at(0).size()).setZero()) / v.size()).eval();

                std::cout << "Compare average (" << average(0) << ", " << average(1) << ", " << average(2) << ") with summands:\n";
                for (const auto& s : v)
                    std::cout << "\t(" << s(0) << ", " << s(1) << ", " << s(2) << ")\n";

                v.clear();
                v.push_back(average);
            }
        }

        // Store final values
        this->storedDualsNodalwise = std::vector<Eigen::Vector3d>{};
        this->storedDualsNodalwise.value().reserve(vertexDuals.size());
        std::transform(std::begin(vertexDuals), std::end(vertexDuals), std::back_inserter(this->storedDualsNodalwise.value()),
            [](const auto& v) { return v.at(0); });
    }
    // Averaging from adjacent elements
    else {
        const bool useWeightedAverage = true;

        this->storedDualsNodalwise = std::vector<Eigen::Vector3d>{};

        const auto nNodes = this->discretisation->get_number_of_nodes();
        const auto NodeElemAdjacency = this->discretisation->get_node_element_adjacency();
        for (auto iN = size_t{0}; iN < nNodes; ++iN) {
            auto averStress = Eigen::Vector3d{}.setZero();
            auto denominator = 0.0;
            for (const auto iE : NodeElemAdjacency.at(iN)) {
                const auto& e = this->discretisation->get_ith_element(iE);
                const auto rho = std::max(e.get_density(), 1e-9);

                const auto [coords, Bmtrx] = e.get_superconvergence_coord_and_B();
                const auto D = this->get_material_stiffness(p, rho);

                auto indices = std::vector<int>{};
                indices.reserve(8);
                for (const auto i : e.nodeIds) {
                    indices.push_back(2 * i);
                    indices.push_back(2 * i + 1);
                }
                const auto uLocal = this->uAll(indices);
                const auto s = (D * (Bmtrx * uLocal)).eval();
                if (useWeightedAverage) {
                    averStress += (rho * s);
                    denominator += rho;
                }
                else {
                    averStress += s;
                    denominator += 1.0;
                }
            }
            averStress /= denominator;
            this->storedDualsNodalwise.value().push_back(averStress);
        }
    }
}

auto Problem::get_material_stiffness(double p, double rho) const -> Eigen::Matrix<double, 3, 3>
{
    const auto E = this->Emin + std::pow(rho, p) * (this->E0 - this->Emin);
    const auto nu2 = this->nu * this->nu;

    // clang-format off
    return (Eigen::MatrixXd(3, 3) <<
        -E/(nu2 - 1),            -(E*this->nu)/(nu2 - 1), 0,
        -(E*this->nu)/(nu2 - 1), -E/(nu2 - 1),            0,
        0,                       0,                       E/(2*this->nu + 2)
        ).finished();
    // clang-format on
}

auto Problem::compute_element_stiffness(double hx, double hy) const -> Eigen::Matrix<double, 8, 8>
{
    constexpr auto initE = 1.0;

    const auto nunu_1 = this->nu * this->nu - 1.0;
    const auto nu_1 = this->nu - 1.0;
    const auto nu3_1 = this->nu * 3.0 - 1.0;
    const auto nu8_8 = this->nu * 8.0 - 8.0;
    const auto nunu8_8 = this->nu * this->nu * 8.0 - 8.0;
    const auto hxhx = hx * hx;
    const auto hyhy = hy * hy;
    const auto hxhxnu = hxhx * this->nu;
    const auto hyhynu = hyhy * this->nu;
    const auto hxhy = hx * hy;
    const auto hxhynunu_1 = hxhy * nunu_1;

    auto K = Eigen::Matrix<double, 8, 8>{};
    K(0, 0) = (initE * (-hxhxnu + hxhx + hyhy * 2.0) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(1, 0) = K(0, 1) = (initE * (-1.0 / 8.0)) / nu_1;
    K(2, 0) = K(0, 2) = (initE * (hxhxnu - hxhx + hyhy * 4.0)) / (hxhynunu_1 * 12.0);
    K(3, 0) = K(0, 3) = (initE * nu3_1 * (-1.0 / 8.0)) / nunu_1;
    K(4, 0) = K(0, 4) = (initE * (hxhxnu - hxhx + hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(5, 0) = K(0, 5) = (initE * nu3_1) / nunu8_8;
    K(6, 0) = K(0, 6) = (initE * (-hxhxnu + hxhx + hyhy * 2.0)) / (hxhynunu_1 * 12.0);
    K(7, 0) = K(0, 7) = (initE / nu8_8);

    K(1, 1) = (initE * (-hyhynu + hxhx * 2.0 + hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(2, 1) = K(1, 2) = (initE * nu3_1) / nunu8_8;
    K(3, 1) = K(1, 3) = (initE * (hyhynu + hxhx - hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(4, 1) = K(1, 4) = (initE * nu3_1 * (-1.0 / 8.0)) / nunu_1;
    K(5, 1) = K(1, 5) = (initE * (hyhynu + hxhx * 4.0 - hyhy)) / (hxhynunu_1 * 12.0);
    K(6, 1) = K(1, 6) = (initE / nu8_8);
    K(7, 1) = K(1, 7) = (initE * (-hyhynu + hxhx * 2.0 + hyhy)) / (hxhynunu_1 * 12.0);

    K(2, 2) = (initE * (-hxhxnu + hxhx + hyhy * 2.0) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(3, 2) = K(2, 3) = (initE / nu8_8);
    K(4, 2) = K(2, 4) = (initE * (-hxhxnu + hxhx + hyhy * 2.0)) / (hxhynunu_1 * 12.0);
    K(5, 2) = K(2, 5) = (initE * (-1.0 / 8.0)) / nu_1;
    K(6, 2) = K(2, 6) = (initE * (hxhxnu - hxhx + hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(7, 2) = K(2, 7) = (initE * nu3_1 * (-1.0 / 8.0)) / nunu_1;

    K(3, 3) = (initE * (-hyhynu + hxhx * 2.0 + hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(4, 3) = K(3, 4) = (initE * (-1.0 / 8.0)) / nu_1;
    K(5, 3) = K(3, 5) = (initE * (-hyhynu + hxhx * 2.0 + hyhy)) / (hxhynunu_1 * 12.0);
    K(6, 3) = K(3, 6) = (initE * nu3_1) / nunu8_8;
    K(7, 3) = K(3, 7) = (initE * (hyhynu + hxhx * 4.0 - hyhy)) / (hxhynunu_1 * 12.0);

    K(4, 4) = (initE * (-hxhxnu + hxhx + hyhy * 2.0) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(5, 4) = K(4, 5) = (initE / nu8_8);
    K(6, 4) = K(4, 6) = (initE * (hxhxnu - hxhx + hyhy * 4.0)) / (hxhynunu_1 * 12.0);
    K(7, 4) = K(4, 7) = (initE * nu3_1) / nunu8_8;

    K(5, 5) = (initE * (-hyhynu + hxhy * 2.0 + hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(6, 5) = K(5, 6) = (initE * nu3_1 * (-1.0 / 8.0)) / nunu_1;
    K(7, 5) = K(5, 7) = (initE * (hyhynu + hxhx - hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);

    K(6, 6) = (initE * (-hxhxnu + hxhx + hyhy * 2.0) * (-1.0 / 6.0)) / (hxhynunu_1);
    K(7, 6) = K(6, 7) = (initE * (-1.0 / 8.0)) / nu_1;

    K(7, 7) = (initE * (-hyhynu + hxhx * 2.0 + hyhy) * (-1.0 / 6.0)) / (hxhynunu_1);

    return K;
}

}    // namespace mto