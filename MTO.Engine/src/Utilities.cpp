#include "Utilities.h"

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <spdlog/stopwatch.h>

#include <fmt/core.h>

#include <numeric>

namespace mto {

std::size_t Triplet::get_number_of_elements() const
{
    return std::accumulate(this->begin(), this->end(), static_cast<std::size_t>(1), std::multiplies<>());
}

auto load_JSON_file(const std::filesystem::path& inFilePath) -> nlohmann::json
{
    auto inJSON = nlohmann::json{};
    auto inFile = std::ifstream{inFilePath};
    inFile >> inJSON;

    return inJSON;
}

auto initialize_logger(std::filesystem::path outputFolder) -> std::shared_ptr<spdlog::logger>
{
    auto consoleLogger = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    consoleLogger->set_level(spdlog::level::info);
    consoleLogger->set_pattern("%v");

    auto fileLogger = std::make_shared<spdlog::sinks::basic_file_sink_mt>(
        (outputFolder / "log.txt").string(),
        true);
    fileLogger->set_level(spdlog::level::info);
    fileLogger->set_pattern("%v");

    auto logger = std::make_shared<spdlog::logger>(
        "MTO.Logger",
        spdlog::sinks_init_list({consoleLogger, fileLogger}));
    spdlog::register_logger(logger);

    return logger;
}

auto double_to_formated_string(double num) -> std::string
{
    std::string s = fmt::format("{:.8g}", num);
    //s.erase(s.erase(s.find_last_not_of('0') + 1).find_last_not_of('.') + 1);
    return s;
}

}    // namespace mto