# Modular Topology Optimization

[![pipeline status](https://gitlab.com/MartinDoskar/modular-topology-optimization/badges/master/pipeline.svg)](https://gitlab.com/MartinDoskar/modular-topology-optimization/commits)

C++ implementation of topology optimization based on Simple Isotropic Material with Penalisation (SIMP) method by Bensoe and Sigmund [1] with emphasis on modular topology optimization proposed by the authors of the code.

The code has been written by:
* Martin Doškář (@MartinDoskar) 
* Marek Tyburec (@tyburec)

both from the Open Mechanics group of the [Czech Technical University in Prague](https://www.cvut.cz/).

### Content
* [Design overview](#design-overview)
  * [Input parameters](#input-parameters)
* [Building the code](#building-the-code)
* [References](#references)



## Design overview

The code with its object-oriented design follows the standard steps in topology optimisation: (i) solving the state equations, (ii) computing sensitivites of the objective and constraints, and (iii) updating the design variables. Class `Task` serves as an overarching class for the whole formulations, storing all the relevant objects and controlling the parameters of the task. `Problem` class in connection with `Discretisation` class define a state equation to solve for an updated densities. Class `Manager` handles all densities used in the formulation, i.e. it stores the density values (design, filtered, and projected), performs all filtering operations (both on the densities and also on the sensitivities generated by individual `Problem` instances), computes volume constraint sensitivity and updates `Discretisation`'s densities with the projected ones. Finally, the update of the design variables is computed via a polymorhpic class specifying `OptimizerInterface`.

### Input parameters

While the code is meant for a research development with direct tweaks in C++ source files expected, once the formulation is finalized, several optimizaiton tasks can be called with a JSON input file specifying task parameters.

| Parameter                   | Default               | Description
| ---                         | ---                   | ---
| `E0`                        | [1.0, double]         | Young's modulus of the solid material
| `Emin`                      | [1.0, double]         | Young's modulus of the void material
| `nu`                        | [0.3, double]         | Poisson's ratio of both the solid and void material
| `limIter`                   | [100, size_t]         | Maximal number of iterations
| `limDesignChange`           | [1e-6, double]        | Convergence threshold for the infinity norm of design change
| `limObjectiveChange`        | [1e-3, double]        | Convergence threshold for the relative objective change
| `penalisation`              | [3.0, double]         | Penalisation parameter (if constant during iterationss)
| `penalisationInit`          | [-, double]           | Initial value of iteration-dependend penalisation parameter
| `penalisationIncrement`     | [-, double]           | Increment of iteration-dependend penalisation parameter
| `penalisationStep`          | [-, size_t]           | Step for the increment of iteration-dependend penalisation parameter
| `penalisationLimit`         | [-, double]           | Limit value of iteration-dependend penalisation parameter
| `qMaxOC`                    | [1.0, double]         | Maximal exponent in continuation modification to OC [3]
| `qAfterIterOC`              | [20, size_t]          | Threshold after which the q coefficient increases in continuation modification to OC
| `qMultiplierOC`             | [1.01, double]        | Multiplier of q coefficient in continuation modification to OC
| `outputFolder`              | ["./", string]        | Define output folder
| `useBinaryVTK`              | [true, bool]          | Switch controlling type of VTK output (legacy or binary XML)
| `storeNodalwiseDuals`       | [true, bool]          | Switch for computing and storing nodal-wise duals in the last iteration
| `storeElementwiseDuals`     | [false, bool]         | Switch for computing and storing elemejt-wise duals in the last iteration
| `saveEachNthStepVTK`        | [1, size_t]           | Number of iterations between stored states in VTK (0 to turn off, the first and the last always saved)
| `saveEachNthStepSVG`        | [1000, size_t]        | Number of iterations between stored states in SVG (0 to turn off, the first and the last always saved)
| `saveModulesEachNthStepSVG` | [1000, size_t]        | Number of iterations between stored states in module SVG (0 to turn off, the first and the last always saved)
| `discretizationType`        | [-, size_t]           | Type of discretization (regular grid = 0, mesh = 1, assembly plan = 2)
| `gridResolution`            | [-, array of int]     | Resolution of the regular grid-based discretisation provided as an array of size_t
| `assemblyPlan`              | [-, array of int]     | Assembly plan provided in a 1D array (first index being the fastest; with 0 used to indicate an empty spot)
| `assemblyResolution`        | [-, array of int]     | Resolution of the assembly plan provided as an array of size_t
| `moduleSize`                | [-, array of double]  | Physical size of each domain 
| `moduleResolution`          | [-, array of int]     | Resolution of individual module
| `additionalDiscretisation`  | [-, JSON object]      | Option to merge additional discretisation (JSON object with any of the above properties)
| `relativeVolumeConstraint`  | [-, double]           | Relative volume constraint 
| `optimizerType`             | ["OC", string]        | Type of optimizer
| `versionOC`                 | [-, int]              | Type of OC versin (by default 0 for compliance optimization and 1 otherwise)
| `stepOC`                    | [0.1, double]         | Maximum move step in OC iteration
| `dampingOC`                 | [-, double]           | Damping factor in OC iterations (default 0.3 in `versionOC`= 1 and 0.5 otherwise)
| `inconsistentModificationOC`| [-, double]           | Inconsistent modification parameter in OC update (default 1.e10 in `versionOC`= 1 and -1e20 otherwise)
| `filterType`                | [-, int]              | Type of filtering (KernelDensity = 0, HelmholtzDensity = 1, KernelSensitivity = 2)
| `filterRadius`              | [-, double]           | Radius-like parameter of filtering
| `problem`                   | [-, array of JSON]    | Array containing definition of several state problems (with entries bellow)
| `prescribedValues`          | [-, array of JSON]    | Specify prescribed values (either DOF-value pair or JSON object specified below)
| `prescribedLoad`            | [-, array of JSON]    | Specify prescribed nodal load (either DOF-value pair or JSON object specified below)
| `objectiveVector`           | [-, array of JSON]    | Specify elements of the objective vector (if not present `prescribedLoad` is taken instead and compliance minimization is assumed)
| `additionalStiffness`       | [-, array of JSON]    | Specify additional spring-like stiffnes for identified DOFs (either DOF-value pair or JSON object specified below)
| `lowerBound`                | [-, array of doubles] | Definition of lower-bound box corner used to extract relevant nodes
| `upperBound`                | [-, array of doubles] | Definition of upper-bound box corner used to extract relevant nodes
| `component`                 | [-, size_t]           | Index of direction/component of vector field to prescribe
| `value`                     | [-, double]           | Prescribed value
| `alwaysFilledElems`         | [-, array of int]     | List of elems' IDs a-priori enforced to be full
| `alwaysEmptyElems`          | [-, array of int]     | List of elems' IDs a-priori enforced to be empty


Below we provide an example of an input of the simple MBB beam problem.
 ```json
{
    "discretisationType": 0,
    "domainResolution": [300, 100],
    "domainSize": [3, 1],
    "limIter": 50,
    "limDesignChange": 1E-6,
    "limObjectiveChange": 1E-6,
    "penalisation": 3,
    "relativeVolumeConstraint": 0.3,
    "filterType": 0,
    "filterRadius": 0.015,
    "optimizerType": "OC",
    "versionOC": 0,
    "problems": [
        {
            "prescribedValues": [
                {
                    "lowerBound": [-0.001, -0.001],
                    "upperBound": [0.001, 0.001],
                    "component": 0,
                    "value": 0
                },
                {
                    "lowerBound": [-0.001, -0.001],
                    "upperBound": [0.001, 0.001],
                    "component": 1,
                    "value": 0
                },
                {
                    "lowerBound": [2.999, -0.001],
                    "upperBound": [3.001, 0.001],
                    "component": 1,
                    "value": 0
                }
            ],
            "prescribedLoad": [
                {
                    "lowerBound": [1.499, 0.999],
                    "upperBound": [1.501, 1.001],
                    "component": 1,
                    "value": -1
                }
            ]
        }
    ]
}
```


 ## Building the code

This projects relies on CMake (version >=3.20) for build automation and several external libraries:

* [base64](https://renenyffenegger.ch/notes/development/Base64/Encoding-and-decoding-base-64-with-cpp/index), v. 1.02 by René Nyffenegger and modified by the authors, for base64 encoding, 
* [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page) for linear algebra, 
* [FMT](https://fmt.dev/latest/index.html) formatting library, 
* [GoogleTest](https://github.com/google/googletest) test framework,
* [GSL](https://github.com/microsoft/GSL), Microsoft's implementation of the Guidelines Support Library,
* [nlohmann's JSON](https://github.com/nlohmann/json) library for JSON parsing,
* [spdlog](https://github.com/gabime/spdlog) logging library.

Aiming at the C++ 17 standard, the code should compile using MSVC (19.21+), GCC (10+), or Clang (10+).

To accelerate linear algebra operations, we advise to link against Math Kernel Library; see [details](https://eigen.tuxfamily.org/dox/TopicUsingIntelMKL.html) and the text below.

 ### Enabling MKL and MKL PARDISO solver using Intel oneAPI Toolkit

 Environment variables must be set in order to make CMake add the right include. These variables are set for given terminal session in *.bat or *.sh scripts in Intel oneAPI folder. When using VS Code, the same initialization can be achieved with Environment Configurator for Intel oneAPI Toolkits and Intel oneAPI: Initialize environment variables.

 When running outside the initialized environment, there might be a need to add a path to either `libomp.dll` or `libiomp5md.dll` to the system-wise PATH environment variable.

 ### Linking with OpenMP

 MKL Pardiso solver uses OpenMP implementation provided by Intel Compiler. While it links also with GCC and Clang OpenMP implementations, linking it with MSVC-based OpenMP implemenation leads to application's crashes.

 ### Compiling with Intel oneAPI compiler in VS Code

 As of August 2021, Intel compiler is not automatically detected by CMake Tools in VS Code. Intel compiler thus must be manually added via ```CMake: Edit User-Local CMake Kits``` .
 
 ### Profiling

 To generate runtime profiling data, please configure with `MTO_PROFILER_PG=ON` and `CMAKE_BUILD_TYPE=Debug` options. After compilation, the application can be run as usual but will appear slower due to generated debugging and profiling data. These are exported prior to the application termination to a datafile `gmon.out` in the current working directory. This file is inspected by the [gprof](https://users.cs.duke.edu/~ola/courses/programming/gprof.html) tool, and visual output produced by [gprof2dot](https://github.com/jrfonseca/gprof2dot).

 ## References
 [1] Bendsoe, M., Sigmund, O. (2003) Topology optimization: theory, methods and applications. Springer, Berlin.

 [2] Kim, N. H., Dong, T., Weinberg, D., & Dalidd, J. (2021). Generalized Optimality Criteria Method for Topology Optimization. Applied Sciences, 11(7), 3175.

 [3] Groenwold, A. A., Etman, L. F. P. (2009). A simple heuristic for gray-scale suppression in optimality criterion-based topology optimization. Structural and Multidisciplinary Optimization, 39(2), 217–225.
